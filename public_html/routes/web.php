<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('auth.login');
});


Route::get('/videos',function(){
	$get_all_videos = App\Video::orderBy('id','desc')->get();
	return view('front_end.videos')->with('get_all_videos',$get_all_videos);
});


Route::get('/', function () {
    return view('index');
});

Route::get('/register',function(){
	abort(404);
});

Route::get('/company_projects','ProjectContoller@companyProjects');
//Route::get('/company_projects/{id}','ProjectContoller@companyProjectDetail');


Route::get('/company_documents/{id}','DocumentController@getOneDocument');

Route::get('/about_us','FrontEndController@aboutUs');
Route::get('/contact_us','FrontEndController@contactUs');
Route::post('/send_message_contact_us','FrontEndController@sendMessageContactUs');
Route::get('logout', 'Auth\LoginController@logout');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('forget_password', function()
{   if (Route::has('login')) {
        if (Auth::check()) {
            return redirect('/login');
    
}
return view('front_end.forget_password'); 
}
});
	
Route::get('/company_documents','DocumentController@allDocumentsFrontEnd');

Route::get('/photos','FrontEndController@allPhotos');
Route::get('album','FrontEndController@allAlbums');
Route::get('album/{id}','FrontEndController@photoAlbum');
Route::get('/google74b7c7d77aa0b104.html',function(){

return view('google_file');
});

Route::group(['middleware' => ['auth']], function () {
Route::get('/contact_us_messages','BackEndController@getContactUsMessages');    
Route::post('/set_message_read','BackEndController@markMessageRead');
Route::get('/create_user','UserController@createUser');
Route::get('/all_users','UserController@viewUsers');
Route::post('/add_user','UserController@addUser');
Route::get('edit_user/{id}','UserController@editUser');
Route::post('/update_user','UserController@updateUser');
Route::post('/delete_contact_message','BackEndController@deleteContactMessage');
Route::get('/undo_contact_message_delete/{id}','BackEndController@undoDeleteContactMessage');
Route::get('/create_slider_show','FrontEndController@createSliderShow');
Route::get('/all_slider_show','FrontEndController@getAllSliderShowImges');
Route::post('/upload_new_slider_show','FrontEndController@uploadNewSliderShow');
Route::post('/end_slide_show','FrontEndController@endSliderShow');
Route::post('/delete_slider_image','FrontEndController@deleteSliderImage');
Route::get('/undo_delete_slider/{id}','FrontEndController@undoDeleteSliderImage');
Route::get('/my_profile','ProfileController@myProfile');
Route::post('/upload_profile_photo','ProfileController@uploadProfilePhotos');
Route::post('/change_my_password','ProfileController@changePassword');
Route::get('/set_image_default/{id}','ProfileController@setImageDefault');
Route::get('/delete_profile_image/{id}','ProfileController@deleteProfileImage');
Route::get('/upload_video','VideoController@uploadVideo');
Route::post('/upload_new_video','VideoController@uploadNewVideo');
Route::get('/all_videos','VideoController@allVideos');
Route::post('/delete_video','VideoController@deleteVideo');
Route::get('/undo_delete_video/{id}','VideoController@undoDeleteVideo');
Route::get('/all_documents','DocumentController@allDocuments');
Route::get('/document/{id}','DocumentController@oneDocument');
Route::post('/add_document','DocumentController@addDocument');
Route::get('/create_document','DocumentController@createDocument');
//Route::get('/create_photo','PhotoController@createPhoto');
//Route::get('/all_photos','PhotoController@allPhotos');
Route::post('/add_photo','PhotoController@addPhoto');
Route::get('/delete_photo/{id}','PhotoController@deletePhoto');
Route::get('/undo_delete_photo/{id}','PhotoController@undoDeletePhoto');
Route::post('/change_my_name','ProfileController@changeName');
Route::get('/message/{id}','FrontEndController@getMessage');
Route::post('/upload_document_image','DocumentController@uploadDocumentImage');
Route::get('/delete_document_image/{id}','DocumentController@deleteDocumentImage');
Route::get('/undo_delete_document_image/{id}','DocumentController@undoDeleteDocumentPhoto');
Route::post('/edit_document_name','DocumentController@update');
Route::resource('projects','ProjectContoller');
Route::post('/upload_project_image','ProjectContoller@uploadImage');
Route::get('/delete_project_image/{id}','ProjectContoller@deleteProjectImage');
Route::get('/undo_delete_project_image/{id}','ProjectContoller@undoDeleteProjectImage');
Route::get('/active_user/{id}','UserController@Activation');
Route::post('/delete_documents','DocumentController@deleteDocuments');
Route::get('/undo_document_delete/{id}','DocumentController@undoDeleteDocument');
Route::get('/set_first_slider/{id}','FrontEndController@setSliderFirst');
Route::get('/projects/edit/{id}','ProjectContoller@editProject');
Route::post('/update_project','ProjectContoller@updateProject');
Route::get('/delete_project/{id}','ProjectContoller@deleteProject');
Route::get('/undo_delete_project/{id}','ProjectContoller@undoDeleteProject');
Route::resource('last_news','LastNewsController');
Route::post('/update_news','LastNewsController@updateNews');
Route::get('delete_news/{id}','LastNewsController@destroy');
Route::get('/undo_delete_news/{id}','LastNewsController@undoDeleteNews');
Route::get('/active_news/{id}','LastNewsController@activeNews');
Route::resource('albums','AlbumController');
Route::post('/update_album','AlbumController@updateAlbum');
Route::get('/active_album/{id}','AlbumController@activeAlbum');
Route::post('/change_album','AlbumController@changeAlbum');
Route::get('test_slider','DocumentController@testSlider');
});