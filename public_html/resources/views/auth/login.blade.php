<!DOCTYPE html>
<html lang="en-US">
 @include('front_end.header')
   <body class="size-1140">
      <!-- TOP NAV WITH LOGO -->  
     
      <section>
         <div id="head">
            <div class="line">
               <h1>Welcome To Sufian Nasser Login</h1>
            </div>
         </div>

         <div id="content" class="left-align contact-page">
            <div class="line">
   @if(session()->has('message_sent'))
   <div style="color: #3c763d;
    background-color: #dff0d8;
    border-color: #d6e9c6;margin-top: 18px; padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;">{{ session('message_sent') }}</div>
         @endif
               <div class="margin">

                  
                <center>  <div class="s-12 l-12">

                     <h2>LogIn</h2>

    @if ($errors->any())
    <div style="background-color: #f2dede; border-color:#ebccd1; color:#a94442; margin-top: 18px; padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <br>
@endif
          <div class="center"> <form class="customform" role="form" method="POST" action="{{ url('/login') }}">
               {{csrf_field()}}
                        <div class="s-12 l-7"><input name="email" placeholder="Your e-mail" title="Your e-mail" type="email" /></div>

                     <div class="s-12 l-7"><input name="password" placeholder="Password" title="Password" type="password" />

                     </div>
                     <div class="s-12 l-7">
                    <a href="/forget_password">Forget Password ?</a>

                     </div>
                   
                  
                    

                        <div class="s-12 m-6 l-12">
 <br>
                  
                          <button type="submit">Log In</button></div>
                     </form> </div>
                  </div> </center>
               </div>
            </div>
         </div>
         
      </section>
      <br>
      <!-- FOOTER -->   
  @include('front_end.footer')
   </body>
</html>