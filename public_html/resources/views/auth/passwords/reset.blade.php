<!DOCTYPE html>
<html lang="en-US">
 @include('front_end.header')
   <body class="size-1140">
      <!-- TOP NAV WITH LOGO -->  
     
      <section>
         <div id="head">
            <div class="line">
               <h1>Reset Password</h1>
            </div>
         </div>

         <div id="content" class="left-align contact-page">
            <div class="line">

               <div class="margin">

                  
                <center>  <div class="s-12 l-12">

                     <h2>Reset Password</h2>

    @if ($errors->any())
    <div style="background-color: #f2dede; border-color:#ebccd1; color:#a94442; margin-top: 18px; padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <br>
@endif
          <div class="center"> 

                     <form method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}" class="customform">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

    <div class="s-12 l-7"><input name="email" placeholder="Your e-mail" title="Your e-mail" type="email" /></div>

                         <div class="s-12 l-7"><input name="password" placeholder="Your Password" title="Your Password" type="password" /></div>

                         <div class="s-12 l-7"><input name="password_confirmation" placeholder="Confirm Password" title="Confirm Password" type="password" /></div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                   </div>
                  </div> </center>
               </div>
            </div>
         </div>
         
      </section>
      <br>
      <!-- FOOTER -->   
  @include('front_end.footer')
   </body>
</html>