<style type="text/css">
   ::-webkit-scrollbar-track
{
 -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
 background-color: #F5F5F5;
}
::-webkit-scrollbar
{
 width: 10px;
 background-color: #F5F5F5;
}
::-webkit-scrollbar-thumb
{
 background-color: #142A83; 
 background-image: -webkit-linear-gradient(90deg,
                                           rgba(255, 255, 255, .2) 25%,
             transparent 25%,
             transparent 50%,
             rgba(255, 255, 255, .2) 50%,
             rgba(255, 255, 255, .2) 75%,
             transparent 75%,
             transparent)
}

</style>

<div style="overflow: hidden; overflow-x: hidden; overflow-y: hidden;"> 
  @include('front_end.header')
   <body class="size-1140">
   
 
      <section>
         <!-- CAROUSEL -->  	
         <div id="carousel">
            <div id="owl-demo" class="owl-carousel owl-theme">
               
              <?php 
               $today_date  = date('Y-m-d');
               $get_first_image = App\SliderShow::where('first_photo',1)->first();
              $get_all_images = App\SliderShow::where('first_photo',0)->orderBy('id','desc')->get(); ?>

              @if($get_first_image)
               <div class="item">
                  <img src="{{Storage::url($get_first_image->image_path)}}">
         <?php  $get_all_news = App\LastNews::where('active',1)->orderBy('id','desc')->get(); ?>
         <div class="carousel-text">
            <div style="margin-left: 30px; width: 20%; color: black; height: 300px; overflow: scroll; overflow-x: hidden;">
               @foreach($get_all_news as $news)
               <h2 style="color: black; background: rgba(0, 0, 0, 0); font-style: italic;">{{$news->main_subject}}</h2>
               <div>

                     {{$news->description}}
                  </div>
               @endforeach
            </div>
         </div> 
               </div>
              @endif
               @foreach($get_all_images as $image)
               @if($image->end_date == NULL or $image->end_date > $today_date)
               <div class="item">
                  <img src="{{Storage::url($image->image_path)}}">      
              <?php  $get_all_news = App\LastNews::where('active',1)->orderBy('id','desc')->get(); ?>
              <div class="carousel-text">
            <div style="margin-left: 30px; width: 20%; color: black; height: 300px; overflow: scroll; overflow-x: hidden;">
               @foreach($get_all_news as $news)
               <h2 style="color: black; background: rgba(0, 0, 0, 0); font-style: italic;">{{$news->main_subject}}</h2>
               <div>
                     {{$news->description}}
                  </div>
               @endforeach
            </div>
         </div> 

               </div>
               @endif
               @endforeach
            </div>
         </div>
         <!-- FIRST BLOCK --> 	
        <?php /* ?> <div id="first-block">
            <div class="line">
               <h2>Some awesome blocks</h2>
               <p class="subtitile">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
               </p>
               <div class="margin">
                  <div class="s-12 m-6 l-3 margin-bottom">
                     <i class="icon-paperplane_ico icon2x"></i>
                     <h3>About</h3>
                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                     </p>
                  </div>
                  <div class="s-12 m-6 l-3 margin-bottom">
                     <i class="icon-star icon2x"></i>
                     <h3>Company</h3>
                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                     </p>
                  </div>
                  <div class="s-12 m-6 l-3 margin-bottom">
                     <i class="icon-message icon2x"></i>
                     <h3>Services</h3>
                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                     </p>
                  </div>
                  <div class="s-12 m-6 l-3 margin-bottom">
                     <i class="icon-mail icon2x"></i>
                     <h3>Contact</h3>
                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                     </p>
                  </div>
               </div>
            </div>
         </div> <?php */ ?>
         <!-- SECOND BLOCK --> 	
        <?php /* ?> <div id="second-block">
            <div class="line">
               <div class="margin-bottom">
                  <div class="margin">
                     <article class="s-12 l-8 center">
                        <h1>Amazing title</h1>
                        <p class="margin-bottom">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                        </p>
                        <a class="button s-12 l-4 center" href="product.html">Read more</a>  			
                     </article>
                  </div>
               </div>
            </div>
         </div> <?php */ ?>
         <!-- GALLERY --> 	
         <?php /* <div id="third-block">
            <div class="line">
              <h2>Responsive gallery</h2>
               <p class="subtitile">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
               </p>
               <div class="margin">
                  <?php /*<div class="s-12 m-6 l-3">
                     <img src="img/first-small.jpg" alt="alternative text">      
                     <p class="subtitile">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                     </p>
                  </div>*/ ?>
              <?php /*   <center> 
                  <?php $sufian = 'public/website_logos/sufian_nasser.jpg' ?>
                     <img src="{{Storage::url($sufian)}}" style="width: 700px;">      
                     <p style="font-size: 20px; color: black;">
                        <h3>CEO MESSAGE</h3>

As I look at the growth over the years since our inception in 2003,
<br> I am extremely proud of what we have achieved, and even more excited about our outlook and about our promising future.
<br>
<font style="font-size: 20px; color: #1E90FF;">Sufian Nasser &amp; Sons</font>
, We are committed to building long lasting relationships based on integrity, performance, value, and customer satisfaction.<br>
We provide the means to guiding our client’s projects to completion on time and within budget, whilst always maintaining unparalleled safety and quality standards and service.
<br>
For over 25 years we have developed our skills and our professional excellence of works in electro-mechanical, roads work, bridges, communications civil and telecom works, and intersections & tunnels work. <br>
Our greatest asset is our loyal, dedicated and hardworking team. We strive to continue to be recognized as an employer of choice and have established programs designed to retain and attract highly skilled and motivated professional managers and workforce.
<br>
We thank the clients we have served over the last 25 years and look forward to many more future endeavor’s and opportunities. <br><br>
We truly value all of the strong relationships we have developed and will continue to nurture those relationships in the years to come. <br>
Staying true to our guiding code of ethics, trustworthiness and professionalism are the common threads that bond our employees and company together. <br>
 Our commitment to the construction industry is unwavering, and we embrace our future with enthusiasm. 
<br>
We will keep on expanding our business while focusing on meeting our clients needs and expectations in terms of Quality, Budget, Time, and Zero Accident record.
<br>
For Construction at its best, you can rely on us!
<br>
<b>Sufian Nasser CEO </b>

                     </p>
               </center> */?>
                <?php /*  <div class="s-12 m-6 l-3">
                     <img src="img/third-small.jpg" alt="alternative text">      
                     <p class="subtitile">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                     </p>
                  </div>
                  <div class="s-12 m-6 l-3">
                     <img src="img/fourth-small.jpg" alt="alternative text">      
                     <p class="subtitile">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                     </p>
                  </div>
               </div>
            </div>
         </div> */ ?>
       <?php /* ?>  <div id="fourth-block">
            <div class="line">
               <div id="owl-demo2" class="owl-carousel owl-theme">
                  <div class="item">
                     <h2>Amazing responsive template</h2>
                     <p class="s-12 m-12 l-8 center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                     </p>
                  </div>
                  <div class="item">
                     <h2>Responsive components</h2>
                     <p class="s-12 m-12 l-8 center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                     </p>
                  </div>
                  <div class="item">
                     <h2>Retina ready</h2>
                     <p class="s-12 m-12 l-8 center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit  lobortis nisl ut aliquip ex ea commodo consequat.
                     </p>
                  </div>
               </div>
            </div>
         </div> <?php */ ?>
      </section>
      <!-- FOOTER -->   
     
   <?php /*  @include('front_end.footer')*/ ?>
       
  
   </body>
 </div>  
</html>