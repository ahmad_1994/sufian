@include('back_end.app')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Contact Us Messages
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">Contact Us Messages</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              @if(session()->has('message_read'))
   <div class="alert alert-success">{{ session('message_read') }}</div>
         @endif

           @if(session()->has('message_deleted'))
           <?php 
           $id = substr(session('message_deleted'), strpos(session('message_deleted'), "!") + 1);
            $message = str_replace($id, '', session('message_deleted'));    
            ?>
  <div class="alert alert-danger">{{$message}} <a href="/undo_contact_message_delete/{{$id}}">Undo</a> </div>
           @endif

            @if(session()->has('undo_successfully'))
   <div class="alert alert-success">{{ session('undo_successfully') }}</div>
         @endif

           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Contact Us Messages</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile Number</th>
                        <th>Subject</th>
                        <th>Message</th>
                        <th>Status</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($get_all_messages as $message)
                      <tr>
                        <td>{{$message->name}}</td>
                        <td>{{$message->email}}</td>
                        <td>{{$message->mobile_number}}</td>
                        <td>{{$message->subject}}</td>
                        <td>{{$message->message}}</td>
                        <td>@if($message->read_or_not == 1)
                          Message Read By {{$message->ReadUser->name}}
                          @elseif($message->read_or_not == 0)
                          Message Not Read
                        @endif</td>
                        <td>
                          <form action="/set_message_read" method="post">
                            {{csrf_field()}}
                            
                <input type="hidden" name="message_id" value="{{$message->id}}">
                @if($message->read_or_not == 0)
            <input type="submit" name="submit" value="Mark As Read" class="btn btn-md btn-primary btn-block">
            @elseif($message->read_or_not == 1)
             <input type="submit" name="submit" value="Mark As Not Read" class="btn btn-md btn-primary btn-block">
             @endif
            </form>

                          <form method="post" action="/delete_contact_message">
{{csrf_field()}}
                              <input type="hidden" name="message_id" value="{{$message->id}}">
    <input type="submit" name="submit" value="Delete" class="btn btn-md btn-danger btn-block">
                         </form>

                <a href="/message/{{$message->id}}" class="btn btn-block btn-md btn-info">View Message</a>

           
                        </td>
                      </tr>
                      @endforeach 
                    </tbody>
                   
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@include('back_end.footer')