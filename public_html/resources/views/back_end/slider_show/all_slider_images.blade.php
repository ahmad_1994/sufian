@include('back_end.app')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            View All Images
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">All Images</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
            	 @if ($errors->any())
    <div class="alert alert-warning alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <br>
@endif

              @if(session()->has('ended_successfully'))
   <div class="alert alert-success">{{ session('ended_successfully') }}</div>
         @endif

         @if(session()->has('first_photo'))
   <div class="alert alert-success">{{ session('first_photo') }}</div>
         @endif
         

          @if(session()->has('delete_slider'))
   <div class="alert alert-danger">Slider show deletee successfully <a href="/undo_delete_slider/{{ session('delete_slider') }}">Undo</a> </div>
         @endif
         	 @if(session()->has('undo_slider'))
   <div class="alert alert-success">{{ session('undo_slider') }}</div>
         @endif


	         
              
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Images</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Image</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Added by</th>
                        <th>Created At</th>
                        <th>Actions</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($get_all_images as $image)
                      <tr>
                        <td>
                          <img src="{{Storage::url($image->image_path)}}" width="400px;">
                        </td>
                        <td>{{$image->start_date}}</td>
                        <td>
                        @if($image->end_date == NULL)
                        <form action="/end_slide_show" method="post">
                        	{{csrf_field()}}
                        	<input type="hidden" name="slider_id" value="{{$image->id}}">
                        <input type="date" name="end_date" class="form-control">
                        <input type="submit" name="submit" value="End Date" class="btn btn-md btn-warning">
                    </form>
                    @else
                    	{{$image->end_date}} <?php /*<b>Ended By :  ?>{{$image->EndedBy->name}} */?></b>
                        @endif
                      </td>
                        <td>
                          {{$image->AddedUser->name}}
                          </td>
                          
                    
                       
                        <td>
                         {{$image->created_at}}
                        </td>
                        <td>
                        	<form action="/delete_slider_image" method="post">
                        		{{csrf_field()}}
                        		<input type="hidden" name="slider_id" value="{{$image->id}}">
                        		<input type="submit" name="submit" value="Delete Slider Image" class="btn btn-md btn-danger">
                        	</form>

                          @if($image->first_photo != 1)
                          <br>
                          <a href="/set_first_slider/{{$image->id}}" class="btn btn-md btn-primary btn-block">Set First Slider</a>
                          @endif
                        </td>
                      </tr>
                      @endforeach 
                    </tbody>
                   
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@include('back_end.footer')