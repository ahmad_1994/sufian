@include('back_end.app')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            {{$get_one_news->main_subject}}

                
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">News</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="col-xs-12">
          <div class="row">
             @if(session()->has('news_added'))
   <div class="alert alert-success">{{ session('news_added') }}</div>
         @endif

         @if(session()->has('success'))
   <div class="alert alert-success">{{ session('success') }}</div>
         @endif

         
      

        


          @if ($errors->any())
    <div class="alert alert-warning alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <br>
@endif
          
 <div class="col-md-12">
              
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">News Information</h3>
                </div><!-- /.box-header -->
               
                  
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">
                      <form action="/update_news" method="post">
                        {{csrf_field()}}
                    <div class="col-md-6">
                      
                        <div class="form-group">
                        <label>Main Subject</label>
                        <input type="text" name="main_subject" class="form-control" value="{{$get_one_news->main_subject}}">
                    </div>
                    
              
</div>
<div class="col-md-6">
                      
                        <div class="form-group">
                         <label>News Description</label>
                   <textarea class="form-control" name="description" 
                    rows="5" cols="30" style="resize: none;">
                    {{$get_one_news->description}}
                   </textarea>
                    </div>
                    
              
</div>

<input type="submit" name="submit" value="Update News" class="btn btn-block btn-success btn-md">
<input type="hidden" name="news_id" value="{{$get_one_news->id}}">
</form>
</div>
                    </div>
                    
               
                  </div><!-- /.box-body -->

                 
                
              </div><!-- /.box -->

     <a href="/last_news" class="btn btn-primary btn-md btn-block">Back To All News</a>

           </div>
            </div><!-- /.col -->
          </div><!-- /.row -->
        
        </section><!-- /.content -->
      
      </div><!-- /.content-wrapper -->

     
@include('back_end.footer')