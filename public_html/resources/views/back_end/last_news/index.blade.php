@include('back_end.app')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            View All Last News
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">All News</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
            	 @if ($errors->any())
    <div class="alert alert-warning alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <br>
@endif

             @if(session()->has('delete_news'))
   <div class="alert alert-danger">News deleted successfully ! <a href="/undo_delete_news/{{ session('delete_news') }}">Undo</a></div>
         @endif

          @if(session()->has('undo_success'))
   <div class="alert alert-success">{{ session('undo_success') }}</div>
         @endif
         
         @if(session()->has('active_news'))
   <div class="alert alert-success">{{ session('active_news') }}</div>
         @endif
         
 



	         
              
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All News</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>News Subject</th>
                        <th>Description</th>
                        <th>Status</th> 
                        <th>Added by</th>
                        <th>Created At</th>
                        <th>Actions</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($get_all_news as $news)
                      <tr>
                        <td>{{$news->main_subject}}</td>
                        <td>
                          {{$news->description}}
                          
                        </td>
                        
                          <td>
                            @if($news->active == 1)
                            Active
                            @else
                            Not Active
                            @endif
                          </td>
                        <td>
                        {{$news->AddedBy->name}}
                          
                      </td>
                        <td>
                          {{$news->created_at}}
                          </td>
                          
                    
                       
                    
                        <td>
                        	<a href="/last_news/{{$news->id}}" class="btn btn-info btn-md">View News</a>
                          <br>

                       
                  <a href="/delete_news/{{$news->id}}" class="btn btn-danger btn-md">Delete News</a>
                  <br>
                  @if($news->active == 0)
                  <a href="/active_news/{{$news->id}}" class="btn btn-md btn-success">Active News</a>
                  @elseif($news->active == 1)
                  <a href="/active_news/{{$news->id}}" class="btn btn-md btn-warning">Deactive News</a>
                  @endif
                        </td>
                        
                      </tr>
                      @endforeach 
                    </tbody>
                   
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@include('back_end.footer')