@include('back_end.app')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Create New User
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">Create New User</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            
              <div class="col-md-12">
                 @if ($errors->any())
    <div class="alert alert-warning alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <br>
@endif
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">New User</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="/add_user">
                  {{csrf_field()}}
                  <div class="box-body">
                    <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                      <label for="exampleInputFile">Name</label>
                      <input type="text" class="form-control" name="name" placeholder="Enter Name" value="{{old('name')}}">
                    
                    </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                      <label for="exampleInputEmail1">Email address</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="email" value="{{old('email')}}">
                    </div> 
                    </div></div>
                    <div class="row">
                      <div class="col-md-6">
                   <div class="form-group">
                      <label for="exampleInputPassword1">Password</label>
                      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
                    </div>
                      </div>

                      <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputFile">Re-Enter Password</label>
                      <input type="password" class="form-control" name="password_confirmation" placeholder="Re-Enter Password">
                    
                    </div>
                      </div>
                    </div>
                    
                
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-success btn-block">Create User</button>
                  </div>
                </form>
              </div><!-- /.box -->

    

           
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@include('back_end.footer')