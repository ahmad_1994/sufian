@include('back_end.app')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            View All USers
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">All Users</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              @if(session()->has('user_added'))
   <div class="alert alert-success">{{ session('user_added') }}</div>
         @endif

          @if(session()->has('user_active'))
   <div class="alert alert-success">{{ session('user_active') }}</div>
         @endif
              
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Users</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Added By</th>
                        <th>Created At</th>
                        <th>Actions</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($get_all_users as $user)
                      <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                        @if($user->added_by != NULL)
                        <?php $check_user = App\User::find($user->added_by); ?>
                        @if($check_user)
                        {{$check_user->name}}
                        @endif
                        @endif
                      </td>
                        <td>{{$user->created_at}}</td>
                    
                       
                        <td>
                         <a href="edit_user/{{$user->id}}" class="btn btn-primary btn-block btn-md">Edit</a> 
                        @if($user->active == 1)
                         <a href="/active_user/{{$user->id}}" class="btn btn-md btn-block btn-warning">Deactivate</a>
                        @else
                        <a href="/active_user/{{$user->id}}" class="btn btn-md btn-block btn-success">Active</a>
                         @endif
                        </td>
                      </tr>
                      @endforeach 
                    </tbody>
                   
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@include('back_end.footer')