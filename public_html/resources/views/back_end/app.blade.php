<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Sufian Nasser & Sons</title>
    <?php $website_logo = 'public/website_logos/one.png';
      $logo = 'public/website_logos/one.png'; ?>
      <link rel="shortcut icon" type="image/png" href="{{Illuminate\Support\Facades\Storage::url($website_logo)}}"/>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="/back_end/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- DATA TABLES -->
    <link href="/back_end/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="/back_end/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="/back_end/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue">
    <div class="wrapper">
      
      <header class="main-header">
        <a href="/" class="logo"><b>Sufian Nasser</b></a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
           <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
             <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                  <?php $get_count_of_messages = 
                  App\contactMessage::where('read_or_not',0)->count();
                  $get_messages_not_read = App\contactMessage::where('read_or_not',0)->get();
                   ?>
                   @if($get_count_of_messages > 0)
                  <span class="label label-success">

                  {{$get_count_of_messages}}
                
              </span>
              @endif
                </a>
                <ul class="dropdown-menu">
                  <li class="header">
                    @if($get_count_of_messages > 0)
                  You have {{$get_count_of_messages}} messages
                  @else

                  No Messages 
                   @endif</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      @foreach($get_messages_not_read as $message)
                      <?php $first_letter = substr($message->name, 0,1);
                      $first_letter = strtoupper($first_letter);
    $path = 'public/letters/'.$first_letter.'.'.'jpg'; 
                       ?>
                      <li><!-- start message -->
                        <a href="/message/{{$message->id}}">
                          <div class="pull-left">
                <img src="{{Storage::url($path)}}" class="img-circle" alt="User Image"/>
                          </div>
                          <h4>
                              {{$message->subject}}
                            <small><i class="fa fa-clock-o"></i></small>
                          </h4>
                          <p></p>
                        </a>
                      </li><!-- end message -->
                      @endforeach
                      
                    
                    
                      
                      </ul>
                  </li>
                  <li class="footer"><a href="/contact_us_messages">See All Messages</a></li>
                </ul>
              </li>
              <!-- Notifications: style can be found in dropdown.less -->
             <?php /*  <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning">10</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 10 notifications</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li>
                        <a href="#">
                          <i class="fa fa-users text-aqua"></i> 5 new members joined today
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-users text-red"></i> 5 new members joined
                        </a>
                      </li>

                      <li>
                        <a href="#">
                          <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-user text-red"></i> You changed your username
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer"><a href="#">View all</a></li>
                </ul>
              </li> <?php */ ?>
              <!-- Tasks: style can be found in dropdown.less -->
             <?php /*  <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-flag-o"></i>
                  <span class="label label-danger">9</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 9 tasks</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Design some buttons
                            <small class="pull-right">20%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">20% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Create a nice theme
                            <small class="pull-right">40%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">40% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Some task I need to do
                            <small class="pull-right">60%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">60% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                      <li><!-- Task item -->
                        <a href="#">
                          <h3>
                            Make beautiful transitions
                            <small class="pull-right">80%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">80% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                    </ul>
                  </li>
                  <li class="footer">
                    <a href="#">View all tasks</a>
                  </li>
                </ul>
              </li> <?php */ ?>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                   <?php $get_user_image = App\ProfilPhotos::where('user_id',Auth::User()->id)->where('default_image',1)->orderby('id','desc')->first(); ?>
                   @if($get_user_image)
                  <img src="{{Storage::url($get_user_image->image_path)}}" class="user-image" alt="User Image"/>
                  @endif
                  <span class="hidden-xs">
                    <?php $get_user = App\User::find(Auth::User()->id); ?>
                      @if($get_user)
                      {{$get_user->name}}
                      @endif

                    </span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    @if($get_user_image)
                    <img src="{{Storage::url($get_user_image->image_path)}}" class="img-circle" alt="User Image" />
                    @endif
                    <p>
                          Website Admin
                      <?php /*<small>Member since Nov. 2012</small>*/ ?>
                    </p>
                  </li>
                  <!-- Menu Body -->
                 <?php /* <li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li>*/ ?>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="/my_profile" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="/logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li> 
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
         <?php /* <div class="user-panel">
            <div class="pull-left image">
              <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p>Alexander Pierce</p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>*/ ?>
          <!-- search form -->
         <?php /* <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>*/ ?>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <?php /*<li class="header">MAIN NAVIGATION</li>*/ ?>
      <li><a href="/home"><i class="fa fa-tachometer"></i> Dashboard</a></li>
            <li><a href="/contact_us_messages"><i class="fa fa-comments"></i> Contact Us Messages</a></li>


           <li class="treeview">
              <a href="#">
                <i class="fa fa-users"></i> <span>Users</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
          <li><a href="/create_user"><i class="fa fa-user-plus"></i>Create New User</a></li>
          <li><a href="/all_users"><i class="fa fa-user"></i>View All Users</a></li>
              </ul>
            </li>

             <li class="treeview">
              <a href="#">
                <i class="fa fa-picture-o"></i> <span>Slider Show Images</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
          <li><a href="/create_slider_show"><i class="fa fa-upload"></i>Upload New Image</a></li>
          <li><a href="/all_slider_show"><i class="fa fa-eye"></i>View All Imges</a></li>
              </ul>
            </li>

           <?php /* <li class="treeview">
              <a href="#">
                <i class="fa fa-picture-o"></i> <span>Photos</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
          <li><a href="/create_photo"><i class="fa fa-file-image-o"></i>Upload New Photo</a></li>
          <li><a href="/all_photos"><i class="fa fa-eye"></i>View All Photos</a></li>
              </ul>
            </li>*/ ?>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-picture-o"></i> <span>Albums</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
          <li><a href="{{route('albums.create')}}"><i class="fa fa-file-image-o"></i>Create New Album</a></li>
          <li><a href="{{route('albums.index')}}"><i class="fa fa-eye"></i>View All Albums</a></li>
              </ul>
            </li>


             <li class="treeview">
              <a href="#">
                <i class="fa fa-file-video-o"></i> <span>Uppload Videos</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
          <li><a href="/upload_video"><i class="fa fa-upload"></i>Upload New Video</a></li>
          <li><a href="/all_videos"><i class="fa fa-video-camera"></i>View All Videos</a></li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-book"></i> <span>Documents</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
          <li><a href="/create_document"><i class="fa fa-file-text-o"></i>Create New Document</a></li>
          <li><a href="/all_documents"><i class="fa fa-eye"></i>View All Document</a></li>
              </ul>
            </li>

              <li class="treeview">
              <a href="#">
                <i class="fa fa-list-ol"></i> <span>Projects</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
          <li><a href="{{route('projects.create')}}"><i class="fa fa-plus"></i>Create New Project</a></li>
          <li><a href="{{route('projects.index')}}"><i class="fa fa-eye"></i>View All Projects</a></li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-newspaper-o"></i> <span>Last News</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
          <li><a href="{{route('last_news.create')}}"><i class="fa fa-plus"></i>Create New News</a></li>
          <li><a href="{{route('last_news.index')}}"><i class="fa fa-eye"></i>View All News</a></li>
              </ul>
            </li>

            <li><a href="/my_profile"><i class="fa fa-user"></i>My Profile</a></li>


            <?php /* ?><li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Layout Options</span>
                <span class="label label-primary pull-right">4</span>
              </a>
              <ul class="treeview-menu">
                <li><a href="../layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
                <li><a href="../layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
                <li><a href="../layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
                <li><a href="../layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
              </ul>
            </li><?php */ ?>
           <?php /* <li>
              <a href="../widgets.html">
                <i class="fa fa-th"></i> <span>Widgets</span> <small class="label pull-right bg-green">new</small>
              </a>
            </li>*/ ?>
           <?php /* <li class="treeview">
              <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>Charts</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="../charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
                <li><a href="../charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
                <li><a href="../charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
              </ul>
            </li>*/ ?>
          <?php /*  <li class="treeview">
              <a href="#">
                <i class="fa fa-laptop"></i>
                <span>UI Elements</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="../UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>
                <li><a href="../UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>
                <li><a href="../UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>
                <li><a href="../UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>
                <li><a href="../UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>
                <li><a href="../UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>
              </ul>
            </li>*/ ?>
           <?php /* <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Forms</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="../forms/general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
                <li><a href="../forms/advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
                <li><a href="../forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
              </ul>
            </li>*/ ?>
           <?php /* <li class="treeview active">
              <a href="#">
                <i class="fa fa-table"></i> <span>Tables</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
                <li class="active"><a href="data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
              </ul>
            </li>*/ ?>
          <?php /*  <li>
              <a href="../calendar.html">
                <i class="fa fa-calendar"></i> <span>Calendar</span>
                <small class="label pull-right bg-red">3</small>
              </a>
            </li>*/ ?>
            <?php /*<li>
              <a href="../mailbox/mailbox.html">
                <i class="fa fa-envelope"></i> <span>Mailbox</span>
                <small class="label pull-right bg-yellow">12</small>
              </a>
            </li>*/ ?>
           <?php /* <li class="treeview">
              <a href="#">
                <i class="fa fa-folder"></i> <span>Examples</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="../examples/invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>
                <li><a href="../examples/login.html"><i class="fa fa-circle-o"></i> Login</a></li>
                <li><a href="../examples/register.html"><i class="fa fa-circle-o"></i> Register</a></li>
                <li><a href="../examples/lockscreen.html"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
                <li><a href="../examples/404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>
                <li><a href="../examples/500.html"><i class="fa fa-circle-o"></i> 500 Error</a></li>
                <li><a href="../examples/blank.html"><i class="fa fa-circle-o"></i> Blank Page</a></li>
              </ul>
            </li>*/ ?>
            <?php /* ?><li class="treeview">
              <a href="#">
                <i class="fa fa-share"></i> <span>Multilevel</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                <li>
                  <a href="#"><i class="fa fa-circle-o"></i> Level One <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                    <li>
                      <a href="#"><i class="fa fa-circle-o"></i> Level Two <i class="fa fa-angle-left pull-right"></i></a>
                      <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
              </ul>
            </li><?php */ ?>
           
            <?php /*<li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-circle-o text-danger"></i> Important</a></li>
            <li><a href="#"><i class="fa fa-circle-o text-warning"></i> Warning</a></li>
            <li><a href="#"><i class="fa fa-circle-o text-info"></i> Information</a></li>*/ ?>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Right side column. Contains the navbar and content of the page -->
      
      <?php /*@include('back_end.footer')
    </div><!-- ./wrapper -->*/ ?>

    


