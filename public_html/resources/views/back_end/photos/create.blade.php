@include('back_end.app')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Create New Photos
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">Create New Photo</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            
              <div class="col-md-12">
                 @if ($errors->any())
    <div class="alert alert-warning alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <br>
@endif
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">New Photo</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="/add_photo" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <div class="box-body">
                    <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                      <label for="exampleInputFile">Choose Photo</label>
              <input type="file" class="form-control" name="file[]" multiple="true">
                    
                    </div>
                    </div>
                
            
</div>
                    </div>
                    
                   <div class="box-footer">
                      <button type="submit" class="btn btn-success btn-block">Upload Photos</button>
                    </div>
                  </div><!-- /.box-body -->

                 
                </form>
              </div><!-- /.box -->

    

           
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@include('back_end.footer')