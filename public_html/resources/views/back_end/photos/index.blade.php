@include('back_end.app')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            View All Photos
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">All Photos</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
            	 @if ($errors->any())
    <div class="alert alert-warning alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <br>
@endif

              @if(session()->has('image_upload'))
   <div class="alert alert-success">{{ session('image_upload') }}</div>
         @endif

          @if(session()->has('delete_photo'))
   <div class="alert alert-danger">Photo deleted successfully <a href="/undo_delete_photo/{{ session('delete_photo') }}">Undo</a> </div>
         @endif
         	 @if(session()->has('undo_photo'))
   <div class="alert alert-success">{{ session('undo_photo') }}</div>
         @endif


	         
              
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Photos</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Photo</th>
                        <th>Album</th>
                        <th>Added by</th>
                        <th>Created At</th>
                        <th>Actions</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($get_all_photos as $photo)
                      <tr>
                        <td>
                          <img src="{{Storage::url($photo->image_path)}}" width="400px;">
                        </td>
                        
                       <td>
                         <form method="post" action="/change_album">
                           <input type="hidden" name="photo_id" value="{{$photo->id}}">
                           <select name="album_id" class="form-control">
                             @foreach($get_active_album as $album)
                             <option value="{{$album->id}}">{{$album->album_name}}</option>
                             @endforeach
                           </select>
                           <input type="submit" name="change_album" value="Change Album" class="btn btn-md btn-info">
                           {{csrf_field()}}
                         </form>

                         @if($photo->album_id != NULL)
                          {{$photo->Album->album_name}}
                         @elseif($photo->album_id == NULL)
                         Choose Album
                         @endif
                       </td>
                        <td>
                          {{$photo->AddedBy->name}}
                          </td>
                          
                    
                       
                        <td>
                         {{$photo->created_at}}
                        </td>
                        <td>
                        <a href="/delete_photo/{{$photo->id}}" class="btn btn-danger btn-block btn-lg">Delete Photo</a>
                        </td>
                      </tr>
                      @endforeach 
                    </tbody>
                   
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@include('back_end.footer')