@include('back_end.app')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php $get_user = App\User::find(Auth::User()->id); ?>

            My Profile @if($get_user) {{$get_user->name}} @endif
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">@if($get_user){{$get_user->name}}@endif</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="col-xs-12">
          <div class="row">
             @if(session()->has('profile_image_uploade'))
   <div class="alert alert-success">{{ session('profile_image_uploade') }}</div>
         @endif

          @if ($errors->any())
    <div class="alert alert-warning alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <br>
@endif
         @if(session()->has('new_password_not_match'))
   <div class="alert alert-danger">{{ session('new_password_not_match') }}</div>
         @endif 
         @if(session()->has('old_password_not_match'))
   <div class="alert alert-danger">{{ session('old_password_not_match') }}</div>
         @endif 

         @if(session()->has('old_password_and_new_password'))
   <div class="alert alert-danger">{{ session('old_password_and_new_password') }}</div>
         @endif 

           @if(session()->has('new_password_not_matched'))
   <div class="alert alert-danger">{{ session('new_password_not_matched') }}</div>
         @endif  

         @if(session()->has('success_change_password'))
   <div class="alert alert-success">{{ session('success_change_password') }}</div>
         @endif

          @if(session()->has('image_set_success'))
   <div class="alert alert-success">{{ session('image_set_success') }}</div>
         @endif 

         @if(session()->has('delete_profile_image'))
   <div class="alert alert-success">{{ session('delete_profile_image') }}</div>
         @endif  

         

         

             
            
              <div class="col-md-4">
              
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Upload My Photo</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="/upload_profile_photo" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <div class="box-body">
                    <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                      <label for="exampleInputFile">Choose Image</label>
              <input type="file" class="form-control" name="image">
                    
                    </div>
                    </div>
                    
              
</div>
                    </div>
                    
                   <div class="box-footer">
                      <button type="submit" class="btn btn-success btn-block">Upload Image</button>
                    </div>
                  </div><!-- /.box-body -->

                 
                </form>
              </div><!-- /.box -->

              <div class="col-md-4">
              
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Change My Password</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="/change_my_password">
                  {{csrf_field()}}
                  <div class="box-body">
                    <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                      <label for="exampleInputFile">Current Password</label>
              <input type="password" class="form-control" name="old_password">
                    
                    </div>
                    </div>
                    
              
</div>
<div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                      <label for="exampleInputFile">New Password</label>
              <input type="password" class="form-control" name="new_password">
                    
                    </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                      <label for="exampleInputFile">Confirm Password</label>
              <input type="password" class="form-control" name="new_password_confirmation">
                    
                    </div>
                    </div>
                    
              
</div>

                    </div>
                    
                   <div class="box-footer">
                      <button type="submit" class="btn btn-success btn-block">Change Password</button>
                    </div>
                  </div><!-- /.box-body -->

                 
                </form>
              </div><!-- /.box -->

      <div class="col-md-4">
        <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Change My Name</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="/change_my_name">
                  {{csrf_field()}}
                  <div class="box-body">
                    <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                      <label for="exampleInputFile">Name</label>
              <input type="text" class="form-control" name="name" value="{{$get_user->name}}">
                <input type="hidden" name="user_id" value="{{$get_user->id}}">    
                    </div>
                    </div>
                    
              
</div>

                    </div>
                    
                   <div class="box-footer">
                      <button type="submit" class="btn btn-success btn-block">Change Password</button>
                    </div>
                  </div><!-- /.box-body -->
        
      </div>

           </div>
            </div><!-- /.col -->



              <div class="row">
                @foreach($get_all_photos_for_the_account as $photo)
            <div class="col-md-4">
              <!-- Default box -->

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">
                    @if($photo->default_image == 0)
                    <a href="/set_image_default/{{$photo->id}}" class="btn btn-md btn-success">Set As Default</a>
                    @endif
                    <a href="/delete_profile_image/{{$photo->id}}" class="btn btn-md btn-danger">Delete</a>
                  </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body" style="display: block;">
                  
                  <p>
                   <img src="{{Storage::url($photo->image_path)}}" height="300px" width="300px"> 
                  </p>
                </div><!-- /.box-body -->
               <?php /* <div class="box-footer" style="display: block;">
                  <code>.box-footer</code>
                </div><!-- /.box-footer-->*/ ?>
              </div><!-- /.box -->
            </div><!-- /.col -->
            @endforeach
          </div>
          </div><!-- /.row -->
        
        </section><!-- /.content -->
      
      </div><!-- /.content-wrapper -->
@include('back_end.footer')