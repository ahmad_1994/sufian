@include('back_end.app')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>

            Message From  {{$get_message->name}}
           </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">Message</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
          <div class="col-xs-12">
                       
            
              <div class="col-md-12">
              <div class="row">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Contact Us Message</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
        
               
                  <div class="box-body">
                    <div class="row">
                    <div class="col-md-12">
                      <tr><b>From : </b> <td>{{$get_message->name}}</td></tr>
                      <br>
                      <br>
                    <tr><b>Mobile Number : </b> <td>{{$get_message->mobile_number}}</td></tr>
                    <br>
                    <br>
                    <tr><b>Message Date And Time : </b> <td>{{$get_message->created_at}}</td></tr>
                    <br>
                    <br>
                    <tr><b>Subject : </b> <td>{{$get_message->subject}}</td></tr>
                    <br>
                    
                    <br>
                    <tr><b>Message : </b> <td>{{$get_message->message}}</td></tr>
                     </div>

                     <center>
                     <form action="/set_message_read" method="post">
                            {{csrf_field()}}
                            
                <input type="hidden" name="message_id" value="{{$get_message->id}}">
                @if($get_message->read_or_not == 0)
            <input type="submit" name="submit" value="Mark As Read" class="btn btn-md btn-primary btn-block">
            @elseif($get_message->read_or_not == 1)
             <input type="submit" name="submit" value="Mark As Not Read" class="btn btn-md btn-primary">
             @endif
            </form>
                 </center>
              
</div>
                    </div>
                    
                  </div><!-- /.box-body -->

                 
        
      </div>
       </div>
    </div>
  </div>
</section>
</div>

@include('back_end.footer')