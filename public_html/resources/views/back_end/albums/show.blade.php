@include('back_end.app')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            {{$find_album->album_name}}

                
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">Album</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="col-xs-12">
          <div class="row">
             @if(session()->has('image_upload'))
   <div class="alert alert-success">{{ session('image_upload') }}</div>
         @endif

         @if(session()->has('updated'))
   <div class="alert alert-success">{{ session('updated') }}</div>
         @endif

@if(session()->has('choose_image'))
   <div class="alert alert-warning">{{ session('choose_image') }}</div>
         @endif
         
      

          @if(session()->has('delete_photo'))
   <div class="alert alert-danger"> Image deleted successfully <a href="/undo_delete_document_image/{{ session('delete_photo') }}">Undo</a> </div>
         @endif


          @if ($errors->any())
    <div class="alert alert-warning alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <br>
@endif
          
             
 <div class="col-md-6">
              
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Album Information</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                  
                  <div class="box-body">
                    <div class="row">
                    <div class="col-md-12">
                      <div class="row">
                        <div class="col-md-4">
                         <b>Album Name : </b>
                      </div>  <form action="/update_album" method="post">
                      <div class="col-md-4">
                       
                          {{csrf_field()}}
                          <input type="hidden" name="album_id" value="{{$find_album->id}}">
                        <input type="text" name="album_name" class="form-control" value="{{$find_album->album_name}}">
                      </div>
                      <div class="col-md-4">
                        <input type="submit" name="submit" class="btn btn-primary btn-md" value="Change Name">

                      </div>
                    </form>
                    </div>
             
              <tr><b>Added By : </b> {{$find_album->AddedBy->name}}</tr>
              @if($find_album->updated_by != 0 and $find_album->updated_by != NULL)
              <br>
              <br>
              <tr><b>Updated By : </b> {{$find_album->UpdatedBy->name}}</tr>
              @endif
             
             


                    </div>
                    
              
</div>
                    </div>
                    
                   
                  </div><!-- /.box-body -->

                 
                
              </div><!-- /.box -->

      <div class="col-md-6">
              
              <!-- general form elements -->
               <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">New Photo</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="/add_photo" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <div class="box-body">
                    <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                      <label for="exampleInputFile">Choose Photo</label>
              <input type="file" class="form-control" name="file[]" multiple="true">
                    <input type="hidden" name="album_id" value="{{$find_album->id}}">
                    </div>
                    </div>
                
            
</div>
                    </div>
                    
                   <div class="box-footer">
                      <button type="submit" class="btn btn-success btn-block">Upload Photos</button>
                    </div>
                  </div><!-- /.box-body -->
                 
                
              </div><!-- /.box -->

           </div>
<a href="/albums" class="btn btn-block btn-info btn-md">Back To All Albums</a>
<br>
            
            </div><!-- /.col -->







              <div class="row">
                @foreach($get_all_photos_in_album as $photo)
            <div class="col-md-4">
              <!-- Default box -->

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">
                    <a href="/delete_photo/{{$photo->id}}" class="btn btn-md btn-danger">Delete</a>
                  </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body" style="display: block;">
                  
                  <p>
                   <img src="{{Storage::url($photo->image_path)}}" height="300px" width="300px"> 
                  </p>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            @endforeach
          </div> 
          </div><!-- /.row -->
        
        </section><!-- /.content -->
      
      </div><!-- /.content-wrapper -->

   

@include('back_end.footer')