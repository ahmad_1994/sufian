@include('back_end.app')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            View All Albums
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">All Albums</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
            	 @if ($errors->any())
    <div class="alert alert-warning alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <br>
@endif

 @if(session()->has('active'))
   <div class="alert alert-success">{{ session('active') }}</div>
         @endif
           


	         
              
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Albums</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Album name</th>
                        <th>Status</th>
                        <th>Added by</th>
                        <th>Created At</th>
                        <th>Actions</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($get_all_albums as $album)
                      <tr>
                        <td>{{$album->album_name}}</td>
                        <td>
                          @if($album->active == 1)
                          Active
                          @elseif($album->active == 0)
                          Not Active
                          @endif
                        </td>
                        
                        <td>
                        {{$album->AddedBy->name}}
                      </td>
                        <td>
                          {{$album->created_at}}
                          </td>
                          
                    
                       
                    
                        <td>
                        	<a href="albums/{{$album->id}}" class="btn btn-info btn-md">View Album</a>
                          @if($album->active == 1)
                    <a href="/active_album/{{$album->id}}" class="btn btn-warning btn-md">Deactive</a>
                    @elseif($album->active == 0)
                     <a href="/active_album/{{$album->id}}" class="btn btn-success btn-md">Active</a>
                     @endif
                        </td>
                      </tr>
                      @endforeach 
                    </tbody>
                   
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@include('back_end.footer')