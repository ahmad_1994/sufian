@include('back_end.app')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            View All Videos
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">All Videos</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
            	 @if ($errors->any())
    <div class="alert alert-warning alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <br>
@endif

              @if(session()->has('success_video'))
   <div class="alert alert-success">{{ session('success_video') }}</div>
         @endif

          @if(session()->has('delete_video'))
   <div class="alert alert-danger">Slider show deletee successfully <a href="/undo_delete_video/{{ session('delete_video') }}">Undo</a> </div>
         @endif
         	 @if(session()->has('undo_video'))
   <div class="alert alert-success">{{ session('undo_video') }}</div>
         @endif


	         
              
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Videos</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Video</th>
                        <th>Subject</th>
                        <th>Description</th>
                        <th>Added by</th>
                        <th>Created At</th>
                        <th>Actions</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($get_all_videos as $video)
                      <tr>
                        <td>
  <video width="320" height="240" controls controlsList="nodownload">
  <source src="{{Storage::url($video->video_path)}}" type="video/mp4">
</video>
                        </td>
                        <td>
                          {{$video->video_subject}}
                        </td>
                        <td>
                        {{$video->video_description}}
                      </td>
                        <td>
                        {{$video->AddedBy->name}}  
                        </td>
                          
                    
                       
                        <td>
                          {{$video->created_at}}
                        </td>
                        <td>
                        	<form method="post" action="/delete_video">
                            {{csrf_field()}}
          <input type="hidden" name="video_id" value="{{$video->id}}">
        <input type="submit" name="submit" class="btn btn-danger btn-block btn-md" value="Delete Video">
                          </form>
                        </td>
                      </tr>
                      @endforeach 
                    </tbody>
                   
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@include('back_end.footer')