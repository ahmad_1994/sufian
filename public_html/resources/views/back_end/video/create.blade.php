@include('back_end.app')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Upload New Video
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">Create New Video</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            
              <div class="col-md-12">
                 @if ($errors->any())
    <div class="alert alert-warning alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <br>
@endif
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">New Video</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="/upload_new_video" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <div class="box-body">
                    <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                      <label for="exampleInputFile">Choose Video</label>
              <input type="file" class="form-control" name="video">
                    
                    </div>
                    </div>

                     <div class="col-md-3">
                      <div class="form-group">
                      <label for="exampleInputFile">Video Subject</label>
              <input type="text" class="form-control" name="video_subject">
                    
                    </div>
                    </div>

                     <div class="col-md-6">
                      <div class="form-group">
                      <label for="exampleInputFile">Video Description</label>
              <textarea class="form-control" name="video_description" style="resize: none;" cols="10" rows="5">
                
              </textarea>
                    
                    </div>
                    </div>
                   
</div>
                    </div>
                    
                   <div class="box-footer">
                      <button type="submit" class="btn btn-success btn-block">Upload Video</button>
                    </div>
                  </div><!-- /.box-body -->

                 
                </form>
              </div><!-- /.box -->

    

           
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@include('back_end.footer')