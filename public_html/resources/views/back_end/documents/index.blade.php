@include('back_end.app')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            View All Documents
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">All Documents</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
            	 @if ($errors->any())
    <div class="alert alert-warning alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <br>
@endif

            @if(session()->has('document_delete'))
   <div class="alert alert-danger">Document deleted successfully <a href="/undo_document_delete/{{ session('document_delete') }}">Undo</a></div>
         @endif

         @if(session()->has('undo_document_delete'))
   <div class="alert alert-success">{{ session('undo_document_delete') }}</div>
         @endif


	         
              
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Documents</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Document name</th>
                        <th>Document Image</th>
                        <th>Added by</th>
                        <th>Created At</th>
                        <th>Actions</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($get_all_documents as $doc)
                      <tr>
                        <td>{{$doc->document_name}}</td>
                        <td>
                          <img src="{{Storage::url($doc->document_image_path)}}" width="300px;">
                        </td>
                        
                        <td>
                        {{$doc->AddedBy->name}}
                      </td>
                        <td>
                          {{$doc->created_at}}
                          </td>
                          
                    
                       
                    
                        <td>
                        	<a href="/document/{{$doc->id}}" class="btn btn-info btn-md">View Document</a>
                          <form action="/delete_documents" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="document_id" value="{{$doc->id}}">
                            <input type="submit" name="submit" value="Delete Document" class="btn btn-md btn-danger">
                          </form>
                        </td>
                      </tr>
                      @endforeach 
                    </tbody>
                   
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@include('back_end.footer')