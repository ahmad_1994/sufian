@include('back_end.app')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            {{$find_album->album_name}}

                
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">Album</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="col-xs-12">
          <div class="row">
             @if(session()->has('image_uploade'))
   <div class="alert alert-success">{{ session('image_uploade') }}</div>
         @endif

         @if(session()->has('success'))
   <div class="alert alert-success">{{ session('success') }}</div>
         @endif

         
      

          @if(session()->has('delete_photo'))
   <div class="alert alert-danger"> Image deleted successfully <a href="/undo_delete_document_image/{{ session('delete_photo') }}">Undo</a> </div>
         @endif


          @if ($errors->any())
    <div class="alert alert-warning alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <br>
@endif
          
              
 <div class="col-md-6">
              
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Album Information</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                  
                  <div class="box-body">
                    <div class="row">
                    <div class="col-md-12">
                      <div class="row">
                        <div class="col-md-4">
                         <b>Album Name : </b>
                      </div> 
                      <div class="col-md-4">
                        <form action="/edit_document_name" method="post">
                          {{csrf_field()}}
                          <input type="hidden" name="document_id" value="{{$find_document->id}}">
                        <input type="text" name="document_name" class="form-control" value="{{$find_document->document_name}}">
                      </div>
                      <div class="col-md-4">
                        <input type="submit" name="submit" class="btn btn-primary btn-md" value="Change Name">

                      </div>
                    </form>
                    </div>
             
              <tr><b>Added By : </b> {{$find_document->AddedBy->name}}</tr>
              @if($find_document->updated_by != 0 and $find_document->updated_by != NULL)
              <br>
              <br>
              <tr><b>Updated By : </b> {{$find_document->UpdatedBy->name}}</tr>
              @endif
              <br>
              <br>
              <tr><b>Document Date And Time : </b> {{$find_document->created_at}}</tr>
              <br>
              <br>
              <tr><b>Document Image : </b> 
<button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#document_image">View Image</button>

                    </div>
                    
              
</div>
                    </div>
                    
                    </form>
                  </div><!-- /.box-body -->

                 
                
              </div><!-- /.box -->

     

           </div>
            </div><!-- /.col -->



              <div class="row">
                @foreach($get_all_images_in_doc as $photo)
            <div class="col-md-4">
              <!-- Default box -->

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">
                    <a href="/delete_document_image/{{$photo->id}}" class="btn btn-md btn-danger">Delete</a>
                  </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body" style="display: block;">
                  
                  <p>
                   <img src="{{Storage::url($photo->image_path)}}" height="300px" width="300px"> 
                  </p>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            @endforeach
          </div>
          </div><!-- /.row -->
        
        </section><!-- /.content -->
      
      </div><!-- /.content-wrapper -->

      <!-- Modal -->
<div id="document_image" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{$find_document->document_name}} Image </h4>
      </div>
      <div class="modal-body">
        <center>
        <img src="{{Storage::url($find_document->document_image_path)}}" width="500px;" height="500px;">
        </center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@include('back_end.footer')