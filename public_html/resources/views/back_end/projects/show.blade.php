@include('back_end.app')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            {{$get_project->name}}

                
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">Project</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="col-xs-12">
          <div class="row">
             @if(session()->has('image_uploade'))
   <div class="alert alert-success">{{ session('image_uploade') }}</div>
         @endif

         @if(session()->has('success_added_image'))
   <div class="alert alert-success">{{ session('success_added_image') }}</div>
         @endif

          @if(session()->has('updated'))
   <div class="alert alert-success">{{ session('updated') }}</div>
         @endif
         
      

          @if(session()->has('delete_photo'))
   <div class="alert alert-danger"> Image deleted successfully <a href="/undo_delete_project_image/{{ session('delete_photo') }}">Undo</a> </div>
         @endif


          @if ($errors->any())
    <div class="alert alert-warning alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <br>
@endif
          
            <?php /* ?>  <div class="col-md-3">
              
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Upload Project Photos</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="/upload_project_image" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <div class="box-body">
                    <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                      <label for="exampleInputFile">Choose File</label>
              <input type="file" class="form-control" name="file[]" multiple="true">
              <input type="hidden" name="project_id" value="{{$get_project->id}}">
                    
                    </div>
                    </div>
                    
              
</div>
                    </div>
                    
                   <div class="box-footer">
                      <button type="submit" class="btn btn-success btn-block">Upload File</button>
                    </div>
                    </form>
                  </div><!-- /.box-body -->

                 
                
              </div><!-- /.box -->  */ ?>
 <div class="col-md-12">
              
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Project Information</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                  
                  <div class="box-body">
                    <div class="row">
                    <div class="col-md-12">
                <tr><b>Added By : </b> {{$get_project->AddedBy->name}}</tr>
              @if($get_project->updated_by != 0 and $get_project->updated_by != NULL)
              <br>
              <br>
              <tr><b>Updated By : </b> {{$get_project->UpdatedBy->name}}</tr>
              @endif
              <br>
              <br>
              <tr><b>Project Date And Time : </b> {{$get_project->created_at}}</tr>
              <br>
              <br>
              @if($get_project->project_main_subject != NULL)
              <tr><b>Project Main Subject: </b> {{$get_project->project_main_subject}}</tr>
              <br><br>
              @endif
              @if($get_project->project_description != NULL)
              <tr><b>Project Description :</b> 
                <?php 

                $desc = $get_project->project_description;
               
                $y = str_replace(':','</h3>',$desc );
            $x = str_replace('.',"\n",$desc );
              $z = nl2br($x);
                 ?>
                 <div>
                  <?php echo $z; ?>        
                   
                 </div>
             

              </tr>
              <br>
              <br>
              @endif
              <tr><b>Project Video : </b> 
<button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#document_video">View Video</button>

<a href="/projects/edit/{{$get_project->id}}" class="btn btn-warning btn-md">Edit Project</a>

                    </div>
                    
              
</div>
                    </div>
                    
                    </form>
                  </div><!-- /.box-body -->

                 
                
              </div><!-- /.box -->

     

           </div>
            </div><!-- /.col -->



              <div class="row">

                <div class="col-md-12">
                  <a href="/projects" class="btn btn-md btn-block btn-primary">Back To Projects</a>
                </div>
            <?php /* ?>    @foreach($get_all_images_in_project as $photo)
            <div class="col-md-4">
              <!-- Default box -->

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">
                    <a href="/delete_project_image/{{$photo->id}}" class="btn btn-md btn-danger">Delete</a>
                  </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body" style="display: block;">

                  
                  <p>
                     <?php $tmp = explode('.',$photo->project_image);
                $extention = end($tmp);
           ?>
           @if($extention == 'png' or $extention == 'jpg' or $extention == 'jpeg')
        
        <img src="{{Storage::url($photo->project_image)}}" width="300px;" height="300px;">
      
        @elseif($extention == 'mp4' or $extention == 'flv')
        <video width="300px;" height="300px;" controls controlsList="nodownload">
  <source src="{{Storage::url($photo->project_image)}}" type="video/mp4">
</video>
        @endif
                    
                  </p>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            @endforeach
          </div><?php */ ?>
          </div><!-- /.row --> 
        
        </section><!-- /.content -->
      
      </div><!-- /.content-wrapper -->

      <!-- Modal -->
<div id="document_video" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{$get_project->name}} Video </h4>
      </div>
      <div class="modal-body">
        <center>

       <video width="500px;" height="500px;" controls controlsList="nodownload">
  <source src="{{Storage::url($get_project->project_image_path)}}" type="video/mp4">
</video>
        </center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@include('back_end.footer')