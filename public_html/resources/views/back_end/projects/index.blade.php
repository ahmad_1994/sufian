@include('back_end.app')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            View All Projects
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">All Projects</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
            	 @if ($errors->any())
    <div class="alert alert-warning alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <br>
@endif

            
  @if(session()->has('delete_project'))
   <div class="alert alert-danger"> Project deleted successfully <a href="/undo_delete_project/{{ session('delete_project') }}">Undo</a> </div>
         @endif

  @if(session()->has('undo_delete_project'))

  <div class="alert alert-success">{{ session('delete_project') }}</div>
  @endif



	         
              
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Projects</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Project name</th>
                        <th>Project Main Video</th>
                         
                        <th>Added by</th>
                        <th>Created At</th>
                        <th>Actions</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($get_all_projects as $project)
                      <tr>
                        <td>{{$project->name}}</td>
                        <td>
                           <video width="300px;" height="200px;" controls controlsList="nodownload">
  <source src="{{Storage::url($project->project_image_path)}}" type="video/mp4">
</video>
                          
                        </td>
                        
                        
                        <td>
                        {{$project->AddedBy->name}}
                      </td>
                        <td>
                          {{$project->created_at}}
                          </td>
                          
                    
                       
                    
                        <td>
                        	<a href="/projects/{{$project->id}}" class="btn btn-info btn-md">View Project</a>
                          <br>

                        <a href="/projects/edit/{{$project->id}}" class="btn btn-warning btn-md">Edit Project</a>
                        <br>
                        <a href="/delete_project/{{$project->id}}" class="btn btn-danger btn-md">Delete Project</a>
                        </td>
                        
                      </tr>
                      @endforeach 
                    </tbody>
                   
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@include('back_end.footer')