@include('back_end.app')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            {{$get_project->name}}

                
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">Edit Project</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="col-xs-12">
          <div class="row">
             @if(session()->has('image_uploade'))
   <div class="alert alert-success">{{ session('image_uploade') }}</div>
         @endif

         @if(session()->has('updated'))
   <div class="alert alert-success">{{ session('updated') }}</div>
         @endif

         
      

         


          @if ($errors->any())
    <div class="alert alert-warning alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <br>
@endif

 <div class="col-md-12">
              
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Project Information</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                  
                  <div class="box-body">
                    <div class="row">
                      <form method="post" action="/update_project" enctype="multipart/form-data">
                        {{csrf_field()}}
                    <div class="col-md-12">
                      <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                        <label>Project Name</label>
                        <input type="text" name="name" value="{{$get_project->name}}" class="form-control">
                      </div>
                      </div>
                     <div class="col-md-6">
                      <div class="form-group">
                        <label>Project Main Subject</label>
                        <input type="text" name="project_main_subject" value="{{$get_project->project_main_subject}}" class="form-control">
                      </div>
                    </div>

                   
</div>
<div class="row">
  <div class="col-md-6">
    <div class="form-group">
    <label>Choose Project Video</label>
    <input type="file" name="image" class="form-control">
  </div>
  </div>

    <div class="col-md-6">
                      <div class="form-group">
                        <label>Project Description</label>
                  <textarea class="form-control" rows="5" cols="30" name="project_description" style="resize: none;">
                        {{$get_project->project_description}}
                  </textarea>
                      </div>
                    </div>
</div>
<input type="hidden" name="project_id" value="{{$get_project->id}}">
<input type="submit" name="submit" value="Edit Project" class="btn btn-block btn-primary btn-lg">
                    </div>
                  </form>
                    
              
</div>
                    </div>
                    
                              </div><!-- /.box-body -->

                 
                
              </div><!-- /.box -->

     

           </div>
            </div><!-- /.col -->



              <div class="row">

          </div><!-- /.row --> 
        
        </section><!-- /.content -->
      
      </div><!-- /.content-wrapper -->
@include('back_end.footer')