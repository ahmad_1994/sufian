 <footer>
         <div class="line">
            <div class="s-12 l-6">
               <p>Copyright 2018, Sufian Nasser & Sons
               </p>
            </div>
            <div class="s-12 l-6">
               <p class="right" style="margin-right: 30px;">
                  <?php  if (Auth::check()) {
                     $get_user = App\User::find(Auth::User()->id);
                   ?>
                
                <a href="/home">{{$get_user->name}}</a>
                |
                <a href="/logout">Logout</a>


                  <?php } else {

                     ?>
         <a href="/login">Log In</a>
                  <?php } ?>


               </p>
            </div>
         </div>
      </footer>