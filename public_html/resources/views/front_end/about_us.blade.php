@include('front_end.header')

 <section>
         <div id="head">
            <div class="line">
               <h1>About Us</h1>
            </div>
         </div>
         <div id="content">
            <div class="line">
               <?php // history ?>
               <h2>CEO MESSAGE</h2>
               <center> <div class="s-12 m-12 l-9 center">
                  <?php $sufian = 'public/website_logos/sufian_nasser.jpg' ?>
                     <img src="{{Storage::url($sufian)}}" style="width: 700px;"> 
                     <br>

                  As I look at the growth over the years since our inception in 2003,
<br> I am extremely proud of what we have achieved, and even more excited about our outlook and about our promising future.
<br>
<font style="font-size: 20px; color: #1E90FF;">Sufian Nasser &amp; Sons</font>
, We are committed to building long lasting relationships based on integrity, performance, value, and customer satisfaction.<br>
We provide the means to guiding our client’s projects to completion on time and within budget, whilst always maintaining unparalleled safety and quality standards and service.
<br>
For over 25 years we have developed our skills and our professional excellence of works in electro-mechanical, roads work, bridges, communications civil and telecom works, and intersections & tunnels work. <br>
Our greatest asset is our loyal, dedicated and hardworking team. 

<br>We strive to continue to be recognized as an employer of choice and have established programs designed to retain and attract highly skilled and motivated professional managers and workforce.
<br>
We thank the clients we have served over the last 25 years and look forward to many more future endeavor’s and opportunities. <br>
We truly value all of the strong relationships we have developed and will continue to nurture those relationships in the years to come. <br>
Staying true to our guiding code of ethics, trustworthiness and professionalism are the common threads that bond our employees and company together. <br>
 Our commitment to the construction industry is unwavering, and we embrace our future with enthusiasm. 
<br>
We will keep on expanding our business while focusing on meeting our clients needs and expectations in terms of Quality, Budget, Time, and Zero Accident record.
<br>
For Construction at its best, you can rely on us!
<br>
<b style="font-size: 20px;">Sufian Nasser <br>
CEO </b>
                 </div> </center>
               

                <?php /* <h2>Our History</h2>
               <p class="s-12 m-12 l-8 center">
   Company was established in the year 2003 under the name of <b>Sufian Nasser & Sons</b> and was operating in country ever sense.
   <br>
Company classification is 1st grade for (Bridges-Underpasses & Communication) and a 2nd grade for (Infrastructure) as per the standards of Ministry of Public Works and Housing.
<br>
Company was performing communication projects ways as a main contractor utilizing company owned machines.   Company owners own all machinery, equipment and tools.
<br>
Company is run by specialized trained staff throughout the structure to carry out missions. Our great believe is to always implement projects with our own resources in complete (micro-trenchers, excavators, loaders, dumpers, mixers, trucks, compacters, Vehicles, and related supporting equipment), along with company owned technical maintenance facility to raise operational readiness.
<br>
Major Projects in communication infrastructure that counts ways over two thousand kilometers were performed by the company for the past few years at the national level. 
<br>
Available Micro-trenching machines were utilized to deliver different trench specifications that ranges from (4cm-15cm width) & (20cm-30cm Depth) in order to satisfy our clients’ needs. Similar tasks were performed utilizing traditional excavators. This has distinguished us as one of the leading companies in providing the local market with the micro-trenching and trenching techniques.
<br>
Company as a main contractor had implemented fully many major projects that were delivered to specifications at the specified time. All these projects are implemented fully by us through major companies holding 1st grade in the communication profession. One main reason for holding this responsibility with success is the fact that our company has the experience and owns all needed resources to carry out such missions



               </p> */ ?>
               <?php // mission ?>
               <h2>Our Vision</h2>
               <p class="s-12 m-12 l-9 center" style="font-size:17px; ">
    We Are here to build the future for you , With you .<br>
   
Our work has a significant impact on the quality of people's lives, both for today and tomorrow.
Recognizing this responsibility, the whole team at SN&SONS always makes sure to render all projects according to the highest quality and security standards, on time, and within budget, and exceed the expectations of our customers, partners, shareholders, and future generations ...

               </p>
             <?php /*  <h2>Offered Services :</h2>

     <li> Design-Implement projects</li>

   <li>Bridges and Underpasses</li> 
     <li> Projects Management</li>


  <li> Micro-trenching </li>

  <li> Communication</li>



 <li> Site surveying</li>
</p>
               </p>*/ ?>
              
            </div>
         </div>


         <?php /*
         <!-- GALLERY -->  
         <div id="third-block">
            <div class="line">
               <h2>Responsive gallery</h2>
               <p class="subtitile">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
               </p>
               <div class="margin">
                  <div class="s-12 m-6 l-3">
                     <img src="img/first-small.jpg">      
                     <p class="subtitile">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                     </p>
                  </div>
                  <div class="s-12 m-6 l-3">
                     <img src="img/second-small.jpg">      
                     <p class="subtitile">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                     </p>
                  </div>
                  <div class="s-12 m-6 l-3">
                     <img src="img/third-small.jpg">      
                     <p class="subtitile">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                     </p>
                  </div>
                  <div class="s-12 m-6 l-3">
                     <img src="img/fourth-small.jpg">      
                     <p class="subtitile">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                     </p>
                  </div>
               </div>
            </div>
         </div><?php */ ?>
        <?php /* <div id="fourth-block">
            <div class="line">
               <div id="owl-demo2" class="owl-carousel owl-theme">
                  <div class="item">
                     <h2>Amazing responsive template</h2>
                     <p class="s-12 m-12 l-8 center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                     </p>
                  </div>
                  <div class="item">
                     <h2>Responsive components</h2>
                     <p class="s-12 m-12 l-8 center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                     </p>
                  </div>
                  <div class="item">
                     <h2>Retina ready</h2>
                     <p class="s-12 m-12 l-8 center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                     </p>
                  </div>
               </div>
            </div>
         </div> */ ?>
      </section>
@include('front_end.footer')