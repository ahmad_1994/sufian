@include('front_end.header')

<style type="text/css">
   .font_class{
      font-size: 30px;
   }

</style>
 <section>
         <div id="head">
            <div class="line">
               <h1>Videos</h1>
            </div>
         </div>
         <div id="content">
            <div class="line">
               <div class="margin">
                   @foreach($get_all_videos as $video)
                     <center>
                  <div class="s-12 l-6">
             <font class="font_class">{{$video->video_subject}}</font>
             
           <div> <video width="500" height="350" controls controlsList="nodownload">
  <source src="{{Storage::url($video->video_path)}}" type="video/mp4">
</video></div>
                  </div>
                  </center>
                  @endforeach
               
               </div>
            </div>
         </div>
      </section>
@include('front_end.footer')