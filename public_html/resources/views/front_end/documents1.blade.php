@include('front_end.header')

 <section>
         <div id="head">
            <div class="line">
               <h1></h1>
            </div>
         </div>
         <div id="content">
            <div class="line">
              <center>
                
               
                
                  @foreach($get_all_documents as $document)
     <div class="s-12 l-6">
      <div class="w3-content w3-display-container">
              <?php $get_imges = App\DocumentImage::where('document_id',$document->id)->get(); ?>
              @if(count($get_imges) > 0)
        <div class="w3-display-container mySlides{{$document->id}}">
          <img src="{{Storage::url($document->document_image_path)}}" style="width:50%; height: 30%;">
        </div>  
        @foreach($get_imges as $image)
         <div class="w3-display-container mySlides{{$document->id}}">
          <img src="{{Storage::url($image->image_path)}}" style="width:50%; height: 30%;">
        </div>
        @endforeach
        @endif 
        
        <button class="w3-button w3-display-left w3-black" onclick="plusDivs(-1)">&#10094;</button>
<button class="w3-button w3-display-right w3-black" onclick="plusDivs(1)">&#10095;</button>
       </div>
       
</div>
      @endforeach



                
                
              </center>     
            </div>
         </div>


      </section>
      @foreach($get_all_documents as $document)
      <script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides{{$document->id}}");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}
</script>
@endforeach

@include('front_end.footer')