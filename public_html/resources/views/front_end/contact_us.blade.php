<!DOCTYPE html>
<html lang="en-US">
 @include('front_end.header')
   <body class="size-1140">
      <!-- TOP NAV WITH LOGO -->  
     
      <section>
         <div id="head">
            <div class="line">
               <h1>Contact Us</h1>
            </div>
         </div>

         <div id="content" class="left-align contact-page">
            <div class="line">
   @if(session()->has('message_sent'))
   <div style="color: #3c763d;
    background-color: #dff0d8;
    border-color: #d6e9c6;margin-top: 18px; padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;">{{ session('message_sent') }}</div>
         @endif
               <div class="margin">

                  <div class="s-12 l-6">
                     <h2>Contact Information</h2>
                     <address>
                  <a href="geo:31.9758551,35.853109399999994"><p><i class="icon-home icon"></i>Amman - Jabr Commercial Complex, Abdullah Ghosheh St 125</p></a>
                      <?php /* <p><i class="icon-globe_black icon"></i> Slovakia - Europe</p>*/ ?>
                        <p><i class="icon-mail icon"></i>
                           <a href="mailto:info@sufiannasser.com">info@sufiannasser.com</a></p>
                     </address>
                     <br />
                    <?php /* <h2>Social Information</h2>
                     <p><i class="icon-facebook icon"></i> <a href="">Sofian Nasser & Sons</a></p>
                     <p><i class="icon-facebook icon"></i> <a href="https://www.facebook.com/myresponsee">Responsee</a></p>
                     <p class="margin-bottom"><i class="icon-twitter icon"></i> <a href="https://twitter.com/MyResponsee">Responsee</a></p>*/ ?>
                  </div>
                  <div class="s-12 l-6">

                     <h2>Contact Us Message</h2>

    @if ($errors->any())
    <div style="background-color: #f2dede; border-color:#ebccd1; color:#a94442; margin-top: 18px; padding: 15px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-radius: 4px;">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <br>
@endif
            <form class="customform" action="/send_message_contact_us" method="post">

               {{csrf_field()}}
                        <div class="s-12 l-7"><input name="email" placeholder="Your e-mail" title="Your e-mail" type="text" /></div>

                        <div class="s-12 l-7"><input name="name" placeholder="Your name" title="Your name" type="text" /></div>

                        <div class="s-12 l-7"><input name="mobile_number" placeholder="Your Phone" title="Your Phone" type="text" /></div>

                        

                        <div class="s-12 l-7"><input name="subject" placeholder="Subject" title="Subject" type="text" /></div>

                        <div class="s-12"><textarea placeholder="Your massage" name="message" rows="5" style="resize: none;"></textarea></div>

                        <div class="s-12 m-6 l-12"><button type="submit">Send Message</button></div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <!-- MAP -->
         <center>	
         <div id="map-block">  	  
           <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6760.069674567486!2d36.098863532181745!3d32.09534412296948!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x151ca1a305d801bd%3A0xe2c468eac5870271!2sSufian+Al+Naser+%26+Sons+Co!5e0!3m2!1sar!2sjo!4v1530273642489" width="1100" height="700" frameborder="0" style="border:0" allowfullscreen></iframe>
         </div></center>
         
      </section>
      <br>
      <!-- FOOTER -->   
  @include('front_end.footer')
   </body>
</html>