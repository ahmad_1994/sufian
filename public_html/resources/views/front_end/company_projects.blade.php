<!DOCTYPE html>
<html lang="en-US">
 @include('front_end.header')
   <body class="size-1140">
      <!-- TOP NAV WITH LOGO -->  
     
      <section>
         <div id="head">
            <div class="line">
               <h1>Company Projects</h1>
            </div>
         </div>

         <div id="content" class="left-align contact-page">
            <div class="line">
            	@foreach($get_all_projects as $project)
               <div class="margin">
               	
                 
                  <div class="s-12 l-4">
                     <h2>{{$project->name}}</h2>
                     @if($project->project_main_subject != NULL)
                     <b>{{$project->project_main_subject}} : </b>
                    
                     @endif 
                     <br>
                    @if($project->project_image_path != NULL)
              <video width="300px;" height="200px;" controls controlsList="nodownload">
  <source src="{{Storage::url($project->project_image_path)}}" type="video/mp4">
</video>
                    @endif
                    </div>
                    
                  
               </div>@endforeach
            </div>
         </div>
     
         
      </section>
      <!-- FOOTER -->   
  @include('front_end.footer')
   </body>
</html>