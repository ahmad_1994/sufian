@include('front_end.header')
<style type="text/css">


   #ul_photo{
position: relative;
top: 50%;
left: 75%;
transform: translate(-50%,-50%);
display: flex;
margin: 0;
padding: 0;
background: #fff;
   }
   #li_photo{
   	list-style: none;
   	display: block;
   	width: 40px;
   	text-align: center;
   	line-height: 40px;
   	background: #2fcbe0;
   	color: white;
   	text-decoration: none;
   	border-radius: 4px;
   	margin: 3px;
   	box-shadow: inset 0 5px 10px rgba(0,0,0,.1),0 2px 5px rgba(0,0,0,.5);
   }
   .center {
    margin: auto;
    width: 60%;
    padding: 10px;

}

.marginLR
{
	margin-left: 10px;
margin-right:10px;
}

   
</style>
 <section>
         <div id="head">
            <div class="line">
               <h1>{{$get_album->album_name}}</h1>
            </div>
         </div>
         <div id="content">
            <div class="line marginLR">
            	<div class="margin">
               @foreach($get_all_photos as $photo)
               <div class="s-6 l-3">
                <a href="{{Storage::url($photo->image_path)}}"> <img src="{{Storage::url($photo->image_path)}}"></a>
                </div>
               
               @endforeach
               <br>
           </div>
               <br>
            
            </div>


            
            <div class="center">
            	<br>
               		<br>
                 {{$get_all_photos->links()}}

               </div>
           
            
         </div>

               
           
      </section>
@include('front_end.footer')