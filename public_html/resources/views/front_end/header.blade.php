 <!DOCTYPE html>
<html lang="en-US">
 <head>
 	
    	<?php $website_logo = 'public/website_logos/one.png';
      $logo = 'public/website_logos/one.png'; ?>
    <link rel="shortcut icon" type="image/png" href="{{Illuminate\Support\Facades\Storage::url($website_logo)}}"/>  
      <title>Sufian Nasser & Sons</title>
     
      <link rel="stylesheet" href="/css/components.css">
      <link rel="stylesheet" href="/css/responsee.css">
      <link rel="stylesheet" href="/css/w3school.css">
     
      <link rel="stylesheet" href="/owl-carousel/owl.carousel.css">
      <link rel="stylesheet" href="/owl-carousel/owl.theme.css">
      
      <!-- CUSTOM STYLE -->  
      <link rel="stylesheet" href="/css/template-style.css">
      <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
      <script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
      
      <script type="text/javascript" src="/js/jquery-ui.min.js"></script>    
      <script type="text/javascript" src="/js/modernizr.js"></script>
      <script type="text/javascript" src="/js/responsee.js"></script> 
       
      <script type="text/javascript" src="/owl-carousel/owl.carousel.js"></script> 
       <script type="text/javascript">
         jQuery(document).ready(function($) {  
           $("#owl-demo").owlCarousel({
            slideSpeed : 300,
            autoPlay : true,
            navigation : false,
            pagination : false,
            singleItem:true
           });
           $("#owl-demo2").owlCarousel({
            slideSpeed : 300,
            autoPlay : true,
            navigation : false,
            pagination : true,
            singleItem:true
           });
         });   
          
      </script>
      <!--[if lt IE 9]>
         <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
      <![endif]-->
   </head>
 <!-- TOP NAV WITH LOGO -->  
      <header>
         <nav>
            <div class="line">
               <div class="top-nav">              
                  <div class="logo hide-l">
                     <a href=""></a>
                  </div>        
   <?php /*> <img src="{{Illuminate\Support\Facades\Storage::url($website_logo)}}">        */ ?> 
                  <p class="nav-text">Custom menu text</p>
                  <div class="top-nav s-12 l-5">
                     <ul class="right top-ul chevron">
                        <li><a href="/">Home</a>
                        </li>
                        <li><a href="/about_us">About Us</a>
                        </li>
                        <li><a href="/contact_us">Contact Us</a>
                        </li>
                     </ul>
                  </div>
                  <ul class="s-12 l-2">
                     <li class="logo hide-s hide-m">
                        <?php /*<a href="../design/"> <br /><strong></strong></a>*/ ?>
         <img src="{{Illuminate\Support\Facades\Storage::url($logo)}}" 

            >
                     </li>
                  </ul>
                  <div class="top-nav s-12 l-5">
                     <ul class="top-ul chevron">

                      <li><a href="/company_projects">Projects</a>
                        </li>
                        <?php /*
                        if (Auth::check()) {
                         ?>
                       <?php /* <li><a href="/home"><?php $get_user = App\User::find(Auth::User()->id); ?>
                           @if($get_user)
                           {{$get_user->name}}
                           @endif

                        </a>
                        </li>
                         <li>
                           <a href="/home">
                              <?php $get_user = App\User::find(Auth::User()->id); ?>
                           @if($get_user)

                           {{$get_user->name}}
                           @endif
                           </a>           
                           <ul>
                           
                              <li><a href="/home"> Welcome To Dashboard</a>
                              </li>
                              <li>
                                 <a href="/logout"> Log Out</a>             
                               
                              </li>
                           </ul>
                        </li>*/ ?>
                     <?php /*}
                     else{


                      ?>
                       <li><a href="/login">Log In</a>
                        </li>

                   <?php } */ ?>
                        <li>
                           <a>Galary</a>			    
                           <ul>
                            <li><a href="/album">Photos</a>
                              </li>
                              <li><a href="/videos">Videos</a>
                              </li>
                             
                           </ul>
                        </li>
                        <li><a href="/company_documents">Documents</a>
                        </li>
                     </ul> 
                  </div>
               </div>
            </div>
         </nav>
      </header>
      </html>