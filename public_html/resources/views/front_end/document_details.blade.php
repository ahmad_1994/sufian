@include('front_end.header')

 <section>
         <div id="head">
            <div class="line">
               <h1>{{$get_document->document_name}}</h1>
            </div>
         </div>
         <div id="content">
            <div class="line">
              <center>
                <a href="/company_documents"><h4 style="color: #2fcbe0;">Back To Documents</h4></a>
                <br>
                <div class="w3-content w3-display-container">
    <div class="w3-display-container mySlides">
  <a href="{{Storage::url($get_document->document_image_path)}}"><img src="{{Storage::url($get_document->document_image_path)}}" style="width:50%"></a>

</div>  
            
          @foreach($get_document_images as $image)
          <div class="w3-display-container mySlides">
          <a href="{{Storage::url($image->image_path)}}"><img src="{{Storage::url($image->image_path)}}" style="width:50%; height: 30%;"></a>

          </div>

          @endforeach
          <button class="w3-button w3-display-left w3-black" onclick="plusDivs(-1)">&#10094;</button>
<button class="w3-button w3-display-right w3-black" onclick="plusDivs(1)">&#10095;</button>

                </div>
                
              </center>     
            </div>
         </div>


      </section>
      <script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}
</script>

@include('front_end.footer')