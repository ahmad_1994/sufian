<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class contactMessage extends Model
{
    use SoftDeletes;

    public function ReadUser()
    {
    	return $this->hasOne('App\User','id','read_by');
    }

    public function DeletedByUser()
    {
    	return $this->hasOne('App\User','id','deleted_by');
    }
}
