<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends Model
{

	use SoftDeletes;
    public function AddedBy()
    {
    	return $this->hasOne('App\User','id','added_by');
    }

    public function DeletedBy()
    {
    	return $this->hasOne('App\User','id','deleted_by');
    }
}
