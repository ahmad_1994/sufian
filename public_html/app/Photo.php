<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Photo;
class Photo extends Model
{
    use SoftDeletes;

    public function AddedBy()
    {
    	return $this->hasOne('App\User','id','added_by');
    }

    public function DeletedBy()
    {
    	return $this->hasOne('App\User','id','deleted_by');
    }

    public function Album()
    {
    	return $this->hasOne('App\Album','id','album_id');
    }

    public static function getRandomImage($album_id)
    {
        $get_album = Album::find($album_id);
        if ($get_album) {
           $get_all_photos_in_album  = Photo::where('album_id',$album_id)->get();
           $all_id = array();
           $count = 0;
           foreach ($get_all_photos_in_album as $photo) {
            $all_id[$count] = $photo->id;
            $count++;   
           }

           
            return $all_id[array_rand($all_id)];

        }
        else{
            return false;
        }
    }

    public static function image ($image_path)
    {

        $path = str_replace('public', 'https://sn-sons.com/storage', $image_path);
        return $path;
        

    }
}
