<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Session;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_user = User::find(Auth::User()->id);
            if ($get_user) {
                
                if ($get_user->active == 1) {
                   return view('back_end.home');  
                }
                else{
                    return redirect('/logout');
                }
            }

            else{
                return redirect('/');
            }
        return view('back_end.home');
    }
}
