<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
use Auth;
use Session;
use DB;
class VideoController extends Controller
{
	public function uploadVideo()
	{

		return view('back_end/video.create');
	}


	public function uploadNewVideo(Request $request)
	{
		 $validatedData = $request->validate([
        'video' => 'required|mimes:mp4,mov,ogg,qt',
        //'video_subject','required',
       ]);

		 $new_video = new Video;
		 $new_video->added_by = Auth::User()->id;
		 $new_video->video_subject = $request->video_subject;
$new_video->video_path = $request->video->store('public/videos');
		if ($request->video_description) {
		 $new_video->video_description = $request->video_description;	
		}

		$new_video->save();

		Session::flash('success_video','Video Added successfully');
		return redirect('/all_videos');
	}

	public function allVideos()
	{
		$get_all_videos = Video::all();
	return view('back_end/video.index')->with('get_all_videos',$get_all_videos);	
	}

	public function undoDeleteVideo($id)
	{
		$get_video = DB::table('videos')->where('id',$id)->first();
       if ($get_video) {
           if ($get_video->deleted_at != NULL) {
                
            DB::table('videos')
            ->where('id', $id)
            ->update(['deleted_at' => NULL , 'deleted_by' => 0]);  

            Session::flash('undo_video','Undo Video successfully');
            return redirect()->back(); 
           }

           else{
            return redirect()->back();
           }
       }
       else{
            return redirect()->back();
           }	

	}
    
    public function deleteVideo(Request $request)
    {
    	$get_video = Video::find($request->video_id);
    	if ($get_video) {
   				$id = $request->video_id;
   			$get_video->deleted_by = Auth::User()->id;
   			$get_video->save();
   			$get_video->delete();

   			Session::flash('delete_video',$id);
   			return redirect()->back();
    	}

    	else{
    		return redirect()->back();
    	}
    }
}
