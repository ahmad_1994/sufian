<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\contactMessage;
use Session;
use Auth;
use App\User;
use DB;
class BackEndController extends Controller
{
    public function getContactUsMessages()
    {
    	$get_all_messages = contactMessage::all();
    	return view('back_end.contact_us_messages')->with('get_all_messages',$get_all_messages);
    }

    public function markMessageRead(Request $request)
    {
    	$get_message = contactMessage::find($request->message_id);
    	if ($get_message) {
    		
    			
    		if ($get_message->read_or_not == 0) {
    			$get_message->read_or_not = 1 ;
    			$get_message->read_by = Auth::User()->id;
    			$get_message->save();
    			Session::flash('message_read','Message successfully read !');
    			return redirect()->back();		
    	}

    		elseif ($get_message->read_or_not == 1) {

    			$get_message->read_or_not = 0 ;
    			$get_message->read_by = 0;
    			$get_message->save();
    			Session::flash('message_read','Message successfully Not read !');
    			return redirect()->back();		
    		}	
    	}

    	else{
    		return redirect()->back();
    	}

    }

    public function deleteContactMessage(Request $request)
    {   
            $message_id = 0;
        $get_message = contactMessage::find($request->message_id);
        if ($get_message) {
            $message_id = $get_message->id;
            
            
            $get_message->deleted_by = Auth::User()->id;
            $get_message->save();

            $get_message->delete();
Session::flash('message_deleted','Message Deleted successfully !'.$message_id);
            return redirect()->back();
            
        }

        else{
            return redirect()->back();
        }
    }

    public function undoDeleteContactMessage($id)
    {
        $get_message = DB::table('contact_messages')->where('id',$id)->first();
        if ($get_message) {
             
             DB::table('contact_messages')
            ->where('id', $id)
            ->update(['deleted_at' => NULL , 'deleted_by' => 0]);


            Session::flash('undo_successfully','Message Not Deleted !');
            return redirect()->back();

        }

        else{
            return redirect()->back();
        }
        
    }
}
