<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProfilPhotos;
use Auth;
use Session;
use App\User;
use DB;
use Hash;
class ProfileController extends Controller
{
    public function myProfile()
    {
        $get_all_photos_for_the_account = ProfilPhotos::where('user_id',Auth::User()->id)->orderBy('id','desc')->get();
    	return view('back_end/profile.my_profile')->with('get_all_photos_for_the_account',$get_all_photos_for_the_account);
    }

    public function uploadProfilePhotos (Request $request)
    {
    	$validatedData = $request->validate([
        'image' => 'required|image:jpg,png',
    ]);
    	$get_all_photos_for_the_account = ProfilPhotos::where('user_id',Auth::User()->id)->where('default_image',1)->get();

    	foreach ($get_all_photos_for_the_account as $photo ) {
    		
    		$get_photo = ProfilPhotos::find($photo->id);
    		if ($get_photo) {
    			$get_photo->default_image = 0;
    			$get_photo->save();	
    		}
    	}

    	
    	$new_profile_image  = new ProfilPhotos;
    	$new_profile_image->user_id = Auth::User()->id;
    	$new_profile_image->image_path = $request->image->store('public/profile_images');
    	$new_profile_image->default_image = 1;
    	$new_profile_image->save();

    	
    	

    	Session::flash('profile_image_uploade','Profile Image Uploaded successfully');
    	return redirect()->back();
    }

    public function changePassword(Request $request){
   if (!(Hash::check($request->get('old_password'), Auth::user()->password))) {

   	Session::flash('old_password_not_match','Your current password does not matches with the password you provided. Please try again.');
return redirect()->back();
}

if(strcmp($request->get('old_password'), $request->get('new_password')) == 0){
//Current password and new password are same
		Session::flash('old_password_and_new_password','New Password cannot be same as your current password. Please choose a different password.');
return redirect()->back();
}
$validatedData = $request->validate([
'old_password' => 'required',
'new_password' => 'required|string|min:6',
]);

	if ($request->new_password != $request->new_password_confirmation) {
		Session::flash('new_password_not_matched','The new password confirmation does not match.');
		return redirect()->back();
		
	}
//Change Password
$user = Auth::user();
$user->password = bcrypt($request->get('new-password'));
$user->save();

Session::flash('success_change_password','Password changed successfully !');
return redirect()->back();
}

    public function setImageDefault($id)
    {
        $find_image = ProfilPhotos::find($id);
        if ($find_image) {
            
            if ($find_image->user_id == Auth::User()->id) {
               
               DB::table('profil_photos')
            ->where('user_id', $find_image->user_id)
            ->update(['default_image' => 0]);

            $find_image->default_image = 1 ;
            $find_image->save();

            Session::flash('image_set_success','Image set successfully');
            return redirect()->back(); 

            }
            else{
                return redirect()->back();
            }
        }
        else{
                return redirect()->back();
            }
        
    }

    public function deleteProfileImage($id)
    {
        $find_image = ProfilPhotos::find($id);
        if ($find_image) {
            if ($find_image->user_id == Auth::User()->id) {

                $find_image->delete();
                Session::flash('delete_profile_image','Image deleted successfully');
                return redirect()->back();

            }

            else{
            return redirect()->back();
        }


            
        }

        else{
            return redirect()->back();
        }
        
    }

    public function changeName(Request $request)
    {
        $get_user = User::find($request->user_id);
        if ($get_user) {
            
            $get_user->name = $request->name;
            $get_user->save();
            Session::flash('image_set_success','Name changed successfully');
            return redirect()->back();   
        }

        else{
            return redirect()->back();
        }
    }

}
