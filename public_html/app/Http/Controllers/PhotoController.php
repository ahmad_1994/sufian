<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Photo;
use Auth;
use Session;
use DB;
use App\Album;
use App\Http\Resources\Photos as PhotoResources;
class PhotoController extends Controller
{

	public function createPhoto()
	{
        //abort(404);
		return view('back_end/photos.create');
		
	}
    

    public function allPhotos()
    {
    	
        $get_all_photos = Photo::/*where('album_id',NULL)->*/orderBy('id','desc')->get();
        $get_active_album = Album::where('active',1)->orderBy('id','desc')->get();

    	return view('back_end/photos.index')->with('get_all_photos',$get_all_photos)->with('get_active_album',$get_active_album);
    }

    public function addPhoto(Request $request)
    {

            if ($request->hasfile('file')) {
            foreach ($request->file as $file) {
                $file_name = $file->getClientOriginalName();
                $file->storeAs('public/photos',$file_name);
                $new_photo = new Photo;
                $new_photo->album_id = $request->album_id;
                $new_photo->image_path = 'public/photos/'.$file_name;
                $new_photo->added_by = Auth::User()->id;

                $new_photo->save();

               
             
            } 
             Session::flash('image_upload','Image uploaded successfully');
                return redirect()->back();
            } 	

            else{
                Session::flash('choose_image','Please choose one image at least');
                return redirect()->back();
            }
    }

    public function deletePhoto($id)
    {
        $get_photo = Photo::find($id);

        if ($get_photo) {
            $photo_id  = $id;
            $get_photo->deleted_by = Auth::User()->id;
            $get_photo->save();

            $get_photo->delete();

            Session::flash('delete_photo',$photo_id);
            return redirect()->back();
        }
    }

    public function undoDeletePhoto($id)
    {
        $get_photo = DB::table('photos')->where('id',$id)->first();

        if ($get_photo) {
            if ($get_photo->deleted_at != NULL) {
                
            DB::table('photos')
            ->where('id', $id)
            ->update(['deleted_at' => NULL , 'deleted_by' => 0]); 
            Session::flash('undo_photo','Undo successfully');
            return redirect()->back();
            }

            else{
                return redirect()->back();
            }
        }

        else{
             return redirect()->back();
        }
    }


    public function photosApi()
    {
        $photos = Photo::take(10)->get();

        return PhotoResources::collection($photos);
    }

     public function singlephotosApi($id)
    {
        return $id;

    }
}
