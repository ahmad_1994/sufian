<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Session;

class UserController extends Controller
{
    public function createUser()
    {
    	return view('back_end/users.create');
    	
    }

    public function viewUsers()
    {

    	$get_all_users = User::orderBy('id','desc')->get();

    	return view('back_end/users.all_users')->with('get_all_users',$get_all_users);    	
    }

    public function addUser(Request $request)
    {
    	$validatedData = $request->validate([
      'name' => 'required|string|max:255',
      'email' => 'required|string|email|max:255|unique:users',
      'password' => 'required|string|min:6|confirmed',
        
    ]);

    	$new_user = new User;
    	$new_user->name = $request->name;
    	$new_user->email = $request->email;
    	$new_user->password = bcrypt($request->password);
    	$new_user->added_by = Auth::User()->id;

    	$new_user->save();

    	Session::flash('user_added','User Added Successfully !');
    	return redirect('/all_users');

    	
    }

    public function editUser($id)
    {
    	$get_user = User::find($id);
    	if ($get_user) {


    		return view('back_end/users.edit_user')->with('get_user',$get_user);
    		
    	}

    	else{
    		return redirect()->back();
    	}
    	
    }

    public function updateUser(Request $request)
    {	
    	$user = User::find($request->user_id);
    	if ($user) {
  		  $validatedData = $request->validate([
      'name' => 'required|string|max:255',
      'email' => 'unique:users,email,'.$user->id,
        
    ]); 

    	$user->name = $request->input('name');
    	$user->email = $request->input('email');
    	$user->save();

    	Session::flash('user_added','User Updated Successfully !');
    	return redirect('/all_users'); 	
    	}	
    }

    public function Activation($id)
    {
        $get_user = User::find($id);
        
        if ($get_user) {
            
            if ($get_user->active == 0) {
            $get_user->active = 1;
            $get_user->save();  
            Session::flash('user_active','User activated Successfully');
            return redirect()->back();    
        }

        elseif($get_user->active == 1){
            $get_user->active = 0;
            $get_user->save(); 

            if ($get_user->id == Auth::User()->id) {
                return redirect('/logout');
            }

            Session::flash('user_active','User deactivated Successfully');
            return redirect()->back();
        }    
        }

        else{
            return redirect()->back();
        }
    }
}
