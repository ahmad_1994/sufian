<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\ProjectDetail;
use Auth;
use Session;
use DB;
class ProjectContoller extends Controller
{
    
    public function index()
    {
        $get_all_projects = Project::orderBy('id','desc')->get();

        return view('back_end/projects.index')->with('get_all_projects',$get_all_projects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('back_end/projects.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
       // 'image' => 'required|image:jpg,png',
        'project_name' => 'required',
        //'project_description'=>'required',
        //'project_main_subject' => 'required',
    ]);

        $new_project = new Project;
        $new_project->name = $request->project_name;
        $new_project->project_image_path = $request->image->store('public/projects');
        $new_project->project_description = $request->project_description;
        $new_project->added_by = Auth::User()->id;
        $new_project->project_main_subject = $request->project_main_subject;
        $new_project->save();

        Session::flash('project_added','Project added successfully .. Please add Files of project');
        return redirect()->route('projects.show',$new_project->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $get_project = Project::find($id);
       if ($get_project) {
           
           $get_all_images_in_project = ProjectDetail::where('project_id',$id)->get();

           return view('back_end/projects.show')->with('get_project',$get_project)->with('get_all_images_in_project',$get_all_images_in_project);
       }

       else{

        return redirect()->back();
       }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function uploadImage(Request $request)
    {
        if ($request->hasfile('file')) {

            foreach ($request->file as $file ) {
                
                $file_name = $file->getClientOriginalName();
                $extention_array = explode('.', $file_name);
                $file_extention= end($extention_array);
                $get_random_name_of_file = ProjectDetail::getRandomString();
                $final_name_of_file = $get_random_name_of_file.'.'.$file_extention;
                $file->storeAs('public/project_details',$final_name_of_file);
                $new_image = new ProjectDetail;
                $new_image->project_image = 'public/project_details/'.$final_name_of_file;
                $new_image->added_by = Auth::User()->id;
                $new_image->project_id = $request->project_id;
                $new_image->save();
            }

            Session::flash('success_added_image','File Added successfully');
            return redirect()->back();

        }
        else{
            return redirect()->back();
        }
        
    }

    public function deleteProjectImage($id)
    {
        $get_project_image = ProjectDetail::find($id);
        if ($get_project_image) {
          
          $project_id = $id;

          $get_project_image->deleted_by = Auth::User()->id;
          $get_project_image->save();

          $get_project_image->delete();

          Session::flash('delete_photo',$project_id);
          return redirect()->back(); 
        }

        else{

            return redirect()->back();
    }
        }

        public function undoDeleteProjectImage($id)
        {
            $get_project_image = DB::table('project_details')->where('id',$id)->first();

            if ($get_project_image) {
                
                if ($get_project_image->deleted_at != NULL) {
                    
            DB::table('project_details')
            ->where('id', $id)
            ->update(['deleted_at' => NULL , 'deleted_by' => 0]);

            Session::flash('success_added_image','Undo successfully');
            return redirect()->back();
                }

                else{
                    return redirect()->back();
                }
            }

            else{
                return redirect()->back();
            }
        }

        public function companyProjects()
        {
        	$get_all_projects  = Project::orderBy('id','desc')->get();

        return view('front_end.company_projects')->with('get_all_projects',$get_all_projects);
        }

        public function companyProjectDetail($id)
        {
        	$get_project = Project::find($id);
        	if ($get_project) {

        		$get_all_project_details = ProjectDetail::where('project_id',$id)->get();
        		return view('front_end.project_details')->with('get_project',$get_project)->with('get_all_project_details',$get_all_project_details);
        		
        	}

        	else{
        		return redirect()->back();
        	}
       		
        }

        public function editProject($id)
        {
        	$get_project = Project::find($id);
        	if ($get_project) {
            
            return view('back_end/projects.edit')->with('get_project',$get_project); 
            }

             else{
             	return redirect()->back();
             }
        }

        public function updateProject(Request $request)
        {	

        	$validatedData = $request->validate([
        		'name' => 'required',
   				 ]);

        	$get_project = Project::find($request->project_id);
        	if ($get_project) {
        
        	

        	if ($request->image) {
        	
        	$get_project->name = $request->input('name');
        	$get_project->project_image_path = $request->image->store('public/projects'); 
        	$get_project->project_main_subject = $request->input('project_main_subject');
        	$get_project->project_description = $request->input('project_description');
        	$get_project->updated_by = Auth::User()->id;
        	$get_project->save();

        	Session::flash('updated','Project updated successfully');
        	return redirect()->route('projects.show',$get_project->id);

        	}

        	else{
        	$get_project->name = $request->input('name');
        	$get_project->project_main_subject = $request->input('project_main_subject');
        	$get_project->project_description = $request->input('project_description');
        	$get_project->updated_by = Auth::User()->id;
        	$get_project->save();

        	Session::flash('updated','Project updated successfully');
        	return redirect()->route('projects.show',$get_project->id);

        	}
        	
        }
        }

        public function deleteProject($id)
        {
        	
        	$get_project = Project::find($id);
        	if ($get_project) {
        		$project_id = $get_project->id;

        		$get_project->deleted_by = Auth::User()->id;
        		$get_project->save();

        		$get_project->delete();

        		Session::flash('delete_project',$project_id);
        		return redirect()->back();
        		
        	}

        	else{
        		return redirect()->back();
        	}
        }

         public function undoDeleteProject($id)
        {
        	$get_project = DB::table('projects')->where('id',$id)->first();

        	if ($get_project) {
        		if ($get_project->deleted_at != NULL) {

        			DB::table('projects')
        			->where('id',$id)
        			->update(['deleted_at' => NULL , 'deleted_by' => 0]);

        			Session::flash('undo_delete_project','Undo successfully');
        			return redirect()->back();
        		
        		}

        		else{
        			return redirect()->back();
        		}
        		
        	}
        	else{
        		return redirect()->back();
        	}
        	
        }
}
