<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Album;
use Auth;
use Session;
use DB;
use App\Photo;
class AlbumController extends Controller
{
   
    public function index()
    {
        $get_all_albums = Album::orderBy('id','desc')->get();
        return view('back_end/albums.index')->with('get_all_albums',$get_all_albums);
        
    }

    
    public function create()
    {
         return view('back_end/albums.create');
        
    }

    
    public function store(Request $request)
    {
       $this->validate($request,array(

        'album_name' => 'required|unique:albums',
       
       ));

       $new_album = new Album;
       $new_album->album_name = $request->album_name;
       $new_album->added_by = Auth::User()->id;
       $new_album->save();

       Session::flash('album_created','Album created successfully');
       return redirect()->route('albums.show',$new_album->id);
    }

    public function show($id)
    {
        $find_album = Album::find($id);
        if ($find_album) {
          $get_all_photos_in_album = Photo::where('album_id',$id)->orderBy('id','desc')->get();
          return view('back_end/albums.show')->with('find_album',$find_album)->with('get_all_photos_in_album',$get_all_photos_in_album);  
        }

        else{
            return redirect()->route('albums.index');
        }
       
    }

    public function updateAlbum(Request $request)
    {
        $this->validate($request,array(

        'album_name' => "required|unique:albums,album_name,$request->album_id",
       
       ));
        $find_album = Album::find($request->album_id);
        if ($find_album) {
          
          $find_album->album_name = $request->input('album_name');
          $find_album->updated_by = Auth::User()->id;
          $find_album->save();

          Session::flash('updated','Album name updated successfully');
          return redirect()->back();  

        }
        else{
            return redirect()->back();
        }
        
    }

    public function activeAlbum($id)
    {
     $find_album = Album::find($id);
     if ($find_album) {
       if ($find_album->active == 1) {
        
        $find_album->active = 0;
        $find_album->updated_by = Auth::User()->id;
        $find_album->save();
        Session::flash('active','Album Deactivated successfully');
        return redirect()->back();     
      } 

        elseif ($find_album->active == 0) {
          $find_album->active = 1;
        $find_album->updated_by = Auth::User()->id;
        $find_album->save();
        Session::flash('active','Album active successfully');
        return redirect()->back(); 
        }

        }
        else{
          return redirect()->back();
        }
    }

    
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        
    }

    
    public function destroy($id)
    {
        
    }

    public function changeAlbum(Request $request)
    {
      $find_album = Album::find($request->album_id);
      $find_photo = Photo::find($request->photo_id);

      if ($find_photo and $find_album) {
        
        $find_photo->album_id = $request->album_id;
        $find_photo->save();
        Session::flash('image_upload','Album set successfully');
        return redirect()->back();
      }

      else{
        return redirect()->back();
      }
    }
}
