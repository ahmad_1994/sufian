<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\contactMessage;
use Session;
use Auth;
use App\User;
use App\SliderShow;
use DB;
use App\Photo;
use App\Album;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
class FrontEndController extends Controller
{
    public function aboutUs()
    {
    	return view('front_end.about_us');
    }

    public function contactUs()
    {
    	return view('front_end.contact_us');
    }

    public function sendMessageContactUs(Request $request)
    {
      // start store in DB
       $validatedData = $request->validate([
        'email' => 'required|email',
        'name' => 'required',
        'subject' => 'required',
        'message' => 'required',
        'mobile_number' => 'required|numeric',
    ]);

       
      $headers = "MIME-Version: 1.0" . "\r\n";
         $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
         //$headers .= 'From:' . $request->email. "\r\n";

      mail('ahmed_ali_1994@hotmail.com',$request->subject,'From : '.$request->email . "\n\n" . 'Mobile Number : ' . $request->mobile_number ."\n\n".'Message : '. $request->message);

       mail('info@sufiannasser.com',$request->subject,'From : '.$request->email . "\n\n" . 'Mobile Number : ' . $request->mobile_number ."\n\n".'Message : '. $request->message);
      
    	
        

    	 $new_message = new contactMessage;
    	 $new_message->mobile_number = $request->mobile_number;
    	 $new_message->name = $request->name;
    	 $new_message->email = $request->email;
    	 $new_message->subject = $request->subject;
    	 $new_message->message = $request->message;

    	 $new_message->save();

       Session::flash('message_sent','Your Message has been sent successfully');
       return redirect()->back();

    }

    public function createSliderShow()
    {
        return view('back_end/slider_show.create');
    }

    public function uploadNewSliderShow(Request $request)
    {
         $validatedData = $request->validate([
        'image' => 'required|image:jpg,png',
        'start_date' => 'required|date',
        
    ]);

        $new_image = new SliderShow;
    $new_image->image_path = $request->image->store('public/sliderShow');
    $new_image->start_date = $request->start_date;
    $new_image->end_date = $request->end_date;
    $new_image->added_by = Auth::User()->id;
    $new_image->save();
    Session::flash('slider_added','Slider Image Added successfully');
    return redirect('all_slider_show');
        
    }

    public function getAllSliderShowImges()
    {
        $get_all_images = SliderShow::orderBy('id','desc')->get();
        //return $get_all_images;
       // return view('back_end');
        return view('back_end/slider_show.all_slider_images')->with('get_all_images',$get_all_images);
    }

    public function endSliderShow(Request $request)
    {
        $get_slider  = SliderShow::find($request->slider_id);
        if ($get_slider) {
         
         $validatedData = $request->validate([
        'end_date' => 'required|date|after_or_equal:'.$get_slider->start_date,
        
    ]);

            $get_slider->end_date = $request->end_date;
            $get_slider->ended_by = Auth::User()->id;
            $get_slider->save();

            Session::flash('ended_successfully','Slider Ended successfully');
            return redirect()->back();

        }
        else 
        {
            return redirect()->back();
        }    
    }

    public function deleteSliderImage(Request $request)
    {
        $get_slider = SliderShow::find($request->slider_id);
        if ($get_slider) {
            $slider_id = $get_slider->id;
            $get_slider->deleted_by = Auth::User()->id;
            $get_slider->save();
            $get_slider->delete();

            Session::flash('delete_slider',$slider_id);
            return redirect()->back();
        }
    }

    public function undoDeleteSliderImage($id)
    {
       $get_slider = DB::table('slider_shows')->where('id',$id)->first();
       if ($get_slider) {
           if ($get_slider->deleted_at != NULL) {
                
            DB::table('slider_shows')
            ->where('id', $id)
            ->update(['deleted_at' => NULL , 'deleted_by' => 0]);  

            Session::flash('undo_slider','Undo Slider Show successfully');
            return redirect()->back(); 
           }

           else{
            return redirect()->back();
           }
       }
       else{
            return redirect()->back();
           }
    }

  public function allPhotos()
  {


    $get_all_photos = Photo::paginate(4);
    return view('front_end/photos')->with('get_all_photos',$get_all_photos);
  }

  public function getMessage($id)
  {
    $get_message = contactMessage::find($id);
    if ($get_message) {
      if ($get_message->read_or_not == 0) {

        $get_message->read_or_not = 1;
        $get_message->read_by = Auth::User()->id;
        $get_message->save();

        
      }

      return view('back_end/message')->with('get_message',$get_message);
    }

    else{
      return redirect()->back();
    }
    
  }

  public function setSliderFirst($id)
  {
    $get_slider = SliderShow::find($id);
    if ($get_slider) {

      DB::table('slider_shows')
            ->update(['first_photo' => 0]);
      
      $get_slider->first_photo = 1;
      $get_slider->save();

      Session::flash('first_photo','Image first photo set successfully');
      return redirect()->back();
    }

    else{
      return redirect()->back();
    }
  }

  public function allAlbums()
  {
    $get_all_albums = Album::orderBy('id','desc')->where('active',1)->paginate(4);
    return view('front_end.albums')->with('get_all_albums',$get_all_albums);
    
  }

  public function photoAlbum($id)
  {
    $get_album = Album::find($id);
    if ($get_album) {
     
     $get_all_photos = Photo::where('album_id',$id)->orderBy('id','desc')->paginate(4);
     return view('front_end.photos')->with('get_all_photos',$get_all_photos)->with('get_album',$get_album); 
    }

    else{
      return redirect()->back();
    }
    
  }
}
