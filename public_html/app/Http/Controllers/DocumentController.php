<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Document;
use App\DocumentImage;
use Auth;
use Session;
use DB;
class DocumentController extends Controller
{
    public function allDocuments()
    {
    	$get_all_documents = Document::orderby('id','desc')->get();
    	return view('back_end/documents.index')->with('get_all_documents',$get_all_documents);
    }

    public function oneDocument($id)
    {
    	$find_document = Document::find($id);
    	if ($find_document) {

            
    		
    		$get_all_images_in_doc = DocumentImage::where('document_id',$id)->get();
    		return view('back_end/documents.show')->with('find_document',$find_document)->with('get_all_images_in_doc',$get_all_images_in_doc);

    	}

    	else{
    		return redirect()->back();
    	}
    	
    }

    public function addDocument(Request $request)
    {
    	$validatedData = $request->validate([
        'image' => 'required|image:jpg,png',
        'document_name' => 'required',
    ]);

        $new_document = new Document;
        $new_document->document_image_path =  $request->image->store('public/website_doc');
        $new_document->document_name = $request->document_name;
        $new_document->added_by = Auth::User()->id;
        $new_document->save();

        Session::flash('document_added','Document added successfully');
        return redirect('/document/'.$new_document->id);

    	
    }

    public function createDocument()
    {

    	return view('back_end/documents.create');
    	
    }

    public function uploadDocumentImage(Request $request)
    {
        
        $validatedData = $request->validate([
        'image' => 'required|image:jpg,png',
    ]);

        $new_document_image = new DocumentImage;
        $new_document_image->image_path =  $request->image->store('public/website_doc_details');
        $new_document_image->document_id = $request->document_id;
        $new_document_image->added_by = Auth::User()->id;
        $new_document_image->save();

        Session::flash('image_uploade','Image added successfully');
        return redirect()->back();

       
    }

    public function deleteDocumentImage($id)
    {
        $get_document_image  = DocumentImage::find($id);
        if ($get_document_image) {
            $image_id = $id;
          $get_document_image->deleted_by = Auth::User()->id;
          $get_document_image->save();

          $get_document_image->delete();
          

          Session::flash('delete_photo',$image_id);
          return redirect()->back();

        }

        else{
            return redirect()->back();
        }
        
    }

    public function undoDeleteDocumentPhoto($id)
    {
        $get_document_image = DB::table('document_images')->where('id',$id)->first();

        if ($get_document_image) {
            
            DB::table('document_images')
            ->where('id',$id)
            ->update(['deleted_at' => NULL , 'deleted_by' => 0]);

            Session::flash('image_uploade','Saved');
            return redirect()->back();

            
        }

        else{
            return redirect()->back();
        }
        
    }

    public function update(Request $request)
    {
        
       $get_document = Document::find($request->document_id);
       if ($get_document) {
        $validatedData = $request->validate([
        'document_name' => 'required',
    ]);
          
          $get_document->document_name = $request->document_name;
          $get_document->updated_by = Auth::User()->id;
          $get_document->save();
          Session::flash('success','Document name changed successfully');
          return redirect()->back();
       }

       else{
        return redirect()->back();
       }
    }

    public function getOneDocument($id)
    {
        $get_document = Document::find($id);
        if ($get_document) {

            $get_document_images = DocumentImage::where('document_id',$id)->get();

            return view('front_end.document_details')->with('get_document_images',$get_document_images)->with('get_document',$get_document);
            
        }

        else{
            return redirect()->back();
        }
    }

    public function allDocumentsFrontEnd()
    {
        $get_all_documents = Document::orderby('id','desc')->get();
        return view('front_end.documents')->with('get_all_documents',$get_all_documents);
    }

    public function testSlider()
    {
        $get_all_documents = Document::orderby('id','desc')->get();
        return view('front_end.documents1')->with('get_all_documents',$get_all_documents);
    }

    public function deleteDocuments(Request $request)
    {
        $get_document = Document::find($request->document_id);
        if ($get_document) {
            $doc_id = $get_document->id;
            $get_all_images_in_the_document = DocumentImage::where('document_id',$request->document_id)->get();

            foreach ($get_all_images_in_the_document as $image) {

                $image->deleted_by = Auth::User()->id;
                $image->save();
                $image->delete();
            }
            $get_document->deleted_by = Auth::User()->id;
            $get_document->save();
            $get_document->delete();

            Session::flash('document_delete',$doc_id);
            return redirect()->back();


        }
        else{
            return redirect()->back();
        }
    }
    public function undoDeleteDocument($id)
    {
        $get_document = DB::table('documents')->where('id',$id)->first();

        if ($get_document) {
            if ($get_document->deleted_at != NULL) {
                
            
            DB::table('document_images')
            ->where('document_id',$id)
            ->update(['deleted_at' => NULL , 'deleted_by' => 0]);

            DB::table('documents')
            ->where('id',$id)
            ->update(['deleted_at' => NULL , 'deleted_by' => 0]);

            Session::flash('undo_document_delete','Undo successfully');
            return redirect()->back();
    }
    else{
        return redirect()->back();
    }
    }
    else
    {
     return redirect()->back();

    }
    }
}
