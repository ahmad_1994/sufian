<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LastNews;
use Auth;
use Session;
use DB;
class LastNewsController extends Controller
{
   
    public function index()
    {
    	$get_all_news = LastNews::orderBy('id','desc')->get();

    	return view('back_end/last_news.index')->with('get_all_news',$get_all_news);       
    }

    
    public function create()
    {
      return view('back_end/last_news.create');  
    }

    
    public function store(Request $request)
    {
    	$validatedData = $request->validate([
        'description' => 'required',
        'main_subject' => 'required',
    ]);

    	$new_news = new LastNews;
    	$new_news->description = $request->description;
    	$new_news->main_subject = $request->main_subject;
    	$new_news->added_by = Auth::User()->id;
    	$new_news->active = 1;
    	$new_news->save();

    	Session::flash('news_added','News Added successfully');
    	return redirect()->route('last_news.show',$new_news->id);
     
     return $request;   
    }

    
    public function show($id)
    {
    	$get_one_news = LastNews::find($id);
    	if ($get_one_news) {
    	return view('back_end/last_news.show')->with('get_one_news',$get_one_news);	
    	}

    	else{
		return redirect()->route('last_news.index');
    	}
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updateNews(Request $request)
    {
    	$find_news = LastNews::find($request->news_id);
    	if ($find_news) {
    	$validatedData = $request->validate([
        'description' => 'required',
        'main_subject' => 'required',
    ]);

    		$find_news->description = $request->input('description');
    		$find_news->main_subject = $request->input('main_subject');
    		$find_news->save();

    		Session::flash('news_added','News updated successfully');
    		return redirect()->back();
    	}
    }

   	
    public function undoDeleteNews($id)
    {

    	$find_news = DB::table('last_news')->where('id',$id)->first();

    	if ($find_news) {
    		

    		if ($find_news->deleted_at != NULL) {
    		
    		DB::table('last_news')
    		->where('id',$id)->update(['deleted_at' => NULL , 'deleted_by' => 0]);

    		Session::flash('undo_success','Undo successfully');
    		return redirect()->back();	
    		}

    		else{
    			return redirect()->back();
    		}
    	}

    	else{
    		return redirect()->back();
    	}
    	
    }

    public function destroy($id)
    {
        $find_news = LastNews::find($id);
        if ($find_news) {
        	$news_id = $id;
        	$find_news->deleted_by = Auth::User()->id;
        	$find_news->save();
        	$find_news->delete();

        	Session::flash('delete_news',$news_id);
        	return redirect()->route('last_news.index');
        	
        }

        else{
        	return redirect()->back();
        }
    }

    public function activeNews($id)
    {
        
        $find_news = LastNews::find($id);
        if ($find_news) {
            if ($find_news->active == 1) {
            
                $find_news->active = 0;
                $find_news->save();
                Session::flash('active_news','News deactive successfully');
                return redirect()->back();
            }
            elseif($find_news->active == 0){
                 $find_news->active = 1;
                $find_news->save();
                Session::flash('active_news','News active successfully');
                return redirect()->back();
            }

        }

        else{
            return redirect()->back();
        }
    }
}
