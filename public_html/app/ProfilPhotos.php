<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfilPhotos extends Model
{
	use SoftDeletes;

	public function User()
	{
		return $this->hasOne('App\User','id','user_id');
	}
}
