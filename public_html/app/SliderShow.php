<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SliderShow extends Model
{
	use SoftDeletes;

	public function AddedUser ()
	{
		return $this->hasOne('App\User','id','added_by');

	} 

	public function DeletedUser ()
	{
		return $this->hasOne('App\User','id','deleted_by');

	}    
	public function EndedBy()
	{
		return $this->hasOne('App\User','id','ended_by');
	}
}
