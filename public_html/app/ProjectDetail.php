<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ProjectDetail extends Model
{
    use SoftDeletes;


    public function AddedBy()
    {
    	return $this->hasOne('App\User','id','added_by');
    }


    public function DeletedBy()
    {
    	return $this->hasOne('App\User','id','deleted_by');
    }

public static function getRandomString() {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'),range('A','Z'));

    for ($i = 0; $i < 25 ; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}
}
