-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 19, 2019 at 07:06 AM
-- Server version: 10.1.40-MariaDB-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `snsokaqa_sufian_nasser`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_companies`
--

CREATE TABLE `about_companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `added_by` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT '0',
  `main_subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `id` int(11) NOT NULL,
  `album_name` varchar(255) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`id`, `album_name`, `active`, `added_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'FIBER TO THE HOME-FTTH', 1, 5, NULL, NULL, NULL, '2018-08-17 17:53:51', '2018-08-17 17:53:51'),
(2, 'RAIN WATER DRAINAGE', 1, 5, NULL, NULL, NULL, '2018-08-17 17:56:10', '2018-08-17 17:56:10'),
(3, 'WATER LINES', 1, 5, NULL, NULL, NULL, '2018-08-17 17:56:42', '2018-08-17 17:56:42'),
(4, 'MASS WIND POWER STATION', 1, 5, 5, NULL, NULL, '2018-08-17 17:57:40', '2018-08-26 15:06:23'),
(5, 'Micro Trenching', 1, 5, NULL, NULL, NULL, '2019-01-26 13:46:48', '2019-01-26 13:46:48');

-- --------------------------------------------------------

--
-- Table structure for table `contact_messages`
--

CREATE TABLE `contact_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_or_not` int(11) NOT NULL DEFAULT '0',
  `read_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_messages`
--

INSERT INTO `contact_messages` (`id`, `mobile_number`, `name`, `email`, `subject`, `message`, `read_or_not`, `read_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '+962797002324', 'Mohammad Otoum', 'mohammad@sufiannasser.com', 'Test', 'Test', 1, 5, 0, NULL, '2018-07-26 15:45:36', '2018-09-01 16:05:51'),
(2, '+962797002324', 'Mohammad Otoum', 'mohammad@sufiannasser.com', 'Test', 'hi', 1, 5, 0, NULL, '2018-09-04 10:05:02', '2018-09-04 10:05:20'),
(3, '+962797002324', 'Mohammad Otoum', 'mohammad@sufiannasser.com', 'Test', 'Hi There', 1, 5, 0, NULL, '2018-10-30 12:31:45', '2018-10-30 12:32:04'),
(4, '16501681031', 'REGINE', 'playbunny1@live.de', 'Look at a goodbonus for your team.', 'Ciao! Look at nice present - Join us now, and we will double or even triple your first deposit.  Are you in?  http://bit.ly/2J7O0st', 1, 5, 0, NULL, '2018-11-14 16:25:56', '2018-11-15 13:02:27'),
(5, '13392610466', 'Ogpu', 'nevapros12@mail.ru', 'Look at a fineoffer for winning.', 'Hi! Please note an important present - slots, roulette and blackjack games.  Try and be our next winner. http://bit.ly/2JadFk6', 1, 5, 0, NULL, '2018-11-24 20:34:48', '2018-11-25 16:23:05'),
(6, '211385531', 'Tylerhix', 'tobias.starborg@studieframjandet.se', 'Please note niceoffer for you.', 'What we have here is , a fineoffer \r\n Just click \r\n \r\nhttp://topratinglist.com?20468', 1, 5, 0, NULL, '2018-12-01 23:00:48', '2018-12-02 15:30:41'),
(7, '175844723', 'Stephentug', 'siv-inger@avki.se', 'There is an unequalledoffers pro you.', 'Hi Look what we help in the behoof you! an extraordinaryloosely be rumoured b seagoing solder forward-looking \r\n To planned click on the concern a adverse in this splinter group  \r\n \r\n \r\nhttp://bit.ly/2PpKSd3', 1, 5, 0, NULL, '2018-12-11 19:40:03', '2018-12-12 13:35:25'),
(8, '167534438', 'Michaelbuppy', 'ritchanthony@gmail.com', 'That is a morselknack close to fitting of you.', 'Hy there,  an captivatingstock up \r\n Good click on the locale subordinate to to in environment  \r\n \r\n \r\nhttp://bit.ly/2UGkWOe', 1, 5, 0, NULL, '2018-12-15 10:25:54', '2018-12-18 14:15:09'),
(9, '12672802374', 'Timias', 'victorioqc@yahoo.es', 'Please note an importantprize for you.', 'Hi! Please note an interesting offers - Awesome welcome offer of up to $/€ 1600 free.  Just click on the link below to qualify. http://bit.ly/2J9pxDa', 1, 5, 0, NULL, '2018-12-17 23:19:16', '2018-12-18 14:15:43'),
(10, '157317245', 'StevenTAULT', 'sara@barnflickan.se', 'There is an invitingsuggest on the push pro you.', 'Hy there,  Elevated theory ! pleasantoffers \r\n Right click on the traffic jam lower to organize  \r\n \r\n \r\nhttp://bit.ly/2UA7oni', 1, 5, 0, NULL, '2018-12-19 11:15:41', '2018-12-23 15:35:47'),
(11, '242212363', 'Thomashex', 'luca.castagno@gmail.com', 'Here is  a upstandingpresent as regards you.', 'an continuous vacationallocate \r\n Legitimate click \r\n \r\n \r\nhttp://bit.ly/2rOHMpm', 1, 5, 0, NULL, '2018-12-23 02:04:42', '2018-12-23 15:35:29'),
(12, '352242181', 'Davidges', 'bottarocalzature@gmail.com', 'That is an pre-eminentoffers sooner than persuasion of you.', 'an supremeforth \r\n Just click on the relations care of the day-star to outfit  \r\n \r\n \r\nhttp://bit.ly/2EMLyaP', 1, 5, 0, NULL, '2018-12-26 10:55:26', '2019-02-04 14:09:21'),
(13, '185331214', 'JoshuaKek', 'simonharperpr@gmail.com', 'That is an winsomecake off a don on the store on reason of you.', 'Hy there,  What we lead on here is , a godlymake \r\n Trustworthy click on the telling under to modify  \r\n \r\n \r\nhttp://bit.ly/2ERX2tA', 1, 5, 0, NULL, '2018-12-30 04:38:14', '2018-12-30 13:02:32'),
(14, '17129937739', 'umeko', '194941@126.com', 'Please note a goodoffering for your team.', 'Good man! There is an important news - Deposit now $/€ 10 - and you will be able to play with  $/€ 20 or even $/€ 30.  Try and be our next winner. http://bit.ly/2yrXURv', 1, 5, 0, NULL, '2019-01-03 21:01:39', '2019-01-05 14:03:43'),
(15, '383287564', 'WilliamDrall', 'spektraphoto.au@gmail.com', 'Look at an surprisingpay pro you.', 'Hi a spasmoblation \r\n Incorruptible click \r\n \r\n \r\nhttps://drive.google.com/file/d/16iMC3yL29QUxk-zX2I11DfDfn5zeWCAD/preview', 1, 5, 0, NULL, '2019-01-03 21:14:34', '2019-01-05 14:03:26'),
(16, '225314833', 'Enriquefathy', 'glasmintvisualmedia@gmail.com', 'That is an amazingoffers for you.', 'Hy there,  What we have here is , a goodoffers \r\n To qualify click on the link below  \r\n \r\nhttps://drive.google.com/file/d/1NvPSqy_2PGbnU_lrsLGeL6cKxIsDMidy/preview', 1, 5, 0, NULL, '2019-01-09 01:19:21', '2019-01-09 14:01:56'),
(17, '151671634', 'Jamesdor', 'cairn.online@gmail.com', 'That is an amazingoffer for you.', 'What we have here is , an interestingoffering \r\n To qualify click on the link below  \r\n \r\nhttps://drive.google.com/file/d/1B8bvDtw9tkz4F0AM8WD0XZxwmGojEtZ2/preview', 1, 5, 0, NULL, '2019-01-13 20:19:16', '2019-01-14 13:22:47'),
(18, '387177411', 'PeterBuify', 'bambambillas@gmail.com', 'Hey Look what we accept quest of you! an astonishingoffers', 'Good news ! a goodoffers \r\n Are you in?    http://servicerubin.ru', 1, 5, 0, NULL, '2019-01-23 07:48:18', '2019-01-23 14:01:40'),
(19, '263747232', 'JamesFug', 'indoforextrader@gmail.com', 'Hy there,  Look what we induce for you! an outstandingoffers', 'Hi Look what we have for you! a magnificenttender \r\n Just now click on the tie up under to meet the requirements    http://bit.ly/2RXTqNJ', 1, 5, 0, NULL, '2019-01-27 14:38:45', '2019-01-28 15:45:59'),
(20, '133516435', 'RobertGew', 'martyn.chappell@gmail.com', 'Hy there,  Sufficient news ! an outstandingoblation', 'delightfullyoffers \r\n At best click on the tie up below to qualify    http://bit.ly/2S2prEe', 1, 5, 0, NULL, '2019-01-31 10:41:24', '2019-02-04 14:09:47'),
(21, '133516435', 'RobertGew', 'blindsbyelegance@gmail.com', 'Hi an superiorput up', 'Hy there,  Actual word ! a goodoffer \r\n Upstanding click   http://bit.ly/2S42yA7', 1, 5, 0, NULL, '2019-02-04 10:00:59', '2019-02-04 14:09:56'),
(22, '357522284', 'JosephMox', 'ghinilambor700@yahoo.com', 'How to get bitcoin being at work?', 'Hello! \r\n \r\nWe spend third part of our lives at work. How to spend this time with benefit? \r\n \r\nYou can grow bitcoins by 10% spending 5 minutes a day! \r\nQuicker than a cup of coffee \r\n \r\nhttp://dcbtc.info increases bitcoins by 10% in 48 hours. \r\nYou will automatically make a profit on your bitcoin wallet. \r\n \r\nStart participating and make a profit! \r\nGuaranteed by the blockchain technology!', 1, 5, 0, NULL, '2019-02-13 02:53:25', '2019-02-19 17:57:53'),
(23, '272475113', 'Haroldoxype', 'liam.bentley@gmail.com', 'an momentousoffering  Lately click on the relate below to ready', 'Hy there,  an distinguishedoffering \r\n Just click \r\n \r\nhttp://CFANM.TK', 1, 5, 0, NULL, '2019-02-22 19:38:41', '2019-02-23 13:52:09'),
(24, '381863352', 'Davidunalt', 'lakeedgeproductions@gmail.com', 'Hey a fineprovide  Moral click on the component beneath to meet the requirements', 'What we be subjected to here is , a pureoffers \r\n Just click on the connection underneath to mitigate  \r\nhttp://servicerubin.ru', 1, 5, 0, NULL, '2019-02-24 05:33:14', '2019-02-26 17:39:47'),
(25, '213245362', 'RobertJow', 'miketwomey2@gmail.com', 'Hy there,  Look what we have with a view you! an momentousdonation  Lately click on the unite under to prepare', 'Kind news ! a primepropose \r\n Are you in?  \r\n \r\nhttp://servicerubin.ru', 1, 5, 0, NULL, '2019-03-01 13:18:47', '2019-03-06 18:42:22'),
(26, '285133677', 'RogercOssy', 'imcasw@gmail.com', 'Confirm that you are not a robot, and learn how to earn $ 1000 a day', 'Confirm that you are not a robot, and learn how to earn $ 1000 a day \r\nhttp://guide-traveler.ru', 1, 5, 0, NULL, '2019-03-02 19:33:44', '2019-03-06 18:42:14'),
(27, '152128857', 'JamesLab', 'formforsome@yandex.ua', 'What should I do with bitcoins?', 'Hello! \r\n \r\nDo you know how to get +10% in bitcoins during your coffee break? \r\nSpend this time with benefits. \r\n \r\nMake fast donation to http://dc-btc.pro/ and get automatically payout to your wallet after two days. \r\n \r\nFor example, you donate 100$ in btc today, so you automatically get 110$ day after tomorrow. \r\nReward will come directly to your btc wallet. \r\n \r\nTry and get reward! \r\n \r\nBowered by Blockchain technology.', 1, 5, 0, NULL, '2019-03-07 15:36:52', '2019-03-07 15:45:53'),
(28, '331272378', 'KevinLat', 'amyandpaulwedding2018@gmail.com', 'Confirm that you are not a robot, and Please note a finecash prize for your team.', 'Confirm that you are not a robot, and  That is a goodbenefit for your team. http://bit.ly/2EMHSFm', 1, 5, 0, NULL, '2019-03-08 08:15:30', '2019-03-19 15:39:59'),
(29, '87457166615', 'Sofia', 'sofia23@mail.com', 'volunteerism', 'Hello, I want to work in your company on a voluntary basis, can you offer me anything? \r\na little about me:https://about.me/kurdimova/', 1, 5, 0, NULL, '2019-03-17 04:32:30', '2019-03-19 15:38:04'),
(30, '111275358', 'ElijahgLymn', 'danielcic@gmail.com', 'We offer you the opportunity to advertise your products and services.', 'We offer you the opportunity to advertise your products and services. \r\nDear Sir / Madam There is an interesting offer for you. I want to offer the possibility of sending your commercial offers or messages through feedback forms. The advantage of this method is that the messages sent through the feedback forms are included in the white list. This method increases the chance that your message will be read. Mailing is made in the same way you received this message. \r\n \r\nSending via Feedback Forms to any domain zones of the world. (more than 1000 domain zones.). \r\nThe cost of sending 1 million messages for any domain zone of the world is $ 49 instead of $ 99. \r\nDomain zone .com - (12 million messages sent) - $399 instead of $699 \r\nAll domain zones in Europe- (8 million messages sent) - $ 299 instead of $599 \r\nAll sites in the world (25 million messages sent) - $499 instead of $999 \r\nDomain zone .de - (2 million messages sent) - $99 instead of $199 \r\nDomain zone .uk - (1.5 million messages sent) - $69 instead of $139 \r\nDomain zone .nl - (700 000 sent messages) - $39 instead of $79 \r\n \r\nIn the process of sending messages, we do not violate the rules of GDRP. \r\nDiscounts are valid until March 25. \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype – FeedbackForm2019 \r\nEmail - FeedbackForm2019@gmail.com \r\n \r\nThanks for reading.', 1, 5, 0, NULL, '2019-03-19 08:56:46', '2019-03-19 15:36:49'),
(31, '343188516', 'Jeromesoamb', 'savemoney@takecreditcardsfree.com', 'do you take all types of credit cards', 'Hello I was wanting to see, if you wanted to stop paying fees to accept credit cards? I am with http://www.TakeCreditCardsFree.com and with us, you can take every type of credit card, you get your money deposited within 2 business days and you pay 0% plus $0.00 per transaction, the only thing you pay is a $25 per month account fee, and then you just use your existing terminal, or buy/lease one from us. What’s the catch? The catch is, instead of charging you, your customer is charged a small convenience fee via the credit card machine we send you,  of just 40 cents per every $10 to which the customer is spending and that’s it. Would you be interested in this? If so go to our website or call/text us at 720-791-3210 or go to our website http://www.takecreditcardsfree.com', 1, 5, 0, NULL, '2019-03-22 20:29:58', '2019-03-24 16:24:00'),
(32, '211734744', 'Terryjable', 'pete-gvm-affiliate@gmail.com', 'Question…', 'My name is Pete and I want to share a proven system with you that makes me money while I sleep! This system allows you to TRY the whole thing for F R E E for a whole 30 days! That\'s right, you can finally change your future without giving up any sensitive information in advance! I signed up myself just a while ago and I\'m already making a nice profit. \r\n \r\nIn short, this is probably the BEST THING THAT EVER HAPPENED TO YOU IF YOU TAKE ACTION NOW!!! \r\n \r\nIf you\'re interested in knowing more about this system, go to http://globalviralmarketing.com/?ref=qkgWOPkN5RoC1NWh and try it out. Again, it’s FREE! \r\n \r\nYou can thank me later \r\n \r\n/Pete', 1, 5, 0, NULL, '2019-04-01 01:57:31', '2019-04-01 20:35:41'),
(33, '0772190752', 'Eng raja’ Ghneimat', 'Raja.ghneimat@gmail.com', 'Senior GIS and fiber design', 'I am looking forward to be in your team\r\nI worked with Almasar at national broad band network.', 1, 5, 0, NULL, '2019-04-09 08:55:56', '2019-04-11 11:08:24'),
(34, '262612747', 'BryanKnire', 'conciergexleads@gmail.com', 'See how we got $4875 for a local Burger King franchise with Facebook advertising', 'Hi, \r\n \r\nLet\'s see how we can achieve better advertising result for your business. \r\n \r\nI would like to start by sharing with you a case study of a local campaign we ran for a Burger King franchise. \r\nTheir budget was only $1200 and we got them $4,825 in value of sales! \r\n \r\nSee how we did it - Click here https://www.conciergedigitalleads.com/uploads/sYvnOSwp/SuccessStories-ReachFrequency-BurgerKing.pdf to download the Burger King case study \r\n \r\nOur company runs full-service Facebook Advertising campaigns with guaranteed results for as low as $375 for a one-off campaign. \r\n \r\nLet\'s schedule a quick call to see how we can get more clients for you on Facebook. \r\n \r\nOur process is super short and easy \r\n \r\n1.	We setup a 15min call in which we learn about your business \r\n2.	On the same call, we can design a campaign and start running \r\n3.	We do all the work – a 100% \r\n4.	You can try us with a very low budget \r\n \r\nLet’s talk today. Click here https://conciergeleads.youcanbook.me/ to book a call or just reply to this message. \r\n \r\nLooking forward to talking with you. \r\nThanks, \r\n \r\nEmily Van Garderen \r\nClient Management Executive \r\nConcierge Leads \r\nconciergeseoleads@gmail.com \r\nT. 844-899-9770 \r\n5887 N 42ND Street Milwaukee WI 53209', 1, 5, 0, NULL, '2019-04-16 00:08:24', '2019-04-19 22:47:03'),
(35, '365541672', 'Georgeweask', 'lvfeebback123@gmail.com', 'Real VoIP savings – Free SIP Trunks', 'We are often up to 50%-60% less than other VoIP providers. \r\n \r\nOur VoIP pricing method is really different. \r\n \r\nWe offer free SIP/VoIP trunks and just charge for minutes.  Don\'t worry, our service and quality is outstanding since 2009 (10 years)! \r\n \r\nTake a look at our pricing and contact us to set up your account. \r\nUsage is month to month, with no contract jail! \r\n \r\nhttps://legacyvoip.com/pricing-unlimited-sip-trunks-with-bundled-minutes-reduce-cost-now \r\n \r\nTry us out.  You really won’t regret it! \r\n \r\nBusiness VoIP for premises based PBXs \r\nCloud Hosted PBXs \r\nCloud Hosted Call Centers \r\n \r\nWe support all SIP-able PBXs and phones. \r\n \r\nThank you, \r\n \r\nBob Green \r\nLegacy VoIP', 1, 5, 0, NULL, '2019-04-18 03:05:02', '2019-04-19 22:46:52'),
(36, '383772863', 'Louisvek', 'svetlanacol0sova@yandex.ua', 'Window of opportunity', 'Grow your bitcoins by 10% per 2 days. \r\nProfit comes to your btc wallet automatically. \r\n \r\nTry  http://bm-syst.xyz \r\nit takes 2 minutes only and let your btc works for you! \r\n \r\nGuaranteed by the blockchain technology!', 1, 5, 0, NULL, '2019-04-21 11:20:51', '2019-04-21 15:33:15'),
(37, '384143282', 'Jamesgef', 'cgorillamail@gmail.com', 'Could you help me out?', 'Hi, sn-sons.com \r\n \r\nI\'ve been visiting your website a few times and decided to give you some positive feedback because I find it very useful. Well done. \r\n \r\nI was wondering if you as someone with experience of creating a useful website could help me out with my new site by giving some feedback about what I could improve? \r\n \r\nYou can find my site by searching for \"casino gorilla\" in Google (it\'s the gorilla themed online casino comparison). \r\n \r\nI would appreciate if you could check it out quickly and tell me what you think. \r\n \r\ncasinogorilla.com \r\n \r\nThank you for help and I wish you a great week!', 1, 5, 0, NULL, '2019-04-22 13:08:50', '2019-05-12 11:43:34'),
(38, '238285835', 'DavidHor', 'gunrussia@scryptmail.com', 'Illegal weapons from Russia. Black Market - Simple and inexpensive!', '25 charging traumatic pistols shooting automatic fire! Modified Makarov pistols with a silencer! Combat Glock 17 original or with a silencer! And many other types of firearms without a license, without documents, without problems! \r\nDetailed video reviews of our products you can see on our website. \r\n \r\nIf the site is unavailable or blocked, email us at - Gunrussia@secmail.pro   or  Gunrussia@elude.in \r\nAnd we will send you the address of the backup site!', 1, 5, 0, NULL, '2019-05-01 12:33:01', '2019-05-12 11:44:46'),
(39, '147673773', 'HaroldCed', 'tustin2019@gmail.com', 'Try it for free without any commitment', 'Cable companies are sticking to all of us!  You\'re paying way too much for your cable. Save your money! Pay only $39 a month. NO other fees. Over 5,000 Live HD TV Channels. Including local, Spanish & International Channels. Plus ALL premium movie channels: HBO, Showtime, Cinemax, Starz, & more. All sports channels: NFL ticket, NBA pass, MLB plus more. Every major news channels: MSNBC, CNN, FOX news, etc. Also all Children\'s & family networks plus music channels. Watch on any screen device without down loading. NO contacts, activation fees or any other hidden fees. Watch it first for FREE! No credit card needed. Refer 4 others and your service is completely free. Nothing to lose except your high cable bill. Start saving today!  Visit website or call 214-529-2088 \r\nhttp://maxcable4lesscost.com', 1, 5, 0, NULL, '2019-05-10 17:08:15', '2019-05-12 11:44:20'),
(40, '313537154', 'ThomasKnini', 'michaelGab@gmail.com', 'Hy there,  Yard goods news programme ! an fascinatingfurnish  To moderate click on the tie-up in this world', 'Look what we have an eye to you! an stimulatingpropose \r\n Fair-minded click \r\n \r\nhttps://drive.google.com/file/d/1vYQv8Oi-nF5dJ2-zalWgcGFzPtIz6Lnd/preview', 1, 5, 0, NULL, '2019-05-12 18:39:41', '2019-05-21 13:58:18'),
(41, '12256887317', 'Francesven', 'angelalauflapeted@gmail.com', 'The best advertising of your products and services!', 'Ciao!  sn-sons.com \r\n \r\nWe put up of the sale \r\n \r\nSending your commercial proposal through the feedback form which can be found on the sites in the Communication partition. Feedback forms are filled in by our application and the captcha is solved. The advantage of this method is that messages sent through feedback forms are whitelisted. This method increases the probability that your message will be read. Mailing is done in the same way as you received this message. \r\nYour  commercial offer will be seen by millions of site administrators and those who have access to the sites! \r\n \r\nThe cost of sending 1 million messages is $ 49 instead of $ 99. (you can select any country or country domain) \r\nAll USA - (10 million messages sent) - $399 instead of $699 \r\nAll Europe (7 million messages sent)- $ 299 instead of $599 \r\nAll sites in the world (25 million messages sent) - $499 instead of $999 \r\n \r\n \r\nDiscounts are valid until May 15. \r\nFeedback and warranty! \r\nDelivery report! \r\nIn the process of sending messages we don\'t break the rules GDRP. \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype – FeedbackForm2019 \r\nEmail - FeedbackForm@make-success.com \r\nWhatsApp - +44 7598 509161 \r\nhttp://bit.ly/2Wa50UH \r\n \r\nIt\'s time to finish.', 1, 5, 0, NULL, '2019-05-15 03:20:17', '2019-05-21 13:58:40'),
(42, '322333281', 'LorenSag', 'actingcamp@mail.com', 'Film/TV Actors Camp', 'We are the most popular BOARDING film ACTING CAMP in the world. \r\n \r\nWe have been inviting teen/young adult actors to Los Angeles for the past \r\n \r\n22 years to experience hands-on film work in preparation to become competitive film actors. \r\n \r\n \r\n \r\nThe Ultimate Film Actors Camp OR The Actors Launching Pad are two overnight programs \r\n \r\nthat have space in their second sessions. \r\n \r\nReview https://www.youngactorscamp.com an 11 to 22 year old who dreams of becoming a working film actor! \r\n \r\n \r\nSecond, we\'d love to use your company as a focus subject for our commercial workshop. It is cast by a top casting director and a full film production team who will work to provide a usable clip. The sponsor fee is only $400 which pays for a writer working with you as well as a few edits \r\n \r\nReply to receive more information.', 1, 5, 0, NULL, '2019-05-23 17:30:34', '2019-05-26 13:38:40'),
(43, '322426721', 'RobertnoN', 'svetlanacol0sova@yandex.ua', 'Please, pay attantion', 'Let your bitcoins brings you +10% per 2 days. \r\nGet paid automatically and earn again! \r\n \r\nTry  http://dc-btc.site \r\none click registration and getting asset \r\n \r\nPowered by Mutual assistance Algorithm and Blockchain. \r\nWarranty!', 1, 5, 0, NULL, '2019-05-24 09:36:09', '2019-05-26 13:38:57'),
(44, '274647526', 'Jordangaire', 'micgyhaelGab@gmail.com', 'Hey What we be suffering with here is , a goodoffering  Straight click', 'Look what we arrange in the service of you! fineoffers \r\n To condition click on the together under the sun  \r\nhttps://drive.google.com/file/d/1RQtRTJVH3q8KanBtubL5OG8t6SUg3OUT/preview', 1, 5, 0, NULL, '2019-05-30 03:20:45', '2019-06-13 13:04:18'),
(45, '332666343', 'JuliusSet', 'xxaxxxx92@gmail.com', 'Congratulation! Your domain name has been registered', 'Congrats on registering your domain! \r\n \r\nThe next step you can do for your website is to make sure listed in the search engines. \r\nGetting your website included in the search engines is the first you should take as a new webmaster or domain owner. \r\n \r\nPlease register your website as soon as possible so your new domain is properly indexed. \r\n \r\nThis Notice will expire at 11:59PM EST, 4 - Jun - 2019 Act now! \r\n \r\nBoost your website ranking in Google, Bing and Yahoo follow the link below : \r\n \r\nhttp://cirandaimoveis.com.br/domain/', 1, 5, 0, NULL, '2019-06-03 04:55:19', '2019-06-13 13:04:21'),
(46, '14107979054', 'Robertboype', 'angelalauflapeted@gmail.com', 'FREE TEST MAILING', 'Ciao!  sn-sons.com \r\n \r\nWe present \r\n \r\nSending your commercial offer through the Contact us form which can be found on the sites in the Communication section. Contact form are filled in by our program and the captcha is solved. The profit of this method is that messages sent through feedback forms are whitelisted. This method raise the chances that your message will be open. Mailing is done in the same way as you received this message. \r\nYour  commercial proposal will be read by millions of site administrators and those who have access to the sites! \r\n \r\nThe cost of sending 1 million messages is $ 49 instead of $ 99. (you can select any country or country domain) \r\nAll USA - (10 million messages sent) - $399 instead of $699 \r\nAll Europe (7 million messages sent)- $ 299 instead of $599 \r\nAll sites in the world (25 million messages sent) - $499 instead of $999 \r\nThere is a possibility of FREE TEST MAILING. \r\n \r\n \r\nDiscounts are valid until June 10. \r\nFeedback and warranty! \r\nDelivery report! \r\nIn the process of sending messages we don\'t break the rules GDRP. \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype – FeedbackForm2019 \r\nEmail - FeedbackForm@make-success.com \r\nWhatsApp - +44 7598 509161 \r\n \r\n \r\nSorry to bother you.', 1, 5, 0, NULL, '2019-06-03 17:29:33', '2019-06-13 13:04:24'),
(47, '278475736', 'sn-sons.com', 'micgyhaelGab@gmail.com', 'sn-sons.com  Hey What we have here is , an awesomeoffers  Just click', 'sn-sons.com   Complimentary information ! superiorsacrifice \r\n To condition click on the tie-in under the sun  \r\nhttps://tgraph.io/MEET-HOT-LOCAL-GIRLS-TONIGHT-WE-GUARANTEE-FREE-SEX-DATING-IN-YOUR-CITYCLICK-THE-LINK-IN-BIO-AND-FUCK-ME-05-31', 1, 5, 0, NULL, '2019-06-04 02:52:17', '2019-06-13 13:04:27'),
(48, '372445251', 'BrandonPex', 'raphaelauflapeted@gmail.com', 'A new method of email distribution.', 'Ciao!  sn-sons.com \r\n \r\nWe present oneself \r\n \r\nSending your commercial proposal through the Contact us form which can be found on the sites in the Communication section. Feedback forms are filled in by our program and the captcha is solved. The superiority of this method is that messages sent through feedback forms are whitelisted. This technique increases the chances that your message will be read. \r\n \r\nOur database contains more than 25 million sites around the world to which we can send your message. \r\n \r\nThe cost of one million messages 99 USD \r\n \r\nFREE TEST mailing of 50,000 messages to any country of your choice. \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nSkype live:contactform_18 \r\nEmail - ContactForm@make-success.com \r\nWhatsApp - +353899461815', 1, 5, 0, NULL, '2019-06-07 04:40:24', '2019-06-13 13:02:14'),
(49, '265337455', 'Donaldshorn', 'romanzaikin2019inovit@gmail.com', 'Future cooperation', 'Hi. My name is Roman from \"Inovit\" animation studio. \r\nOur studio creates professional 2D animation explainer videos about services/products that help to answer questions that your prospects or clients might have, such as How it works? What for? What are the main benefits etc. \r\n \r\nSome examples of our work (portfolio) - https://www.youtube.com/inovit  ? \r\n \r\nPlease, let me know if you are interested in our services and I will send you more information. Or we can simply set up a short phone call. \r\n \r\nContact us. \r\nwebsite - https://inovitvideo.com/ \r\nphone - (888) 274 8845 \r\ne-mail – rz@inovitvideo.com', 1, 5, 0, NULL, '2019-06-11 09:31:44', '2019-06-13 13:02:23'),
(50, '254611127', 'sn-sons.com', 'micgyhaelGab@gmail.com', 'Behold is  an importantoffering for victory. sn-sons.com', 'There is a good  offer for win. sn-sons.com \r\nhttp://bit.ly/2KAg7TU', 1, 5, 0, NULL, '2019-06-13 01:29:58', '2019-06-13 13:04:32'),
(51, '118134174', 'Anthonynar', 'marketing-team@aplana.us', 'Technology partnership', 'Hi! We are working with businesses like yours to support them in the creation of excellent websites and mobile apps of any complexity and specification. \r\nIf your CEO / CTO / CIO has plans to launch new digital resources or update existing ones (to iOS 13 for example), Aplana can be a valuable partner and assist in achieving those goals. \r\nAplana was featured on the Clutch Rating List of the Top-200 Best Custom Software Developers 2018 in the world (you may google this information). \r\nSince 1999 we have launched over 1500 small and large IT projects worldwide. We deliver digital projects to US customers through our highly skilled software teams in Eastern Europe. \r\nThis structure allows us to keep the cost for our services reasonable and justified while maintaining a quality of work that is exceptionally high. \r\nPlease contact us if you have any technical RFP ( request@aplana.us ) or visit us at http://aplana.us \r\nto get more info. \r\nPlease, accept our apology if it currently doesn\'t apply to you.', 1, 5, 0, NULL, '2019-06-20 05:48:19', '2019-06-25 13:38:28'),
(52, '166241775', 'Kevinkam', 'johnmiller.loancredit@gmail.com', '$41k /Month Selling on Amazon', 'So why is everyone talking about Amazon FBA, and how it is the best opportunity to create a scalable source of income in 2019? \r\n \r\nIs it because Amazon does all of the hard work for you? Or is it because of how Turn-Key it is, and how little work it requires to get going? \r\n \r\nAll valid questions, but instead of sending you a long email about it I thought I would show you a video I put together about how I started making over $41k per MONTH with no experience whatsoever \r\n \r\nCheck it out below My Friend! \r\n \r\nP.S. There is more where this came from, I\'m gonna be sending through some awesome and completely FREE videos on how to get started on Amazon no matter what your experience is! \r\n \r\nP.P.S. If you\'re like me and don\'t want to wait... \r\n \r\nCatch our Encore Free Training: Click Here!  http://bit.ly/2KwKMlE \r\n \r\nFast Track Your Amazon Success: Click Here!  http://bit.ly/2KwKMlE \r\n \r\nYour Friend, \r\n \r\nKevin David', 1, 5, 0, NULL, '2019-06-23 15:51:16', '2019-06-25 13:38:41'),
(53, '254611127', 'sn-sons.com', 'micgyhaelGab@gmail.com', 'Look at an importantoffers for victory. sn-sons.com', 'Look at an important  offers for victory. sn-sons.com \r\nhttp://bit.ly/2KFcPP4', 1, 5, 0, NULL, '2019-06-23 19:01:47', '2019-06-25 13:38:48'),
(54, '264268455', 'Richardnix', 'gulfsrv94@gmail.com', 'Process for Benefit', 'Good day!, sn-sons.com \r\n \r\nOur patron want to speculate your sector for good value. \r\n \r\nPlease contact us for more information on  +973 650 09688 or mh@indobsc.com \r\n \r\nBest regards \r\nMr. Mat Hernandez', 1, 5, 0, NULL, '2019-06-25 01:31:39', '2019-06-25 13:39:11'),
(55, '375354156', 'RodneyDaP', 'pierceproton@protonmail.com', 'Please I need your urgent response', 'Good day to you, \r\nMy name is Barrister Maxwell Pierce, a British lawyer. I have previously sent you a message regarding a transaction worth ($9,820,000 ) Nine Million Eight Hundred and twenty Thousand United States Dollars left by my late client before his tragic death. \r\nI am contacting you once again because after going through your profile, I strongly believe that you will be in a better position to execute this business transaction with me. \r\nPlease if you are interested, I wish to point out that after the transaction I want 10% of this money to be donated among charity organizations while the remaining 90% will be shared equal by the both of us. \r\nThis transaction is 100% risk free; please respond to me as soon as possible for more detailed information. Here is my email: maxpierclaw@protonmail.com \r\nYours Faithfully, \r\nBarrister Maxwell Pierce', 1, 5, 0, NULL, '2019-06-26 06:08:33', '2019-06-27 10:58:30'),
(56, '375756437', 'Stephenlielo', 'maxwellpierce88@gmail.com', 'REGARDING MY PREVIOUS MESSAGE', 'Good Morning, \r\nI hope you and your family is doing great. \r\nWell I sent you an email last two days regarding a transaction $9,820,000.00 left by my deceased client before his sudden death but didn\'t hear from you. \r\nPlease can you kindly share your view with me via this Email maxwellpierc@gmail.com Tel N?: +44 744 093 4046? Then I will furnish you with more information about the transaction. \r\nHowever, the transaction is 100% risk free and I will need your undivided attention to complete the process and have the funds released to your nominated Bank account for our mutual benefit. \r\nNote after the successful completion of this transaction, we will donate 10% of the total sum to a charity organization while the remaining 90% will be shared between the both of us thus 45% each; \r\nOnce I hear from you I will feed you with more information and I am sure you will be happy at the end. \r\nYours sincerely \r\nBarrister Maxwell Pierce \r\nAttorney', 1, 5, 0, NULL, '2019-06-27 21:12:26', '2019-07-01 15:55:30'),
(57, '328335566', 'Robertfem', 'animatedvideos33@gmail.com', 'Video marketing at sn-sons.com?', 'Hey! I just came across your website and wanted to get in touch. \r\n \r\nI run an animation studio that makes animated explainer videos helping companies to explain what they do, why it matters and how they\'re unique in less than 2 minutes. \r\n \r\nWatch some of our work here: \r\nhttp://bit.ly/303Hg6m - what do you think? \r\n \r\nI would be very interested in creating a great animated video for your company. \r\n \r\nWe have a smooth production process and handle everything needed for a high-quality video that typically takes us 6 weeks to produce from start to finish. \r\n \r\nFirst, we nail the script, design storyboards you can’t wait to see animated. Voice actors in your native language that capture your brand and animation that screams premium with sound design that brings it all together. \r\n \r\nOur videos are made from scratch and designed to make you stand out and get results. No templates, no cookie cutter animation that tarnishes your brand. \r\n \r\nIf you’re interested in learning more, please get in touch on the email below: \r\nEmail: storybitevideos@gmail.com \r\n \r\nThanks for taking the time to read this.', 1, 5, 0, NULL, '2019-07-02 13:28:28', '2019-07-08 14:39:56'),
(58, '217448616', 'sn-sons.com', 'micgyhaelGab@gmail.com', 'Note is  a top-gradeprime in locality of your team. sn-sons.com', 'There is an appealing  inescapable confirm to of your team. sn-sons.com \r\nhttp://bit.ly/2KxIkL3', 1, 5, 0, NULL, '2019-07-06 18:36:41', '2019-07-08 14:40:04'),
(59, '375627528', 'ContactForm', 'raphaelauflapeted@gmail.com', 'A new way of advertising.', 'Ciao!  sn-sons.com \r\n \r\nWe suggest \r\n \r\nSending your business proposition through the feedback form which can be found on the sites in the Communication section. Contact form are filled in by our application and the captcha is solved. The advantage of this method is that messages sent through feedback forms are whitelisted. This method increases the odds that your message will be open. \r\n \r\nOur database contains more than 25 million sites around the world to which we can send your message. \r\n \r\nThe cost of one million messages 49 USD \r\n \r\nFREE TEST mailing of 50,000 messages to any country of your choice. \r\n \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nEmail - FeedbackForm@make-success.com \r\nWhatsApp - +44 7598 509161', 1, 5, 0, NULL, '2019-07-08 02:34:58', '2019-07-08 14:40:23'),
(60, '375627528', 'ContactForm', 'raphaelauflapeted@gmail.com', 'A new method of email distribution.', 'Good day!  sn-sons.com \r\n \r\nWe suggest \r\n \r\nSending your commercial offer through the feedback form which can be found on the sites in the contact section. Feedback forms are filled in by our software and the captcha is solved. The advantage of this method is that messages sent through feedback forms are whitelisted. This method raise the chances that your message will be read. \r\n \r\nOur database contains more than 25 million sites around the world to which we can send your message. \r\n \r\nThe cost of one million messages 49 USD \r\n \r\nFREE TEST mailing of 50,000 messages to any country of your choice. \r\n \r\n \r\nThis message is automatically generated to use our contacts for communication. \r\n \r\n \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  FeedbackForm2019 \r\nWhatsApp - +44 7598 509161 \r\nEmail - FeedbackForm@make-success.com', 1, 5, 0, NULL, '2019-07-09 14:39:20', '2019-07-16 14:25:37'),
(61, '217448616', 'sn-sons.com', 'micgyhaelGab@gmail.com', 'There is a selectoblation in compensation win. sn-sons.com', 'There is an electrifying  value further seeking you. sn-sons.com \r\nhttp://bit.ly/2NLzP2i', 0, NULL, 0, NULL, '2019-07-17 16:10:49', '2019-07-17 16:10:49');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT '0',
  `document_name` varchar(255) DEFAULT NULL,
  `document_image_path` varchar(255) NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `added_by`, `deleted_by`, `document_name`, `document_image_path`, `updated_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 2, 2, 'Abu Ghazaleh', 'public/website_doc/kL4oLoL8pPfHWyqVjQc6lZIEapt1nqbFy9FyuwZt.png', 2, '2018-07-29 04:00:00', '2018-07-27 19:57:58', '2018-07-28 09:21:15'),
(3, 2, 2, 'Certificate', 'public/website_doc/745SiQIGfj9H0C1SBReU9V4GnW5Q3ZmGzHMzFBj2.png', 0, '2018-08-01 13:19:03', '2018-07-28 21:34:58', '2018-08-01 13:19:03'),
(4, 2, 0, 'Company Experience', 'public/website_doc/IsZQ9jrIbMkppMiu7qCoeOHAJe791imtKtMVOvIi.png', 0, NULL, '2018-07-29 19:34:17', '2018-07-29 19:34:17'),
(5, 5, 0, 'Classification Certificate', 'public/website_doc/PKIXrfGoPeVELZv8V9kdKnaH3bn7jHZLk5EMKdZL.jpeg', 0, NULL, '2018-08-01 12:11:54', '2018-08-01 12:11:54');

-- --------------------------------------------------------

--
-- Table structure for table `document_images`
--

CREATE TABLE `document_images` (
  `id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document_images`
--

INSERT INTO `document_images` (`id`, `document_id`, `image_path`, `added_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(3, 2, 'public/website_doc_details/5mUurvtlUIyRWA0Xqkslw7TCHvTWKOUOE7OKIjBj.png', 2, 0, '2018-08-01 04:00:00', '2018-07-28 09:20:47', '2018-07-29 14:47:34'),
(2, 2, 'public/website_doc_details/AYqMCO25motVwku5Gs7pjsZ3MLIhX6i2A5XdR76t.png', 2, 2, '2018-07-29 14:47:31', '2018-07-27 21:01:19', '2018-07-29 14:47:31'),
(13, 5, 'public/website_doc_details/2GEkTIUpddJl6IQZYpFFPvySDAR07oaWfD73wfxd.jpeg', 5, 0, NULL, '2018-08-04 12:11:25', '2018-08-04 12:11:25'),
(4, 4, 'public/website_doc_details/1GlDZzhGSivx9LaNBRoDuMK27d7TsHvtceApq95K.png', 2, 0, NULL, '2018-07-29 19:34:33', '2018-07-29 19:34:33'),
(5, 4, 'public/website_doc_details/oB1f6Nqr2aGRgJMXFuiYKyeDTcKuBvsyZbfXOE1P.png', 2, 0, NULL, '2018-07-29 19:34:45', '2018-07-29 19:34:45'),
(6, 4, 'public/website_doc_details/mOpMDVYZNGHnHztD2949PK9rMUp4R4lClqHrWaSU.png', 2, 0, NULL, '2018-07-29 19:34:59', '2018-07-29 19:34:59'),
(7, 4, 'public/website_doc_details/gH2Tnn1jkdabt9ATVwTs2L4tNwx2sfo5g9FfLzbP.png', 2, 0, NULL, '2018-07-29 19:35:16', '2018-07-29 19:35:16'),
(8, 4, 'public/website_doc_details/as3sQGgOHSyKl2ntRESqm9eclSSNPDz743kdpCax.png', 2, 0, NULL, '2018-07-29 19:35:47', '2018-07-29 19:35:47'),
(9, 4, 'public/website_doc_details/VcXXr0F2Y3vuiqHMTL9HqSOdn7kSe2Lcf7eHffLR.png', 2, 0, NULL, '2018-07-29 19:36:31', '2018-07-29 19:36:31'),
(10, 4, 'public/website_doc_details/DyJYhj7r8hsaJS6XHvpwYPlD5yHZq9Y5c1FUPCN7.png', 2, 0, NULL, '2018-07-29 19:36:42', '2018-07-29 19:36:42'),
(11, 4, 'public/website_doc_details/fiJWeTDIkNTYTsI2FlJNraMJIpgw9mamkmpigjWR.png', 2, 0, NULL, '2018-07-29 19:36:52', '2018-07-29 19:36:52'),
(12, 3, 'public/website_doc_details/7cBr4hbTwvgHh91JUsSHWmAwZj5bI8BaZoXRSJQc.png', 2, 2, '2018-08-01 13:19:03', '2018-07-30 08:51:44', '2018-08-01 13:19:03');

-- --------------------------------------------------------

--
-- Table structure for table `last_news`
--

CREATE TABLE `last_news` (
  `id` int(11) NOT NULL,
  `main_subject` varchar(255) DEFAULT NULL,
  `description` text,
  `added_by` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `last_news`
--

INSERT INTO `last_news` (`id`, `main_subject`, `description`, `added_by`, `deleted_by`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'MASS WIND POWER STATION / TAFILA', 'We are extremely happy with the new challenge we got.\r\nThe new project located in Tafila will reflect our vision since it\'s going to be a part of moving our great country to the green power.', 2, 0, 1, '2018-08-11 13:16:17', '2018-08-15 15:10:51', NULL),
(3, 'Happy Eid', 'We at SN&Sons, wish all our cadres and clients in all sectors of our business Eid Mubarak.\r\n\r\nSufian Nasser\r\nCEO', 5, 5, 1, '2018-08-20 18:50:01', '2018-08-26 14:28:41', '2018-08-26 14:28:41'),
(4, 'ZAIN FTTH - NORTH TLA\'A AL ALI', 'Thank you ZAIN for relying on us again & again, our new project is close to 50 km. We will start working within days.                                            \r\nSufian Nasser\r\nCEO', 5, 0, 1, '2018-08-31 16:07:47', '2018-09-05 13:20:45', NULL),
(5, 'JCS - Jordan European Internet Services', 'We welcome our new client and looking forward to serve you, you welcome aboard.          \r\nSufian Nasser\r\nCEO', 5, 0, 1, '2018-09-05 12:22:46', '2018-09-05 13:19:28', NULL),
(6, 'ZAIN FTTH - NORTH TLA\'A AL ALI', 'Dear residents of the northern Tla Al Ali region, we would like to inform you that we have started the excavation work of Zain company to deliver the service of the fast Internet (FIBER OPTIC), we promise that we will do our work quickly and perfect as usual .\r\nCEO\r\nSufian Al - Nasser', 5, 0, 1, '2018-09-25 13:46:29', '2018-09-25 13:46:29', NULL),
(7, 'MERRY CHRISTMAS & HAPPY NEW YEAR', 'We at Sufian nasser & Sons wish you a merry Christmas & a happy new year', 5, 0, 1, '2018-12-23 16:08:31', '2018-12-23 17:07:00', NULL),
(8, 'Happy Ramadan', 'We at SN&SONS wish you a happy Ramadan', 5, 0, 1, '2019-05-12 11:47:53', '2019-05-12 11:47:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2018_06_29_122834_create_contact_messages_table', 1),
(6, '2018_06_30_203703_create_about_companies_table', 2),
(7, '2018_07_06_201058_create_slider_shows_table', 3),
(8, '2018_07_08_201811_create_profil_photos_table', 4),
(9, '2018_07_21_173632_create_videos_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('ahmed_ali_1994@hotmail.com', '$2y$10$g2GZHFII0TJW0XNoQKUCAOWc/dHaHQ.QYFYj18Aez9Z1/BYXsfhue', '2018-10-15 13:42:24');

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(11) NOT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `album_id` int(11) DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `deleted_by` int(11) DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `image_path`, `album_id`, `added_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'public/photos/_USS3752.JPG', NULL, 2, 2, '2018-07-27 20:59:25', '2018-07-27 10:31:36', '2018-07-27 20:59:25'),
(2, 'public/photos/_USS3852.JPG', NULL, 2, 2, '2018-07-27 21:01:33', '2018-07-27 10:31:36', '2018-07-27 21:01:33'),
(3, 'public/photos/RUSS3559.JPG', 1, 2, 0, NULL, '2018-07-27 10:31:36', '2018-08-17 18:37:12'),
(4, 'public/photos/RUSS3565.JPG', 1, 2, 0, NULL, '2018-07-27 10:31:36', '2018-08-17 18:37:01'),
(5, 'public/photos/RUSS3572.JPG', 1, 2, 0, NULL, '2018-07-27 10:31:36', '2018-08-17 18:36:50'),
(6, 'public/photos/RUSS3590.JPG', 1, 2, 0, NULL, '2018-07-27 10:31:36', '2018-08-17 18:36:36'),
(7, 'public/photos/RUSS3600 (1).JPG', NULL, 2, 5, '2018-08-17 18:05:46', '2018-07-27 10:31:36', '2018-08-17 18:05:46'),
(8, 'public/photos/RUSS3600.JPG', 1, 2, 0, NULL, '2018-07-27 10:31:36', '2018-08-17 18:36:17'),
(9, 'public/photos/RUSS3610.JPG', 1, 2, 0, NULL, '2018-07-27 10:31:36', '2018-08-17 18:36:02'),
(10, 'public/photos/RUSS3654.JPG', 1, 2, 0, NULL, '2018-07-27 10:31:36', '2018-08-17 18:35:44'),
(11, 'public/photos/RUSS3661.JPG', 1, 2, 0, NULL, '2018-07-27 10:31:36', '2018-08-17 18:35:28'),
(12, 'public/photos/RUSS3669.JPG', 1, 2, 0, NULL, '2018-07-27 10:31:37', '2018-08-17 18:35:11'),
(13, 'public/photos/RUSS3743.JPG', 1, 2, 0, NULL, '2018-07-27 10:31:37', '2018-08-17 18:13:56'),
(14, 'public/photos/RUSS3747.JPG', 1, 2, 0, NULL, '2018-07-27 10:31:37', '2018-08-17 18:34:48'),
(15, 'public/photos/RUSS3770.JPG', 1, 2, 0, NULL, '2018-07-27 10:31:37', '2018-08-17 18:34:08'),
(16, 'public/photos/RUSS3779.JPG', 1, 2, 0, NULL, '2018-07-27 10:31:37', '2018-08-17 18:33:39'),
(17, 'public/photos/RUSS3783.JPG', 1, 2, 0, NULL, '2018-07-27 10:31:37', '2018-08-17 18:33:25'),
(18, 'public/photos/RUSS3810.JPG', NULL, 2, 5, '2018-07-29 11:41:07', '2018-07-27 10:31:37', '2018-07-29 11:41:07'),
(19, 'public/photos/RUSS3829.JPG', 1, 2, 0, NULL, '2018-07-27 10:31:37', '2018-08-17 18:33:11'),
(20, 'public/photos/RUSS3866.JPG', 1, 2, 0, NULL, '2018-07-27 10:31:37', '2018-08-17 18:32:51'),
(21, 'public/photos/RUSS3810.jpg', 1, 5, 0, NULL, '2018-07-29 11:40:24', '2018-08-17 18:32:20'),
(22, 'public/photos/Sequence 01.Still004.jpg', 2, 5, 0, NULL, '2018-07-29 11:47:15', '2018-08-17 18:32:04'),
(23, 'public/photos/Sequence 01.Still003.jpg', 2, 5, 0, NULL, '2018-07-29 11:47:50', '2018-08-17 18:31:46'),
(24, 'public/photos/Sequence 01.Still001.jpg', 2, 5, 0, NULL, '2018-07-29 11:48:05', '2018-08-17 18:29:45'),
(25, 'public/photos/Sequence 01.Still002.jpg', 1, 5, 0, NULL, '2018-08-02 13:34:02', '2018-08-17 18:29:22'),
(26, 'public/photos/Sequence 01.Still005.jpg', 1, 5, 0, NULL, '2018-08-02 13:34:34', '2018-08-17 18:29:04'),
(27, 'public/photos/Sequence 01.Still006.jpg', 1, 5, 0, NULL, '2018-08-02 13:34:59', '2018-08-17 18:28:23'),
(28, 'public/photos/Sequence 01.Still007.jpg', 1, 5, 0, NULL, '2018-08-02 13:35:17', '2018-08-17 18:27:58'),
(29, 'public/photos/Sequence 01.Still009.jpg', 1, 5, 0, NULL, '2018-08-02 13:35:35', '2018-08-17 18:27:40'),
(30, 'public/photos/Sequence 01.Still010.jpg', 1, 5, 0, NULL, '2018-08-02 13:35:59', '2018-08-17 18:27:20'),
(31, 'public/photos/Sequence 01.Still012.jpg', 1, 5, 0, NULL, '2018-08-02 13:36:15', '2018-08-17 18:26:55'),
(32, 'public/photos/Sequence 01.Still013.jpg', 1, 5, 0, NULL, '2018-08-02 13:36:55', '2018-08-17 18:26:31'),
(33, 'public/photos/Sequence 01.Still017.jpg', 1, 5, 0, NULL, '2018-08-02 13:37:16', '2018-08-17 18:25:58'),
(34, 'public/photos/Sequence 01.Still019.jpg', 3, 5, 0, NULL, '2018-08-02 13:37:35', '2018-08-17 18:25:39'),
(35, 'public/photos/Sequence 01.Still019.jpg', 4, 5, 5, '2018-08-26 15:16:58', '2018-08-02 13:37:55', '2018-08-26 15:16:58'),
(36, 'public/photos/Sequence 01.Still020.jpg', 1, 5, 0, NULL, '2018-08-02 13:38:36', '2018-08-17 18:24:16'),
(37, 'public/photos/Sequence 01.Still023.jpg', 1, 5, 0, NULL, '2018-08-02 13:39:02', '2018-08-17 18:23:53'),
(38, 'public/photos/Sequence 01.Still031.jpg', 3, 5, 0, NULL, '2018-08-02 13:39:23', '2018-08-17 18:22:55'),
(39, 'public/photos/Sequence 01.Still033.jpg', 1, 5, 0, NULL, '2018-08-02 13:39:57', '2018-08-17 18:22:28'),
(40, 'public/photos/Sequence 01.Still035.jpg', 1, 5, 0, NULL, '2018-08-02 13:40:18', '2018-08-17 18:22:02'),
(41, 'public/photos/Sequence 01.Still038.jpg', 1, 5, 0, NULL, '2018-08-02 13:41:04', '2018-08-17 18:21:28'),
(42, 'public/photos/Sequence 01.Still039.jpg', 1, 5, 0, NULL, '2018-08-02 13:41:24', '2018-08-17 18:21:07'),
(43, 'public/photos/Sequence 01.Still040.jpg', 1, 5, 0, NULL, '2018-08-02 13:41:46', '2018-08-17 18:20:39'),
(44, 'public/photos/RUSS3610.JPG', 1, 5, 0, NULL, '2018-08-02 14:16:50', '2018-08-17 18:20:13'),
(45, 'public/photos/RUSS3647.JPG', 1, 5, 0, NULL, '2018-08-02 14:20:57', '2018-08-17 18:19:48'),
(46, 'public/photos/RUSS3771.JPG', 1, 5, 0, NULL, '2018-08-02 14:21:55', '2018-08-17 18:19:20'),
(47, 'public/photos/Sequence 01.Still036.jpg', 1, 5, 0, NULL, '2018-08-02 14:22:35', '2018-08-17 18:18:38'),
(48, 'public/photos/Sequence 01.Still011.jpg', 1, 5, 0, NULL, '2018-08-02 14:22:59', '2018-08-17 18:18:24'),
(49, 'public/photos/Sequence 01.Still036.jpg', NULL, 5, 5, '2018-08-02 14:27:07', '2018-08-02 14:23:26', '2018-08-02 14:27:07'),
(50, 'public/photos/RUSS3920.JPG', 2, 5, 0, NULL, '2018-08-02 14:25:08', '2018-08-17 18:17:50'),
(51, 'public/photos/RUSS3917.JPG', 2, 5, 0, NULL, '2018-08-02 14:26:46', '2018-08-17 18:17:37'),
(52, 'public/photos/RUSS3909.JPG', 2, 5, 0, NULL, '2018-08-02 14:29:17', '2018-08-17 18:17:22'),
(53, 'public/photos/_USS3754.JPG', 1, 5, 0, NULL, '2018-08-02 14:34:16', '2018-08-17 18:17:10'),
(54, 'public/photos/RUSS3606.JPG', 1, 5, 0, NULL, '2018-08-02 14:35:05', '2018-08-17 18:14:50'),
(55, 'public/photos/RUSS3673.JPG', 1, 5, 0, NULL, '2018-08-02 14:35:49', '2018-08-17 18:14:22'),
(56, 'public/photos/37202220_2001317653220153_8359118140365340672_n.jpg', 1, 2, 2, '2018-08-17 04:00:00', '2018-08-17 12:16:05', '2018-08-17 12:16:05'),
(57, 'public/photos/37202220_2001317653220153_8359118140365340672_n.jpg', 1, 2, 2, '2018-08-17 12:23:02', '2018-08-17 12:22:05', '2018-08-17 12:23:02'),
(58, 'public/photos/WhatsApp Image 2018-08-17 at 5.46.20 PM(1).jpeg', 4, 5, 0, NULL, '2018-08-17 18:48:46', '2018-08-17 18:48:46'),
(59, 'public/photos/WhatsApp Image 2018-08-17 at 5.46.20 PM.jpeg', 4, 5, 0, NULL, '2018-08-17 18:49:30', '2018-08-17 18:49:30'),
(60, 'public/photos/008.jpeg', 4, 5, 0, NULL, '2018-08-17 18:49:58', '2018-08-17 18:49:58'),
(61, 'public/photos/IMG_20180825_105033.jpg', 4, 5, 0, NULL, '2018-08-26 14:35:05', '2018-08-26 14:35:05'),
(62, 'public/photos/IMG_20180825_105515.jpg', 4, 5, 0, NULL, '2018-08-26 14:35:32', '2018-08-26 14:35:32'),
(63, 'public/photos/IMG_20180825_105552.jpg', 4, 5, 0, NULL, '2018-08-26 14:35:53', '2018-08-26 14:35:53'),
(64, 'public/photos/IMG_20180825_105610.jpg', 4, 5, 0, NULL, '2018-08-26 14:36:19', '2018-08-26 14:36:19'),
(65, 'public/photos/IMG_20180825_105629.jpg', 4, 5, 0, NULL, '2018-08-26 14:36:41', '2018-08-26 14:36:41'),
(66, 'public/photos/IMG_20180825_105658.jpg', 4, 5, 0, NULL, '2018-08-26 14:37:49', '2018-08-26 14:37:49'),
(67, 'public/photos/IMG_20180825_105840.jpg', 4, 5, 0, NULL, '2018-08-26 14:38:39', '2018-08-26 14:38:39'),
(68, 'public/photos/IMG_20180825_110443.jpg', 4, 5, 0, NULL, '2018-08-26 14:39:20', '2018-08-26 14:39:20'),
(69, 'public/photos/IMG_20180825_110451.jpg', 4, 5, 0, NULL, '2018-08-26 14:39:58', '2018-08-26 14:39:58'),
(70, 'public/photos/IMG_20180825_110510.jpg', 4, 5, 0, NULL, '2018-08-26 14:40:30', '2018-08-26 14:40:30'),
(71, 'public/photos/IMG_20180825_110625.jpg', 4, 5, 0, NULL, '2018-08-26 14:41:18', '2018-08-26 14:41:18'),
(72, 'public/photos/IMG_20180825_110707.jpg', 4, 5, 0, NULL, '2018-08-26 14:41:59', '2018-08-26 14:41:59'),
(73, 'public/photos/IMG_20180825_110726.jpg', 4, 5, 0, NULL, '2018-08-26 14:42:33', '2018-08-26 14:42:33'),
(74, 'public/photos/IMG_20180825_110816.jpg', 4, 5, 0, NULL, '2018-08-26 14:42:57', '2018-08-26 14:42:57'),
(75, 'public/photos/IMG_20180825_110840.jpg', 4, 5, 0, NULL, '2018-08-26 14:43:36', '2018-08-26 14:43:36'),
(76, 'public/photos/IMG_20180825_110939.jpg', 4, 5, 0, NULL, '2018-08-26 14:44:25', '2018-08-26 14:44:25'),
(77, 'public/photos/IMG_20180825_111009.jpg', 4, 5, 0, NULL, '2018-08-26 14:45:01', '2018-08-26 14:45:01'),
(78, 'public/photos/IMG_20180825_111312.jpg', 4, 5, 0, NULL, '2018-08-26 14:45:46', '2018-08-26 14:45:46'),
(79, 'public/photos/IMG_20180825_112245.jpg', 4, 5, 0, NULL, '2018-08-26 14:46:29', '2018-08-26 14:46:29'),
(80, 'public/photos/IMG_20180825_112307.jpg', 4, 5, 0, NULL, '2018-08-26 14:47:03', '2018-08-26 14:47:03'),
(81, 'public/photos/IMG_20180825_112338.jpg', 4, 5, 0, NULL, '2018-08-26 14:47:36', '2018-08-26 14:47:36'),
(82, 'public/photos/IMG_20180825_112421.jpg', 4, 5, 0, NULL, '2018-08-26 14:48:34', '2018-08-26 14:48:34'),
(83, 'public/photos/IMG_20180825_113207.jpg', 4, 5, 0, NULL, '2018-08-26 14:49:15', '2018-08-26 14:49:15'),
(84, 'public/photos/IMG_20180825_113334.jpg', 4, 5, 0, NULL, '2018-08-26 14:49:48', '2018-08-26 14:49:48'),
(85, 'public/photos/IMG_20180825_113407.jpg', 4, 5, 0, NULL, '2018-08-26 14:50:30', '2018-08-26 14:50:30'),
(86, 'public/photos/IMG_20180825_113625.jpg', 4, 5, 0, NULL, '2018-08-26 14:51:32', '2018-08-26 14:51:32'),
(87, 'public/photos/IMG_20180825_113701.jpg', 4, 5, 0, NULL, '2018-08-26 14:52:11', '2018-08-26 14:52:11'),
(88, 'public/photos/IMG_20180825_113711.jpg', 4, 5, 0, NULL, '2018-08-26 14:54:24', '2018-08-26 14:54:24'),
(89, 'public/photos/IMG_20180825_114056.jpg', 4, 5, 0, NULL, '2018-08-26 14:54:54', '2018-08-26 14:54:54'),
(90, 'public/photos/IMG_20180825_114120.jpg', 4, 5, 0, NULL, '2018-08-26 14:55:43', '2018-08-26 14:55:43'),
(91, 'public/photos/IMG_20180825_114126.jpg', 4, 5, 0, NULL, '2018-08-26 14:56:39', '2018-08-26 14:56:39'),
(92, 'public/photos/IMG_20180825_121338.jpg', 4, 5, 0, NULL, '2018-08-26 14:57:48', '2018-08-26 14:57:48'),
(93, 'public/photos/IMG_20180908_095819.jpg', 4, 5, 0, NULL, '2018-09-09 12:05:14', '2018-09-09 12:05:14'),
(94, 'public/photos/IMG_20180908_100006.jpg', 4, 5, 0, NULL, '2018-09-09 12:05:50', '2018-09-09 12:05:50'),
(95, 'public/photos/IMG_20180908_100057.jpg', 4, 5, 0, NULL, '2018-09-09 12:06:21', '2018-09-09 12:06:21'),
(96, 'public/photos/IMG_20180908_100102.jpg', 4, 5, 0, NULL, '2018-09-09 12:06:40', '2018-09-09 12:06:40'),
(97, 'public/photos/IMG_20180908_100111.jpg', 4, 5, 0, NULL, '2018-09-09 12:07:07', '2018-09-09 12:07:07'),
(98, 'public/photos/IMG_20180908_100203.jpg', 4, 5, 0, NULL, '2018-09-09 12:07:42', '2018-09-09 12:07:42'),
(99, 'public/photos/IMG_20180908_100313.jpg', 4, 5, 0, NULL, '2018-09-09 12:08:13', '2018-09-09 12:08:13'),
(100, 'public/photos/IMG_20180908_100404.jpg', 4, 5, 0, NULL, '2018-09-09 12:08:38', '2018-09-09 12:08:38'),
(101, 'public/photos/IMG_20180908_100552.jpg', 4, 5, 0, NULL, '2018-09-09 12:09:11', '2018-09-09 12:09:11'),
(102, 'public/photos/IMG_20180908_100641.jpg', 4, 5, 0, NULL, '2018-09-09 12:09:35', '2018-09-09 12:09:35'),
(103, 'public/photos/IMG_20180908_100838.jpg', 4, 5, 0, NULL, '2018-09-09 12:09:56', '2018-09-09 12:09:56'),
(104, 'public/photos/IMG_20180908_100854.jpg', 4, 5, 0, NULL, '2018-09-09 12:10:14', '2018-09-09 12:10:14'),
(105, 'public/photos/IMG_20180908_100932.jpg', 4, 5, 0, NULL, '2018-09-09 12:10:41', '2018-09-09 12:10:41'),
(106, 'public/photos/IMG_20180908_100934.jpg', 4, 5, 0, NULL, '2018-09-09 12:11:04', '2018-09-09 12:11:04'),
(107, 'public/photos/IMG_20180908_101235.jpg', 4, 5, 0, NULL, '2018-09-09 12:11:35', '2018-09-09 12:11:35'),
(108, 'public/photos/IMG_20180908_101356.jpg', 4, 5, 0, NULL, '2018-09-09 12:12:58', '2018-09-09 12:12:58'),
(109, 'public/photos/IMG_20180908_101407.jpg', 4, 5, 0, NULL, '2018-09-09 12:13:46', '2018-09-09 12:13:46'),
(110, 'public/photos/IMG_20180908_101420.jpg', 4, 5, 0, NULL, '2018-09-09 12:14:14', '2018-09-09 12:14:14'),
(111, 'public/photos/IMG_20180908_101512.jpg', 4, 5, 0, NULL, '2018-09-09 12:14:41', '2018-09-09 12:14:41'),
(112, 'public/photos/IMG_20180908_103248.jpg', 4, 5, 0, NULL, '2018-09-09 12:15:03', '2018-09-09 12:15:03'),
(113, 'public/photos/IMG_20180908_105314.jpg', 4, 5, 0, NULL, '2018-09-09 12:16:01', '2018-09-09 12:16:01'),
(114, 'public/photos/IMG_20180908_105352.jpg', 4, 5, 0, NULL, '2018-09-09 12:16:56', '2018-09-09 12:16:56'),
(115, 'public/photos/IMG_20180908_105537.jpg', 4, 5, 0, NULL, '2018-09-09 12:18:18', '2018-09-09 12:18:18'),
(116, 'public/photos/IMG_20180908_105913.jpg', 4, 5, 0, NULL, '2018-09-09 12:19:19', '2018-09-09 12:19:19'),
(117, 'public/photos/IMG_20180908_110038.jpg', 4, 5, 0, NULL, '2018-09-09 12:21:04', '2018-09-09 12:21:04'),
(118, 'public/photos/IMG_20180908_110712.jpg', 4, 5, 0, NULL, '2018-09-09 12:22:30', '2018-09-09 12:22:30'),
(119, 'public/photos/IMG_20180908_110730.jpg', 4, 5, 0, NULL, '2018-09-09 12:23:23', '2018-09-09 12:23:23'),
(120, 'public/photos/IMG_20180908_111048.jpg', 4, 5, 0, NULL, '2018-09-09 12:24:08', '2018-09-09 12:24:08'),
(121, 'public/photos/IMG_20180908_111804.jpg', 4, 5, 0, NULL, '2018-09-09 12:28:26', '2018-09-09 12:28:26'),
(122, 'public/photos/IMG_20180908_111915.jpg', 4, 5, 0, NULL, '2018-09-09 12:32:11', '2018-09-09 12:32:11'),
(123, 'public/photos/IMG_20180908_112702.jpg', 4, 5, 0, NULL, '2018-09-09 12:32:43', '2018-09-09 12:32:43'),
(124, 'public/photos/IMG_20180908_112846.jpg', 4, 5, 0, NULL, '2018-09-09 12:33:19', '2018-09-09 12:33:19'),
(125, 'public/photos/IMG_20180908_113023.jpg', 4, 5, 0, NULL, '2018-09-09 12:34:31', '2018-09-09 12:34:31'),
(126, 'public/photos/IMG_20180908_113044.jpg', 4, 5, 0, NULL, '2018-09-09 12:34:57', '2018-09-09 12:34:57'),
(127, 'public/photos/IMG_20180908_113053.jpg', 4, 5, 0, NULL, '2018-09-09 12:35:23', '2018-09-09 12:35:23'),
(128, 'public/photos/IMG_20180908_113457.jpg', 4, 5, 0, NULL, '2018-09-09 12:35:50', '2018-09-09 12:35:50'),
(129, 'public/photos/IMG_20180908_113705.jpg', 4, 5, 0, NULL, '2018-09-09 12:36:20', '2018-09-09 12:36:20'),
(130, 'public/photos/IMG_20180908_115909.jpg', 4, 5, 0, NULL, '2018-09-09 12:36:45', '2018-09-09 12:36:45'),
(131, 'public/photos/IMG_20180908_120029.jpg', 4, 5, 0, NULL, '2018-09-09 12:37:11', '2018-09-09 12:37:11'),
(132, 'public/photos/IMG_20180908_120103.jpg', 4, 5, 0, NULL, '2018-09-09 12:37:32', '2018-09-09 12:37:32'),
(133, 'public/photos/WhatsApp Image 2019-01-26 at 10.38.01 AM(13).jpeg', 5, 5, 0, NULL, '2019-01-26 13:47:05', '2019-01-26 13:47:05'),
(134, 'public/photos/WhatsApp Image 2019-01-26 at 10.38.01 AM(11).jpeg', 5, 5, 0, NULL, '2019-01-26 13:47:23', '2019-01-26 13:47:23'),
(135, 'public/photos/WhatsApp Image 2019-01-26 at 10.38.01 AM(9).jpeg', 5, 5, 0, NULL, '2019-01-26 13:47:41', '2019-01-26 13:47:41'),
(136, 'public/photos/WhatsApp Image 2019-01-26 at 10.38.01 AM(8).jpeg', 5, 5, 0, NULL, '2019-01-26 13:47:59', '2019-01-26 13:47:59'),
(137, 'public/photos/WhatsApp Image 2019-01-26 at 10.38.01 AM(7).jpeg', 5, 5, 0, NULL, '2019-01-26 13:48:18', '2019-01-26 13:48:18'),
(138, 'public/photos/WhatsApp Image 2019-01-26 at 10.38.01 AM(4).jpeg', 5, 5, 0, NULL, '2019-01-26 13:48:42', '2019-01-26 13:48:42'),
(139, 'public/photos/WhatsApp Image 2019-01-26 at 10.38.01 AM(3).jpeg', 5, 5, 0, NULL, '2019-01-26 13:49:14', '2019-01-26 13:49:14');

-- --------------------------------------------------------

--
-- Table structure for table `profil_photos`
--

CREATE TABLE `profil_photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default_image` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profil_photos`
--

INSERT INTO `profil_photos` (`id`, `image_path`, `default_image`, `user_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(7, 'public/profile_images/SSWQAb7sqDEpgEY1ZonKbt46BFIx3uFeuQR88A2M.jpeg', 1, 2, NULL, '2018-07-23 22:32:28', '2018-07-23 22:32:28');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `project_image_path` varchar(255) DEFAULT NULL,
  `project_main_subject` varchar(255) DEFAULT NULL,
  `project_description` text,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `deleted_by` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `project_image_path`, `project_main_subject`, `project_description`, `added_by`, `updated_by`, `deleted_by`, `deleted_at`, `updated_at`, `created_at`) VALUES
(3, 'Culvert’s', 'public/projects/EL7HnPq9VkG7bwD9jRDFI5YHuFmLdKEotWDsKmfK.mp4', 'Coredour Abdoun culverts', 'The Purpose of the project was to collect the rain water in Abdoun area.\r\nThree culverts were implemented in three phases during 2008, 2010 and 2013.\r\nDuration of each phase was 80 days.', 2, 5, 0, NULL, '2018-08-04 22:41:35', '2018-08-03 18:06:06'),
(4, 'Culvert’s', 'public/projects/NzW3RMzmhULxxHSTs3pn9F2JQxHV6N1P1ih489WQ.mp4', NULL, NULL, 2, 2, 2, '2018-08-04 13:51:45', '2018-08-04 13:51:45', '2018-08-04 12:47:17'),
(5, 'Fiber To The Home', 'public/projects/mVKR9fVEe60W85XLFzBXDCAjJoIK4L4dAZJrEnT5.mp4', '•	North Abdoun FTTH', 'The purpose from this project is to cover the whole area with Zain network', 5, 5, 0, NULL, '2018-08-05 12:50:48', '2018-08-04 21:48:36'),
(6, 'Ghabawi Water Line', 'public/projects/RxZ39xaqO5NO0qnfI1JQPHGnQRodODrSWXtO3VdF.mp4', 'Ghabawi Water Line', 'The purpose from this project was to service Al-Ghabawi Electricity station', 5, 0, 0, NULL, '2018-08-04 22:14:46', '2018-08-04 22:14:46'),
(7, 'Rain Water drainage Line', 'public/projects/EZ9HAKvgxkrKsyrtHaswOKZdE4q2iIozMzwoVBVp.mp4', 'Rain Water drainage Line', 'Rain Water drainage extending from Alba House (Swelieh) to 8th circle.', 5, 0, 0, NULL, '2018-08-04 22:33:45', '2018-08-04 22:33:45');

-- --------------------------------------------------------

--
-- Table structure for table `project_details`
--

CREATE TABLE `project_details` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `project_image` varchar(255) NOT NULL,
  `added_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `deleted_by` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `slider_shows`
--

CREATE TABLE `slider_shows` (
  `id` int(10) UNSIGNED NOT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_photo` int(1) NOT NULL DEFAULT '0',
  `added_by` int(11) NOT NULL,
  `ended_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider_shows`
--

INSERT INTO `slider_shows` (`id`, `image_path`, `start_date`, `end_date`, `first_photo`, `added_by`, `ended_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'public/sliderShow/ad8SbQPOlt0BBKkD3tztAhE5uN86EHZ3UnY3AH3n.jpeg', '2018-07-23', NULL, 0, 2, NULL, 5, '2018-08-11 17:35:28', '2018-07-24 00:26:39', '2018-08-11 17:35:28'),
(2, 'public/sliderShow/xCntTUixEfbHhSIS21igP9EIp2YafCT10NbfrxZT.jpeg', '2018-07-23', NULL, 0, 2, NULL, 2, '2018-07-24 22:27:13', '2018-07-24 00:27:28', '2018-07-24 22:27:13'),
(3, 'public/sliderShow/neEVNderYrdtk0zXdO8xj9O39qvT8z0pHgu1ZzXY.jpeg', '2018-07-23', NULL, 0, 2, NULL, 2, '2018-07-24 22:27:12', '2018-07-24 00:35:36', '2018-07-24 22:27:12'),
(4, 'public/sliderShow/OGjzQJmIEWdDh0GzIcQNrLCYhzOmC8C8nEQ8ftLo.jpeg', '2018-07-23', NULL, 0, 2, NULL, 2, '2018-07-24 22:27:10', '2018-07-24 00:45:07', '2018-07-24 22:27:10'),
(5, 'public/sliderShow/cZXpWuQcnJxlZhNVFTvVJQkISPB9xzvQqermh50x.jpeg', '2018-07-24', NULL, 0, 2, NULL, 2, '2018-07-24 22:27:08', '2018-07-24 08:46:50', '2018-07-24 22:27:08'),
(6, 'public/sliderShow/fyy8Mdgj3XzBDRAia4KoA4wrHvvTZTXV2Lt6YXeT.jpeg', '2018-07-24', NULL, 0, 2, NULL, 2, '2018-07-24 22:27:01', '2018-07-24 08:47:24', '2018-07-24 22:27:01'),
(7, 'public/sliderShow/OGjzQJmIEWdDh0GzIcQNrLCYhzOmC8C8nEQ8ftLo.jpeg', '2018-07-24', NULL, 0, 2, NULL, 5, '2018-08-11 17:32:20', '2018-07-24 22:28:43', '2018-08-11 17:32:20'),
(8, 'public/sliderShow/ZAiiM7SEVAxSwkiSDVn8TRVZ2yD1cpN0hR1tLq1m.jpeg', '2018-07-24', NULL, 0, 2, NULL, NULL, NULL, '2018-07-24 22:30:08', '2018-07-24 22:30:08'),
(9, 'public/sliderShow/YrZQkNGfxEDCFVpIGClRZPyhx9YhH6mB9tOymQyQ.jpeg', '2018-07-24', NULL, 0, 2, NULL, 5, '2018-07-29 11:42:53', '2018-07-24 22:30:49', '2018-07-29 11:42:53'),
(10, 'public/sliderShow/NMXOHs1tZLSSlqvODTpGHYMJN4VOJOXeWJ1VnnNa.jpeg', '2018-07-24', NULL, 0, 2, NULL, NULL, NULL, '2018-07-24 22:31:47', '2018-08-03 17:14:15'),
(11, 'public/sliderShow/QoiJn08SNmAHfgDg5gudpMMkDPshWgnwU6D2yFg6.jpeg', '2018-07-29', NULL, 1, 5, NULL, NULL, NULL, '2018-07-29 11:42:48', '2018-08-03 17:14:27'),
(12, 'public/sliderShow/czxcbc9zRiiO2q2DiZ080DXpm2wcKiwoLBtwyCqi.jpeg', '2018-08-11', NULL, 0, 5, NULL, NULL, NULL, '2018-08-11 17:33:10', '2018-08-11 17:33:10'),
(13, 'public/sliderShow/VMoFI2CG8TAyUTxi46KWNWGtkx8KAGKxCDRu56Qh.jpeg', '2018-08-11', NULL, 0, 5, NULL, NULL, NULL, '2018-08-11 17:35:00', '2018-08-11 17:35:00'),
(14, 'public/sliderShow/awZY2o4S6bMz5v5Aguj2wd0LEPWZZ6jt0nF9oRUH.jpeg', '2018-08-14', NULL, 0, 5, NULL, 5, '2018-08-14 14:15:59', '2018-08-14 14:05:51', '2018-08-14 14:15:59'),
(15, 'public/sliderShow/VelpJPqUvAq9j2UcJLzWjaqOVdNQgCgjh9iMKCSN.jpeg', '2018-08-14', NULL, 0, 5, NULL, NULL, NULL, '2018-08-14 14:15:50', '2018-08-14 14:15:50'),
(16, 'public/sliderShow/tUcrvLRNwfUSATJSmQNjrfQs1mrRox72zKghVk27.jpeg', '2018-08-26', '2018-09-09', 0, 5, 5, NULL, NULL, '2018-08-26 15:08:07', '2018-09-09 12:43:30'),
(17, 'public/sliderShow/Z4fvBu1IlanBrIEPKBtP2ZuZv9OXfBxcuoCcyCaJ.jpeg', '2018-09-09', NULL, 0, 5, NULL, NULL, NULL, '2018-09-09 12:39:04', '2018-09-09 12:39:04'),
(18, 'public/sliderShow/wyGhR5xlTh8xUxragXd3l0483a9CEwe28dMlLeh2.jpeg', '2018-09-12', '2018-09-15', 0, 5, 5, NULL, NULL, '2018-09-12 15:08:46', '2018-09-15 15:27:14'),
(19, 'public/sliderShow/WXILbbBcgG6AYjeqvWuiFmO849mJK4IFxh8ND34w.jpeg', '2018-10-15', NULL, 0, 5, NULL, NULL, NULL, '2018-10-15 12:40:37', '2018-10-15 12:40:37'),
(20, 'public/sliderShow/Srz7ioI7b0rL1eAJfFD8Xe2vhZxsGwTMiurmVBKk.jpeg', '2018-10-15', NULL, 0, 5, NULL, NULL, NULL, '2018-10-15 12:42:26', '2018-10-15 12:42:26'),
(21, 'public/sliderShow/sRTvpUdX5DvVrZzFH2hFlGXWFmtlGDMNHCGNTtOS.jpeg', '2018-12-23', '2019-01-01', 0, 5, NULL, NULL, NULL, '2018-12-23 16:05:15', '2018-12-23 16:05:15'),
(22, 'public/sliderShow/3OduFkUs9aq7w8nsw65dYMVl7N8Je1ISjkBiDKdA.jpeg', '2019-01-26', NULL, 0, 5, NULL, NULL, NULL, '2019-01-26 13:53:05', '2019-01-26 13:53:05');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `active`, `password`, `added_by`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 'Ahmad Sulaiman', 'ahmed_ali_1994@hotmail.com', 1, '$2y$10$Nt3rtA26fRdp65wYrukVBuRPZE.WE/mxKptvOq5.M3UHPU92P82t2', 2, 'phugKJ6Afimy4LJkMQ88TNvdSjYaKtOiJPCYtS9kBiUtHZjkLuknkV0frLsu', NULL, '2018-07-03 15:16:35', '2018-07-30 21:58:39'),
(3, 'Ahmad Ali', 'ahmad-sulaiman-1994@outlook.com', 0, '$2y$10$KXcVdpo0f2fo2GvJBe3dH.lNpeDXHrOWYeCHF7bylDL6xbPMBs/ni', 2, 'Q91X8er3n5hNLGUeMHz3bkPvh0TlkT6vPra0rvKZ08HZtUiuYz65l9znFKwo', NULL, '2018-07-06 09:00:37', '2018-08-01 00:43:02'),
(4, 'Ahmad Abo', 'ahmad.s@ecompute.net1', 0, '$2y$10$dk7Xo2UBUTa7gXPYOWBRr.eq8woM7ToxfOf55qsAQlUFKSHl40wWO', 2, NULL, NULL, '2018-07-06 09:02:10', '2018-08-01 00:43:37'),
(5, 'Mohammad Otoum', 'mohammad@sufiannasser.com', 1, '$2y$10$6jVH0s9kwxDYSn4NHEcrJOChcfRwFXx3SUpvbeDfQ247EJSnGp91e', 2, 'JaJDhMU1mbOJY2upCfLKZfrv7A3s0mlamCHNp2hC350COeYZqrM5rS86Sn4w', NULL, '2018-07-26 15:30:28', '2018-08-05 12:10:25');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `added_by` int(11) NOT NULL,
  `video_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_description` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `added_by`, `video_path`, `video_subject`, `video_description`, `deleted_at`, `deleted_by`, `created_at`, `updated_at`) VALUES
(4, 2, 'public/videos/SJYuVZQmj5cO9b0eDqlNSL8m7YPhZ2Ocyyh3NrrY.mp4', 'Promo 1', 'Promo', '2018-07-24 20:44:09', 2, '2018-07-24 20:43:01', '2018-07-24 20:44:09'),
(5, 2, 'public/videos/WdDbCoiFZE6Ol0gCN3ck5XMxa28WJbiEw8wCHp0d.mp4', 'Stages of work', 'Promo Two', NULL, 0, '2018-07-24 21:00:21', '2018-07-24 21:00:21'),
(6, 2, 'public/videos/EoihjakljUIIPPSKVKAOQPQASFBNMDFHHQKF.mp4', 'Sn&Sons Company', 'Promo One', NULL, 0, '2018-07-25 00:37:00', '2018-07-25 00:37:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_companies`
--
ALTER TABLE `about_companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_messages`
--
ALTER TABLE `contact_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_images`
--
ALTER TABLE `document_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `last_news`
--
ALTER TABLE `last_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profil_photos`
--
ALTER TABLE `profil_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_details`
--
ALTER TABLE `project_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_shows`
--
ALTER TABLE `slider_shows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_companies`
--
ALTER TABLE `about_companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `contact_messages`
--
ALTER TABLE `contact_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `document_images`
--
ALTER TABLE `document_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `last_news`
--
ALTER TABLE `last_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;

--
-- AUTO_INCREMENT for table `profil_photos`
--
ALTER TABLE `profil_photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `project_details`
--
ALTER TABLE `project_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `slider_shows`
--
ALTER TABLE `slider_shows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
